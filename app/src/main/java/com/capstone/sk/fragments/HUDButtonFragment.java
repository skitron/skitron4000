package com.capstone.sk.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.capstone.sk.R;
import com.capstone.sk.adapters.HUDButtonAdapter;
import com.capstone.sk.control.button.ButtonInformation;
import com.capstone.sk.control.hud.HUDView;
import com.capstone.sk.helpers.FragmentAccessor;
import com.capstone.sk.helpers.Utils;

import java.util.ArrayList;

/**
 * Created by Aaron on 3/4/2015.
 */
public class HUDButtonFragment extends Fragment
{
    private static final String HUD_NAME = "hudname";
    private static final String HUD_BUTTON_INFORMATION = "hudbutton";

    private String hudName;
    private ButtonInformation hudButtonInformation;
    private HUDButtonAdapter hudButtonAdapter;
    private Context context;
    private FragmentAccessor fragmentAccessor;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            fragmentAccessor = (FragmentAccessor) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + FragmentAccessor.class.getSimpleName());
        }
    }

    public static HUDButtonFragment newInstance(String hudName, ButtonInformation buttonInformation)
    {
        HUDButtonFragment buttonFragment = new HUDButtonFragment();
        Bundle args = new Bundle();
        args.putString(HUD_NAME, hudName);
        args.putSerializable(HUD_BUTTON_INFORMATION, buttonInformation);
        buttonFragment.setArguments(args);

        return buttonFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        context = getActivity();

        Bundle args = getArguments();
        if (args != null)
        {
            hudName = args.getString(HUD_NAME);
            hudButtonInformation = (ButtonInformation) args.getSerializable(HUD_BUTTON_INFORMATION);
        }

        ActionBarActivity activity = (ActionBarActivity) getActivity();
        activity.getSupportActionBar().setTitle(hudName + " Buttons");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                getActivity().getSupportFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_hud_button, container, false);

        hudButtonAdapter = new HUDButtonAdapter(getActivity(), hudButtonInformation);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.button_action_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(hudButtonAdapter);

        return view;
    }

    @Override
    public void onPause()
    {
        super.onPause();

        ArrayList<HUDButtonAdapter.ButtonInfo> buttonInfos = hudButtonAdapter.getButtonInfos();
        HUDView hudView = fragmentAccessor.getViewController().getHUDViewForName(hudName);

        if (hudView != null)
        {
            Log.d("HUDButtonFragment", "Found hudview.");

            for (HUDButtonAdapter.ButtonInfo info : buttonInfos)
            {
                hudButtonInformation.setFunction(info.getName(), info.getFunctionName());
            }

            hudView.setButtonInformation(hudButtonInformation);
            fragmentAccessor.getViewController().resaveView(hudView);
        }

        Utils.makeToast(context, "Saved button information.");
    }
}
