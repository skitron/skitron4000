package com.capstone.sk.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capstone.sk.R;
import com.capstone.sk.adapters.HUDMainAdapter;
import com.capstone.sk.control.hud.HUDView;
import com.capstone.sk.control.hud.ViewController;
import com.capstone.sk.customui.RecyclerViewSwipeListener;
import com.capstone.sk.helpers.FragmentAccessor;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.listeners.ActionClickListener;
import com.nispok.snackbar.listeners.EventListenerAdapter;

/**
 * Created by Aaron on 12/31/2014.
 *
 * Displays all of the currently editable and available HUD Views as cards.
 */
public class HUDMainFragment extends Fragment
{
    private HUDMainAdapter hudAdapter;
    private ActionBarActivity activity;
    private FloatingActionButton floatingActionButton;
    private LinearLayoutManager layoutManager;
    private FragmentAccessor fragmentAccessor;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            fragmentAccessor = (FragmentAccessor) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + FragmentAccessor.class.getSimpleName());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getActivity() != null)
        {
            activity = (ActionBarActivity) getActivity();

            // Create the adapter for the list of views, and populate it with the View Controller's huds
            hudAdapter = new HUDMainAdapter(activity, activity.getSupportFragmentManager());
            hudAdapter.add(fragmentAccessor.getViewController().getHUDViews());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_hud_main, container, false);

        setupRecyclerView((RecyclerView) view.findViewById(R.id.hud_recycler_view));

        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.hud_main_add);
        setupFloatingActionButton();

        return view;
    }

    private void setupRecyclerView(RecyclerView hudRecyclerView)
    {
        // The Recycler View must have a layout manager set
        layoutManager = new LinearLayoutManager(activity);
        hudRecyclerView.setLayoutManager(layoutManager);
        hudRecyclerView.setAdapter(hudAdapter);

        RecyclerViewSwipeListener swipeTouchListener = new RecyclerViewSwipeListener(hudRecyclerView,
                new RecyclerViewSwipeListener.SwipeListener()
                {
                    @Override
                    public boolean canSwipe(int position)
                    {
                        return position != hudAdapter.getItemCount() - 1;
                    }

                    @Override
                    public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions)
                    {

                    }

                    @Override
                    public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions)
                    {
                        for (int position : reverseSortedPositions)
                        {
                            HUDView hudView = hudAdapter.getHUDView(position);
                            String name = hudView.getHUDName();

                            // Get rid of the hud in the list, and tell the external view controller to remove
                            // the file as well
                            hudAdapter.remove(position);
                            fragmentAccessor.getViewController().removeView(hudView);

                            // Create a snackbar that disappears in 5 seconds, and offers undo functionality
                            SnackbarManager.show(Snackbar.with(activity) // context
                                    .text(name + " removed") // text to display
                                    .duration(5000)
                                    .actionLabel("Undo") // action button label
                                    .actionListener(new ActionClickListener()
                                    {
                                        @Override
                                        public void onActionClicked(Snackbar snackbar)
                                        {
                                            // If undo is selected, then we want re-add to the list and to the
                                            // external ViewController
                                            HUDView undoView = hudAdapter.undo();
                                            fragmentAccessor.getViewController().resaveView(undoView);
                                        }
                                    }) // action button's ActionClickListener
                                    .eventListener(new EventListenerAdapter()
                                    {
                                        @Override
                                        public void onDismiss(Snackbar snackbar)
                                        {
                                            // Causes the FAB to animate back to where it started.
                                            floatingActionButton.animate().translationY(0);
                                        }

                                        @Override
                                        public void onShow(Snackbar snackbar)
                                        {
                                            // Causes the FAB to animate up above the snackbar
                                            floatingActionButton.animate().translationY(-snackbar.getHeight());
                                        }
                                    })
                                    , activity); // activity where it is displayed
                        }
                    }
                });

        // Add the listener to the recyclerView
        hudRecyclerView.addOnItemTouchListener(swipeTouchListener);
    }

    private void setupFloatingActionButton()
    {
        floatingActionButton.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                .title("Add a new HUD view.")
                                .customView(R.layout.add_hud_view, true)
                                .positiveText("Add")
                                .negativeText("Cancel")
                                .autoDismiss(false)
                                .callback(new MaterialDialog.ButtonCallback()
                                {
                                    @Override
                                    public void onPositive(MaterialDialog dialog)
                                    {
                                        View addView = dialog.getCustomView();

                                        // Pull out the edit text field
                                        EditText nameField = (EditText) addView.findViewById(R.id.hud_new_name);
                                        Editable editable = nameField.getText();
                                        String hudName = editable.toString();

                                        // Pull out the template name field
                                        Spinner templateSelect = (Spinner) addView.findViewById(R.id.hud_template_spinner);
                                        String[] templateNames = getActivity().getResources().getStringArray(R.array.template_names);
                                        String templateName = templateNames[templateSelect.getSelectedItemPosition()];

                                        final TextView errorText = (TextView) addView.findViewById(R.id.hud_error_text);

                                        ViewController viewController = fragmentAccessor.getViewController();

                                        // If the View Controller already has a hud with that name, show the error message
                                        if (hudName.isEmpty())
                                        {
                                            errorText.setText(R.string.add_hud_view_empty_name);
                                            errorText.setVisibility(View.VISIBLE);
                                        }
                                        else if (viewController.hasName(hudName))
                                        {
                                            errorText.setText(R.string.add_hud_view_error_message);
                                            errorText.setVisibility(View.VISIBLE);
                                        }
                                        else
                                        {
                                            // Otherwise add the hud to the list and tell the View Controller
                                            // to create a file and add it to the list of HUDS it has
                                            viewController.resaveView(new HUDView(getActivity(), hudName, templateName));
                                            hudAdapter.add(viewController.getHUDViews());
                                            layoutManager.scrollToPosition(hudAdapter.getItemCount() - 1);
                                            dialog.dismiss();
                                        }
                                    }

                                    @Override
                                    public void onNegative(MaterialDialog dialog)
                                    {
                                        dialog.dismiss();
                                    }
                                })
                                .build();

                        View addView = dialog.getCustomView();
                        EditText nameField = (EditText) addView.findViewById(R.id.hud_new_name);

                        nameField.setOnFocusChangeListener(new View.OnFocusChangeListener()
                        {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus)
                            {
                                if (hasFocus)
                                {
                                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                                }
                            }
                        });

                        nameField.setFocusable(true);
                        nameField.setFocusableInTouchMode(true);
                        nameField.requestFocus();
                        dialog.show();
                    }
                }
        );

    }


    @Override
    public void onResume()
    {
        super.onResume();

        // Pull in any changes that have been made externally
        hudAdapter.add(fragmentAccessor.getViewController().getHUDViews());
    }

    @Override
    public void onPause()
    {
        super.onPause();

        // Save all of the current changes, and clear the undo functionality
        fragmentAccessor.getViewController().saveHUDS();
        hudAdapter.clearUndo();
        SnackbarManager.dismiss();
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        hudAdapter.recycle();
    }
}
