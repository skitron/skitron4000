package com.capstone.sk.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capstone.sk.R;
import com.capstone.sk.adapters.HUDEditorAdapter;
import com.capstone.sk.control.content.ContentManager;
import com.capstone.sk.control.content.ContentType;
import com.capstone.sk.control.hud.HUDView;
import com.capstone.sk.editor.elements.HUDBackgroundElement;
import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.elements.HUDIconBankElement;
import com.capstone.sk.editor.elements.HUDTextElement;
import com.capstone.sk.editor.general.HUDEditor;
import com.capstone.sk.helpers.FragmentAccessor;
import com.capstone.sk.helpers.Utils;
import com.capstone.sk.helpers.ViewFactory;

/**
 * Created by Aaron on 5/21/2014.
 */
public class HUDEditorFragment extends Fragment
{
    private static final String HUD_NAME = "hudname";
    private static final String HUD_TEMPLATE_NAME = "hudtemplate";

    private Context context;
    private HUDView editedView;
    private String hudName, templateName;
    private HUDEditor editor;
    private HUDEditorAdapter hudEditorAdapter;
    private ContentManager contentManager;
    private FragmentAccessor fragmentAccessor;

    private boolean showDeleteIcon = false;

    public static HUDEditorFragment newInstance(String hudName, String templateName)
    {
        HUDEditorFragment fragment = new HUDEditorFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(HUD_NAME, hudName);
        bundle.putSerializable(HUD_TEMPLATE_NAME, templateName);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            fragmentAccessor = (FragmentAccessor) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + FragmentAccessor.class.getSimpleName());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Bundle args = getArguments();
        if (args != null)
        {
            hudName = (String) args.getSerializable(HUD_NAME);
            templateName = (String) args.getSerializable(HUD_TEMPLATE_NAME);

            Utils.setActionBarTitle(getActivity(), hudName);
        }

        context = getActivity();
        contentManager = fragmentAccessor.getViewController().getContentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_hud_editor, container, false);

        ListView hudListView = (ListView) view.findViewById(R.id.hud_edit_listview);
        RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.hud_scaled);
        layoutSetup(layout);

        editedView = new HUDView(context, hudName, templateName);
        editedView.recreate(layout);
        editedView.updateContent(contentManager.getDefaultContent());

        editor = new HUDEditor(context, editedView.getBackgroundElement());
        hudEditorAdapter = new HUDEditorAdapter(context, editor.getHUDItems());
        hudListView.setAdapter(hudEditorAdapter);

        ViewTreeObserver vto = view.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                editor.setSelectedElement(editedView.getBackgroundElement(), hudEditorAdapter);
            }
        });

        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        editedView.load();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        editedView.save();
        editedView.saveExternal();

        fragmentAccessor.getViewController().resaveView(editedView);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_hud_editor, menu);

        // Tint the menu icons white since it doesn't happen automatically
        reTintMenuIcon(R.id.action_add_element, menu);
        reTintMenuIcon(R.id.action_delete_element, menu);

        MenuItem deleteIcon = menu.findItem(R.id.action_delete_element);
        deleteIcon.setVisible(showDeleteIcon);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        ActionBarActivity parent = (ActionBarActivity) context;

        switch (id)
        {
            case android.R.id.home:
                parent.getSupportFragmentManager().popBackStack();
                return true;
            case R.id.action_add_element:
                showAddElementDialog();
                return true;
            case R.id.action_delete_element:
                new MaterialDialog.Builder(context)
                        .title("Delete selected item.")
                        .content("Are you sure you want to delete the selected item?")
                        .positiveText("Yes")
                        .negativeText("No")
                        .callback(new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onPositive(MaterialDialog dialog)
                            {
                                editedView.removeElement(editor.getSelectedItem());
                                editor.setSelectedElement(editedView.getBackgroundElement(), hudEditorAdapter);
                                setShowDeleteIcon(false);
                            }
                        })
                        .show();

                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void setShowDeleteIcon(boolean showDeleteIcon)
    {
        this.showDeleteIcon = showDeleteIcon;
        getActivity().invalidateOptionsMenu();
    }

    private void reTintMenuIcon(int res, Menu menu)
    {
        MenuItem item = menu.findItem(res);
        Drawable icon = item.getIcon();
        icon.mutate().setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_IN);
        item.setIcon(icon);
    }

    /**
     * Creates and shows the Add Element Dialog.
     */
    private void showAddElementDialog()
    {
        MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Add a new element.")
                .customView(R.layout.add_hud_element, true)
                .positiveText("Add")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback()
                {
                    @Override
                    public void onPositive(MaterialDialog dialog)
                    {
                        View dialogView = dialog.getCustomView();
                        Spinner elementType = (Spinner) dialogView.findViewById(R.id.hud_element_type_spinner);
                        Spinner contentType = (Spinner) dialogView.findViewById(R.id.hud_content_type_spinner);

                        String elementName = elementType.getSelectedItem().toString();
                        String contentName = contentType.getSelectedItem().toString();

                        if (elementName.equals("Text Element"))
                        {
                            HUDTextElement textElement = new HUDTextElement(ViewFactory.makeTextView(context, "New Element"), context);
                            textElement.setContentType(ContentType.fromString(contentName.toLowerCase().replace(" ", "_")));
                            textElement.updateContent(contentManager.getDefaultContent());
                            editedView.addElement(textElement);

                            editor.setSelectedElement(textElement, hudEditorAdapter);
                        }
                        else if (elementName.equals("Icon Element"))
                        {
                            HUDIconBankElement iconBankElement = new HUDIconBankElement(ViewFactory.makeIconBankLayout(context), context);
                            iconBankElement.addIcon(ContentType.fromString(contentName.toLowerCase().replace(" ", "_")));
                            iconBankElement.updateContent(contentManager.getDefaultContent());
                            editedView.addElement(iconBankElement);

                            editor.setSelectedElement(iconBankElement, hudEditorAdapter);
                        }
                    }
                })
                .build();

        View dialogView = dialog.getCustomView();
        final Spinner contentType = (Spinner) dialogView.findViewById(R.id.hud_content_type_spinner);

        Spinner elementType = (Spinner) dialogView.findViewById(R.id.hud_element_type_spinner);

        elementType.setAdapter(ArrayAdapter.createFromResource(context,
                R.array.hud_element_names, R.layout.support_simple_spinner_dropdown_item));

        elementType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String item = (String) parent.getSelectedItem();
                int arrayRes = R.array.content_text;

                switch (item)
                {
                    case "Text Element":
                        arrayRes = R.array.content_text;
                        break;
                    case "Icon Element":
                        arrayRes = R.array.content_icons;
                        break;
                    case "Image Element":

                        break;
                }

                contentType.setAdapter(ArrayAdapter.createFromResource(context,
                        arrayRes, R.layout.support_simple_spinner_dropdown_item));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        dialog.show();
    }

    /**
     * Sets the background layout to support dragging and clicking.
     *
     * @param parentView Layout to add OnClick and OnDrag listeners to
     */
    public void layoutSetup(final RelativeLayout parentView)
    {
        parentView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                // IF ALL ELSE FAILS GO TO THE SOURCE
                // THIS IS THE CHEESY ANDROID SOURCE COPY
                Rect tmpRect = new Rect();
                int childCount = parentView.getChildCount();

                for (int i = childCount - 1; i >= 0; i--)
                {
                    View child = parentView.getChildAt(i);
                    child.getHitRect(tmpRect);

                    // If one of the child views is being touched set that to the selected.
                    if (tmpRect.contains((int) event.getX(), (int) event.getY()))
                    {
                        HUDElement element = (HUDElement) child.getTag();
                        editor.setSelectedElement(element, hudEditorAdapter);
                        setShowDeleteIcon(true);

                        return true;
                    }
                }

                // Otherwise fall through and set the background element to selected.
                HUDElement element = (HUDElement) v.getTag();
                editor.setSelectedElement(element, hudEditorAdapter);
                setShowDeleteIcon(false);

                return true;
            }
        });

        parentView.setOnDragListener(new View.OnDragListener()
        {
            float initialX,
                    initialY;

            public boolean onDrag(View v, DragEvent dragEvent)
            {
                int action = dragEvent.getAction();
                View dragged = (View) dragEvent.getLocalState();

                switch (action)
                {
                    case DragEvent.ACTION_DRAG_STARTED:
                        initialX = dragEvent.getX();
                        initialY = dragEvent.getY();

                        HUDElement element = (HUDElement) dragged.getTag();
                        editor.setSelectedElement(element, hudEditorAdapter);

                        setShowDeleteIcon(!(element instanceof HUDBackgroundElement));

                        break;
                    case DragEvent.ACTION_DROP:
                        RelativeLayout container = (RelativeLayout) v;

                        int[] viewLocation = new int[2];
                        dragged.getLocationOnScreen(viewLocation);

                        float newX = dragEvent.getX() + (viewLocation[0] - initialX);
                        float newY = dragEvent.getY() + (viewLocation[1] - initialY);

                        Log.d("HudEditorFragment", "(" + newX + ", " + newY + ") is new drop location.");
                        // Set the positions to be within the bounds of the screen
                        // and to snap to those bounds if necessary
                        if (newX < 0) newX = 0;
                        if (newY < container.getY() || container != parentView)
                            newY = container.getY();

//                        if (newX + dragged.getWidth() > container.getWidth())
//                            newX = container.getWidth() - dragged.getWidth();

                        if (newY + dragged.getHeight() > container.getHeight() || container != parentView)
                            newY = container.getHeight() - dragged.getHeight();

                        dragged.setX(newX);
                        dragged.setY(newY);

                        container.invalidate();
                        dragged.setVisibility(View.VISIBLE);

                        break;

                    case DragEvent.ACTION_DRAG_ENDED:
                        dragged.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }

                return true;
            }
        });
    }
}
