package com.capstone.sk.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capstone.sk.R;
import com.capstone.sk.adapters.GroupListAdapter;
import com.capstone.sk.helpers.FragmentAccessor;
import com.capstone.sk.messaging.Group;
import com.capstone.sk.messaging.MessageController;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trent on 4/22/2015.
 */
public class GroupsFragment extends Fragment implements ActionMode.Callback, GroupListAdapter.ClickListener
{
    private static final String TAG = GroupsFragment.class.getSimpleName();
    private GroupListAdapter groupListAdapter;
    private Context context;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayoutManager layoutManager;

    private ActionMode actionMode;
    private FragmentAccessor fragmentAccessor;
    private MessageController messageController;
    private RecyclerView groupList;
    private TextView noFriends;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getActivity() != null)
        {

            context = getActivity();
            groupListAdapter = new GroupListAdapter(getActivity(), this);
            messageController = fragmentAccessor.getMessageController();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_groups, container, false);

        noFriends = (TextView) view.findViewById(R.id.no_friends_text);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.message_swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                messageController.getGroups(new MessageController.GroupResponseCallback()
                {
                    @Override
                    public void onGroupResponse(ArrayList<Group> response)
                    {
                        if (response != null)
                        {
                            for (Group group : response) groupListAdapter.add(group);
                            layoutManager.scrollToPosition(0);
                            checkNoFriends();
                        }

                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        });

        groupList = (RecyclerView) view.findViewById(R.id.group_list);
        setupGroupList(groupList);

        final FloatingActionsMenu addMenu = (FloatingActionsMenu) view.findViewById(R.id.add_menu);

        FloatingActionButton groupFab = (FloatingActionButton) view.findViewById(R.id.group_add);
        groupFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                        .title("Add a Group")
                        .customView(R.layout.add_group, true)
                        .positiveText("Add")
                        .negativeText("Cancel")
                        .autoDismiss(false)
                        .callback(new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onPositive(final MaterialDialog dialog)
                            {
                                View addView = dialog.getCustomView();

                                // Pull out the edit text field
                                final TextView errorText = (TextView) addView.findViewById(R.id.error_text);
                                EditText nameField = (EditText) addView.findViewById(R.id.group_name);
                                EditText passField = (EditText) addView.findViewById(R.id.group_password);

                                final String groupName = "#" + nameField.getText().toString();
                                String password = passField.getText().toString();

                                if (groupName.length() < 2)
                                {
                                    errorText.setText("Group name can't be empty.");
                                }
                                else
                                {
                                    Log.d(TAG, "Attempting to join " + groupName + " with password " + password);
                                    errorText.setText("Sending group request to server...");
                                    messageController.joinGroup(groupName, password, new MessageController.GroupJoinCallback()
                                    {
                                        @Override
                                        public void onGroupResponse(String code)
                                        {
                                            switch (code.toLowerCase())
                                            {
                                                case "":
                                                    Log.d(TAG, "Group joined successfully!");

                                                    if (!groupName.isEmpty())
                                                        groupListAdapter.add(new Group(groupName, ""));

                                                    if (dialog.isShowing())
                                                        dialog.dismiss();

                                                    checkNoFriends();
                                                    break;
                                                case "invalid_pw":
                                                    Log.d(TAG, "Invalid group password.");
                                                    errorText.setText("Invalid group password.");
                                                    break;
                                                case "in_group":
                                                    Log.d(TAG, "Already in the group.");
                                                    errorText.setText("You are already in that group.");
                                                    break;
                                                case "error":
                                                    errorText.setText("Server unable to respond.");
                                                    break;
                                                default:
                                                    if (dialog.isShowing()) dialog.dismiss();
                                                    break;
                                            }

                                        }
                                    });
                                }
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                dialog.dismiss();
                            }
                        })
                        .build();

                dialog.show();
                addMenu.toggle();
            }

        });

        final FloatingActionButton friendFab = (FloatingActionButton) view.findViewById(R.id.friend_add);
        friendFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                        .title("Add a Friend")
                        .customView(R.layout.add_friend, true)
                        .positiveText("Add")
                        .negativeText("Cancel")
                        .autoDismiss(false)
                        .callback(new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onPositive(MaterialDialog dialog)
                            {
                                View addView = dialog.getCustomView();

                                // Pull out the edit text field
                                TextView errorText = (TextView) addView.findViewById(R.id.error_text);
                                EditText nameField = (EditText) addView.findViewById(R.id.group_name);
                                String friendName = nameField.getText().toString();

                                if (friendName.isEmpty())
                                {
                                    errorText.setText("Friend name cannot be empty.");
                                    errorText.setVisibility(View.VISIBLE);
                                }
                                else if (friendName.startsWith("#"))
                                {
                                    errorText.setText("Only groups can start with '#'");
                                    errorText.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    groupListAdapter.add(new Group(friendName, ""));
                                    messageController.addGroup(new Group(friendName, ""));
                                    dialog.dismiss();
                                    checkNoFriends();
                                }
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                dialog.dismiss();
                            }
                        })
                        .build();

                addMenu.toggle();
                dialog.show();

            }
        });


        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.group_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_delete_all:
                ArrayList<Group> removedGroups = groupListAdapter.removeAll();
                messageController.removeGroups(removedGroups);
                checkNoFriends();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.d(TAG, "onResume() being called.");

        groupListAdapter.removeAll();
        groupListAdapter.addAll(messageController.getGroups());
        checkNoFriends();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        Log.d(TAG, "onPause() being called.");

        if (actionMode != null) actionMode.finish();
        swipeRefreshLayout.setRefreshing(false);
        messageController.save();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            fragmentAccessor = (FragmentAccessor) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + FragmentAccessor.class.getSimpleName());
        }
    }

    private void checkNoFriends()
    {
        noFriends.setVisibility(groupListAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    /**
     * Sets up the RecyclerView that's used for displaying groups.
     * This includes setting the layout manager, adapter, and scroll listener.
     *
     * @param groupList the RecyclerView to setup
     * @see android.support.v7.widget.RecyclerView
     */
    private void setupGroupList(RecyclerView groupList)
    {
        layoutManager = new LinearLayoutManager(context);
        groupList.setLayoutManager(layoutManager);
        groupList.setHasFixedSize(true);
        groupList.setAdapter(groupListAdapter);

        groupList.setOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState)
            {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                swipeRefreshLayout.setEnabled(layoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
    {
        // Inflate a menu resource providing context menu items
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.message_delete_menu, menu);
        return true;
    }

    @Override
    public void onItemClicked(int position, Group group)
    {
        MessageSendFragment messageSendFragment = MessageSendFragment.newInstance(group.getName());
        FragmentManager fm = getActivity().getSupportFragmentManager();

        // Open an instance of the HUD Button Fragment
        fm.beginTransaction()
                .replace(R.id.content_frame, messageSendFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    @Override
    public boolean onItemLongClicked(int position)
    {
        if (actionMode == null)
        {
            // If the action mode doesn't exist, create it and start with '1' selected item
            actionMode = ((ActionBarActivity) getActivity()).startSupportActionMode(this);
            actionMode.setTitle("1");
        }
        else
        {
            if (groupListAdapter.getSelectedItemCount() == 0) actionMode.finish();
            else actionMode.setTitle(groupListAdapter.getSelectedItemCount() + "");
        }

        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
    {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.action_delete:

                ArrayList<Group> deletedGroups = new ArrayList<>();
                Group deletedGroup;

                List<Integer> selectedItemPositions = groupListAdapter.getSelectedIndicies();
                int currPos;

                for (int i = selectedItemPositions.size() - 1; i >= 0; i--)
                {
                    currPos = selectedItemPositions.get(i);
                    deletedGroup = groupListAdapter.remove(currPos);
                    deletedGroups.add(deletedGroup);
                }

                groupListAdapter.clearSelections();
                messageController.removeGroups(deletedGroups);
                actionMode.finish();

                checkNoFriends();

                return true;

            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode)
    {
        this.actionMode = null;

        for (int i = layoutManager.findFirstVisibleItemPosition(); i <= layoutManager.findLastVisibleItemPosition(); i++)
        {
            // Animate back when the action mode is destroyed
            if (groupListAdapter.isPositionSelected(i))
            {
                GroupListAdapter.GroupViewHolder holder = (GroupListAdapter.GroupViewHolder) groupList.findViewHolderForPosition(i);
                holder.animateFromSelected();
            }
        }

        // Wait to clear the selections so we don't have state issues
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                groupListAdapter.clearSelections();
            }
        }, 500);
    }
}
