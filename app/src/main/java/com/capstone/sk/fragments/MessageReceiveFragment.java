package com.capstone.sk.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capstone.sk.R;
import com.capstone.sk.adapters.MessageReceiveAdapter;
import com.capstone.sk.helpers.FragmentAccessor;
import com.capstone.sk.messaging.AudioController;
import com.capstone.sk.messaging.MessageController;
import com.capstone.sk.messaging.VoiceMessage;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aaron on 2/10/2015.
 * Used for receiving and displaying incoming messages.
 */
public class MessageReceiveFragment extends Fragment implements ActionMode.Callback, MessageReceiveAdapter.ClickListener
{
    private static final String TAG = MessageReceiveFragment.class.getSimpleName();

    private Context context;
    private MessageReceiveAdapter messageReceiveAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView noMessageText;
    private FragmentManager fragmentManager;
    private LinearLayoutManager layoutManager;

    private ActionMode actionMode;
    private RecyclerView messageList;
    private MessageController messageController;
    private AudioController audioController;
    private FragmentAccessor fragmentAccessor;
    private Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        context = getActivity();
        handler = new Handler();
        fragmentManager = getActivity().getSupportFragmentManager();
        messageReceiveAdapter = new MessageReceiveAdapter(getActivity(), this);
        messageController = fragmentAccessor.getMessageController();
        audioController = messageController.getAudioController();

        messageController.setOnNewMessageListener(new MessageController.OnNewMessageListener()
        {
            @Override
            public void onNewMessages(ArrayList<VoiceMessage> messages)
            {
                // Remove all those currently displayed and only add new messages so we get the nice animation effect
                ArrayList<VoiceMessage> displayedMessages = messageReceiveAdapter.getMessages();
                messages.removeAll(displayedMessages);
                messageReceiveAdapter.addAll(messages);

                swipeRefreshLayout.setRefreshing(false);
                layoutManager.scrollToPosition(0);
                checkForNoMessages();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_message_receive, container, false);

        // Initialize the no messages views
        noMessageText = (TextView) view.findViewById(R.id.no_messages_text);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.message_swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                Log.d(TAG, "Messages being refreshed.");
                messageController.refreshMessages();
            }
        });

        messageList = (RecyclerView) view.findViewById(R.id.message_list);
        setupMessageList(messageList);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.message_create);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new MessageSendFragment())
                        .addToBackStack(null)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commitAllowingStateLoss();
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.message_receive_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.delete_all_messages:
                ArrayList<VoiceMessage> displayedMessages = messageReceiveAdapter.getMessages();
                Log.d(TAG, "All " + displayedMessages.size() + " messages are being deleted.");

                messageController.removeMessages(displayedMessages);
                messageReceiveAdapter.removeAll();
                checkForNoMessages();

                return true;
//            case R.id.send_test_messages:
//                Random random = new Random();
//                int r = random.nextInt(5) + 1;
//
//                SharedPreferences prefs = SharedPrefs.getSharedPreferences(context);
//                String accountName = prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);
//
//                for (int i = 0; i < r; i++)
//                {
//                    VoiceMessage message = new VoiceMessage.Builder()
//                            .to(accountName)
//                            .from(accountName)
//                            .content("SGVsbG8=")
//                            .duration(String.format("%d", i))
//                            .build();
//
//                    messageController.sendTestMessage(accountName, message);
//                }
//
//                Log.d(TAG, "Sent " + r + " messages.");
//                Utils.makeToast(context, "Sent " + r + " messages.");
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.d(TAG, "onResume() being called.");

        messageReceiveAdapter.removeAll();
        messageReceiveAdapter.addAll(messageController.getMessages());
        checkForNoMessages();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        Log.d(TAG, "onPause() being called.");

        if (actionMode != null) actionMode.finish();

        audioController.stopReplaying();
        audioController.deleteTempFiles();

        swipeRefreshLayout.setRefreshing(false);
        messageController.save();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            fragmentAccessor = (FragmentAccessor) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + FragmentAccessor.class.getSimpleName());
        }
    }

    /**
     * Check to see if there are any messages, and set the background view accordingly.
     */
    private void checkForNoMessages()
    {
        boolean noMessages = messageReceiveAdapter.getMessages().size() == 0;

        noMessageText.setVisibility(noMessages ? View.VISIBLE : View.GONE);
    }


    /**
     * Sets up the RecyclerView that's used for displaying messages.
     * This includes setting the layout manager, adapter, and scroll listener.
     *
     * @param messageList the RecyclerView to setup
     * @see android.support.v7.widget.RecyclerView
     */
    private void setupMessageList(RecyclerView messageList)
    {
        layoutManager = new LinearLayoutManager(context);
        messageList.setLayoutManager(layoutManager);
        messageList.setHasFixedSize(true);
        messageList.setAdapter(messageReceiveAdapter);

        messageList.setOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState)
            {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                swipeRefreshLayout.setEnabled(layoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
    {
        // Inflate a menu resource providing context menu items
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.message_delete_menu, menu);
        return true;
    }

    @Override
    public void onItemClicked(int position, VoiceMessage message)
    {
        audioController.playMessage(message);
    }

    @Override
    public boolean onItemLongClicked(int position)
    {
        if (actionMode == null)
        {
            // If the action mode doesn't exist, create it and start with '1' selected item
            actionMode = ((ActionBarActivity) getActivity()).startSupportActionMode(this);
            actionMode.setTitle("1");
        }
        else
        {
            // If there are ever 0 items, remove the action mode, otherwise update the count
            if (messageReceiveAdapter.getSelectedItemCount() == 0) actionMode.finish();
            else actionMode.setTitle(messageReceiveAdapter.getSelectedItemCount() + "");
        }

        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
    {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.action_delete:
                ArrayList<VoiceMessage> deletedMessages = new ArrayList<>();
                VoiceMessage deletedMessage;

                List<Integer> selectedItemPositions = messageReceiveAdapter.getSelectedIndicies();
                int currPos;

                for (int i = selectedItemPositions.size() - 1; i >= 0; i--)
                {
                    currPos = selectedItemPositions.get(i);
                    deletedMessage = messageReceiveAdapter.remove(currPos);
                    deletedMessages.add(deletedMessage);
                }

                messageController.removeMessages(deletedMessages);
                messageReceiveAdapter.clearSelections();

                checkForNoMessages();
                actionMode.finish();

                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode)
    {
        this.actionMode = null;

        for (int i = layoutManager.findFirstVisibleItemPosition(); i <= layoutManager.findLastVisibleItemPosition(); i++)
        {
            // Animate back when the action mode is destroyed
            if (messageReceiveAdapter.isPositionSelected(i))
            {
                MessageReceiveAdapter.MessageViewHolder holder = (MessageReceiveAdapter.MessageViewHolder) messageList.findViewHolderForPosition(i);
                holder.animateFromSelected();
            }
        }

        // Wait to clear the selections so we don't have state issues
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                messageReceiveAdapter.clearSelections();
            }
        }, 500);
    }
}
