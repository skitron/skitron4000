package com.capstone.sk.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capstone.sk.R;
import com.capstone.sk.customui.CircleView;
import com.capstone.sk.customui.CircularProgressBar;
import com.capstone.sk.helpers.FragmentAccessor;
import com.capstone.sk.helpers.SharedPrefs;
import com.capstone.sk.helpers.Utils;
import com.capstone.sk.messaging.AudioController;
import com.capstone.sk.messaging.AudioListener;
import com.capstone.sk.messaging.MessageController;

/**
 * Created by Aaron on 3/1/2015.
 * Used for sending an outgoing message.
 */
public class MessageSendFragment extends Fragment implements View.OnClickListener
{
    private static final String TO_KEY = "to";

    private FragmentAccessor fragmentAccessor;
    private MessageController messageController;
    private AudioController audioController;

    private TextView lengthText;
    private EditText toField;
    private CircleView audioVolume;
    private CircularProgressBar recordProgress, replayProgress;
    private ImageView recordButton, replayButton;
    private String toFilled;

    /**
     * Allows for passing in a name for the to field to be filled in by default.
     * Uses the standard pattern of passing in arguments.
     *
     * @param toField The name to pass in
     * @return An instance of the fragment with the correct arguments.
     */
    public static MessageSendFragment newInstance(String toField)
    {
        MessageSendFragment fragment = new MessageSendFragment();
        Bundle args = new Bundle();
        args.putString(TO_KEY, toField);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            fragmentAccessor = (FragmentAccessor) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + FragmentAccessor.class.getSimpleName());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        messageController = fragmentAccessor.getMessageController();
        audioController = messageController.getAudioController();
        Utils.setActionBarTitle(getActivity(), "Send Message");

        Bundle args = getArguments();

        if (args != null)
        {
            toFilled = args.getString(TO_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_message_send, container, false);

        toField = (EditText) view.findViewById(R.id.to_field);
        recordButton = (ImageView) view.findViewById(R.id.record_button);
        replayButton = (ImageView) view.findViewById(R.id.replay_button);
        recordProgress = (CircularProgressBar) view.findViewById(R.id.audio_progress);
        replayProgress = (CircularProgressBar) view.findViewById(R.id.replay_progress);
        lengthText = (TextView) view.findViewById(R.id.message_length_text);
        audioVolume = (CircleView) view.findViewById(R.id.audio_volume);

        recordProgress.setMax(AudioController.MAX_MESSAGE_DURATION);

        recordButton.setOnClickListener(this);
        replayButton.setOnClickListener(this);

        toField.setText(toFilled);

        audioController.setAudioListener(new AudioListener()
        {
            @Override
            public void onStartRecord()
            {
                replayButton.setEnabled(false);
                replayProgress.setProgress(0);
                audioVolume.animate().alpha(1f).start();
            }

            @Override
            public void onEndRecord()
            {
                replayButton.setEnabled(true);
                audioVolume.animate().alpha(0f).start();
            }

            @Override
            public void onRecordStep(int messageLength, int messageAmplitude)
            {
                recordProgress.setProgress(messageLength);
                lengthText.setText(String.format("%1.2f s", (float) messageLength / 1000));
                audioVolume.setRadius((float) (20 * Math.log10(messageAmplitude)));
            }

            @Override
            public void onStartReplay(int duration)
            {
                recordButton.setEnabled(false);
                replayProgress.setMax(duration);
            }

            @Override
            public void onEndReplay()
            {
                recordButton.setEnabled(true);
            }

            @Override
            public void onReplayStep(int messageLength)
            {
                replayProgress.setProgress(messageLength);
            }
        });


        // Make sure the keyboard disappears whenever focus is lost
        toField.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (v.getId() == R.id.to_field && !hasFocus)
                {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                }
            }
        });


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.message_send_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        audioController.stopRecording();
        audioController.stopReplaying();

        switch (item.getItemId())
        {
            case android.R.id.home:
                getActivity().getSupportFragmentManager().popBackStack();
                return true;
            case R.id.send_message:
                if (sendMessage()) getActivity().getSupportFragmentManager().popBackStack();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Pops up an error dialog using the provided content.
     * Used to provide user feedback if a message hasn't been recorded yet.
     *
     * @param content Main content to show in the error dialog.
     */
    private void showErrorDialog(String content)
    {
        new MaterialDialog.Builder(getActivity()).title("Error").content(content).positiveText("Ok").show();
    }

    private boolean sendMessage()
    {
        SharedPreferences prefs = SharedPrefs.getSharedPreferences(getActivity());
        String from = prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);
        String to = toField.getText().toString();

        return messageController.sendMessage(new MessageController.MessageSendCallback()
        {
            @Override
            public void onMessageSend(MessageController.SendError errorCode)
            {
                switch (errorCode)
                {
                    case SEND_ERROR_NO_TO:
                        showErrorDialog("Please fill in the to field.");
                        break;

                    case SEND_ERROR_NO_MESSAGE:
                        showErrorDialog("Please record a message.");
                        break;

                    case SEND_ERROR_NO_FROM:
                        showErrorDialog("Please login to an account.");
                        break;
                }
            }
        }, to, from);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.record_button:
                audioController.toggleRecording();
                break;

            case R.id.replay_button:
                audioController.toggleReplaying();
                break;

            default:
                break;
        }
    }
}
