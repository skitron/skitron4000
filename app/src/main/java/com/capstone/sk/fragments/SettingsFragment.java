package com.capstone.sk.fragments;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.capstone.sk.R;

/**
 * Created by Aaron on 5/18/2014.
 */
public class SettingsFragment extends PreferenceFragment
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        setupSummaryUpdate("key_elevation_units");
        setupSummaryUpdate("key_speed_units");
        setupSummaryUpdate("key_display_resolution");
        setupSummaryUpdate("key_compression_method");
        setupSummaryUpdate("key_music_player");
    }

    private CharSequence getEntryFromString(ListPreference pref, String indexString)
    {
        int index = pref.findIndexOfValue(indexString);
        return pref.getEntries()[index];
    }

    private void setupSummaryUpdate(String key)
    {
        ListPreference pref = (ListPreference) findPreference(key);
        if (pref != null)
        {
            pref.setSummary(getEntryFromString(pref, pref.getValue()));
            pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener()
            {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue)
                {
                    preference.setSummary(getEntryFromString((ListPreference) preference, newValue.toString()));
                    return true;
                }
            });
        }
    }


}
