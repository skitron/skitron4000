package com.capstone.sk.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.capstone.sk.R;
import com.capstone.sk.comm.ControllerBluetooth;
import com.capstone.sk.comm.HUDBluetooth;
import com.capstone.sk.comm.RunLengthEncoder;
import com.capstone.sk.control.hud.HUDView;
import com.capstone.sk.control.hud.ViewController;
import com.capstone.sk.helpers.FragmentAccessor;
import com.capstone.sk.helpers.Utils;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Aaron on 2/3/2015.
 * Allows connection to the HUD and Controller and the UI changes with the connection state.
 */
public class DevicesFragment extends Fragment implements View.OnClickListener
{
    private static final String DEVICE_NAME = "SkiTron";
    private static final String TAG = DevicesFragment.class.getSimpleName();

    private ImageView controllerIcon, hudIcon;
    private TextView controllerStatus, hudStatus;
    private SwitchCompat controllerSwitch, hudSwitch;
    private ImageView hudPreview;

    private ControllerBluetooth controllerBluetooth;
    private HUDBluetooth hudBluetooth;
    private ViewController viewController;
    private FragmentAccessor fragmentAccessor;
    private Handler bluetoothHandler;

    private String connected, notConnected;
    private int connectedColor, disconnectedColor;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            fragmentAccessor = (FragmentAccessor) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + FragmentAccessor.class.getSimpleName());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        controllerBluetooth = fragmentAccessor.getControllerBluetooth();
        hudBluetooth = fragmentAccessor.getHUDBluetooth();
        viewController = fragmentAccessor.getViewController();
        bluetoothHandler = new Handler();

        connected = getResources().getString(R.string.connected);
        notConnected = getResources().getString(R.string.not_connected);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_devices, container, false);

        connectedColor = getResources().getColor(R.color.colorPrimary);
        disconnectedColor = getResources().getColor(R.color.partial_transparent);

        // Bluetooth button
        Button sendPlaced = (Button) view.findViewById(R.id.send_placed);
        ImageButton cycleLeft = (ImageButton) view.findViewById(R.id.cycle_left);
        ImageButton cycleRight = (ImageButton) view.findViewById(R.id.cycle_right);

        // HUD Preview View
        hudPreview = (ImageView) view.findViewById(R.id.hud_preview);
        viewController.updateActive();

        // Controller UI elements
        controllerSwitch = (SwitchCompat) view.findViewById(R.id.controller_switch);
        controllerIcon = (ImageView) view.findViewById(R.id.controller_connect_icon);
        controllerStatus = (TextView) view.findViewById(R.id.controller_connection_status);

        // HUD UI Elements
        hudIcon = (ImageView) view.findViewById(R.id.hud_connect_icon);
        hudSwitch = (SwitchCompat) view.findViewById(R.id.hud_switch);
        hudStatus = (TextView) view.findViewById(R.id.hud_connection_status);

        // Set listeners
        controllerSwitch.setOnClickListener(this);
        hudSwitch.setOnClickListener(this);
        sendPlaced.setOnClickListener(this);
        cycleLeft.setOnClickListener(this);
        cycleRight.setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        boolean controllerConnected = controllerBluetooth.isConnected(), hudConnected = hudBluetooth.isConnected();

        // Set the controller to reflect the connection state
        controllerIcon.setColorFilter(controllerConnected ? connectedColor : disconnectedColor);
        controllerStatus.setText(controllerConnected ? connected : notConnected);
        controllerSwitch.setChecked(controllerConnected);

        // Set the HUD to reflect the connection state
        hudIcon.setColorFilter(hudConnected ? connectedColor : disconnectedColor);
        hudStatus.setText(hudConnected ? connected : notConnected);
        hudSwitch.setChecked(hudConnected);
    }

    /**
     * Sets the color of the icon as well as the Controller status text, using a string (R.string).
     * This is used from both outside the fragment and inside.
     *
     * @param statusRes   The res of the string to use
     * @param isConnected Is the controller connected or not?
     */
    public void setControllerStatus(int statusRes, boolean isConnected)
    {
        //Check to see if we're currently looking at this fragment, as it's possible to get a status
        //update asynchronously when we're not looking at the fragment
        if (isAdded())
        {
            controllerIcon.setColorFilter(isConnected ? connectedColor : disconnectedColor);
            controllerSwitch.setChecked(isConnected);
            controllerStatus.setText(getResources().getString(statusRes));
        }
    }

    private void sendBitmap()
    {
        HUDView hudView = fragmentAccessor.getViewController().getActiveHUDView();
        if (hudView == null) return;

        // Get the preview bitmap
//        Bitmap p = RunLengthEncoder.getInstance(getActivity()).getBitmapPreview(hudView);
//        hudPreview.setImageBitmap(p);

        // Encode the actual image
        long time = System.nanoTime();
        byte[] encoded = RunLengthEncoder.getInstance(getActivity()).encodeHudView(hudView);

        int before = 800 * 480 * 2;
        int after = encoded.length;

        String output = String.format("%.2f kB -> %.2f kB, ratio of %.1f:1 in %d ms",
                ((float) before) / 1000, ((float) after) / 1000, ((float) before) / after, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time));
        Utils.makeToast(getActivity(), output);

        HUDBluetooth hudBluetooth = fragmentAccessor.getHUDBluetooth();

        if (hudBluetooth.isConnected())
        {
            hudBluetooth.writeBytes(encoded);
            Log.d(TAG, "Sent " + encoded.length + " (0x" + Integer.toHexString(encoded.length).toUpperCase() + ") bytes.");
        }
        else
        {
            Log.d(TAG, "Not connected, unable to send.");
        }
    }

    /**
     * Sets the color of the icon as well as the HUD status text, using a string (R.string).
     * This is used from both outside the fragment and inside.
     *
     * @param statusRes   The res of the string to use
     * @param isConnected Is the controller connected or not?
     */
    public void setHudStatus(int statusRes, boolean isConnected)
    {
        //Check to see if we're currently looking at this fragment, as it's possible to get a status
        //update asynchronously when we're not looking at the fragment
        if (isAdded())
        {
            hudIcon.setColorFilter(isConnected ? connectedColor : disconnectedColor);
            hudSwitch.setChecked(isConnected);
            hudStatus.setText(getResources().getString(statusRes));
        }
    }

    /**
     * Updates the image view that represents the active HUDView.
     *
     * @param hudView The active HUDView
     */
    public void updatePreview(HUDView hudView)
    {
        if (isAdded())
        {
            View hud = hudView.getView();
            Bitmap bitmap = Bitmap.createBitmap(hud.getMeasuredWidth(), hud.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            hud.draw(canvas);

            hudPreview.setImageBitmap(bitmap);
        }
    }

    /**
     * Time delayed check to see if the controller is available.
     */
    private Runnable checkControllerConnection = new Runnable()
    {
        @Override
        public void run()
        {
            controllerBluetooth.stopScanning();
            ArrayList<BluetoothDevice> devices = controllerBluetooth.getDevices();

            boolean found = false;

            // If there are no devices found, then we're unable to connect
            // Otherwise look for a SkiTron device and connect
            for (BluetoothDevice device : devices)
            {
                if (device.getName() != null)
                {
                    // Ensure there are no case issues when we check
                    if (device.getName().contains(DEVICE_NAME))
                    {
                        found = true;
                        controllerBluetooth.startConnection(device);
                    }
                }
            }

            if (!found) setControllerStatus(R.string.unable_to_connect, false);
        }
    };

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.controller_switch:

                if (controllerSwitch.isChecked())
                {
                    if (!controllerBluetooth.isConnected())
                    {
                        // Start scanning
                        setControllerStatus(R.string.attempting_connection, false);
                        controllerBluetooth.beginScanning();

                        // Wait 1 second and try to connect to a SkiTron device
                        bluetoothHandler.removeCallbacks(checkControllerConnection);
                        bluetoothHandler.postDelayed(checkControllerConnection, 1000);
                    }
                }
                else
                {
                    // If we're already connected attempt to disconnect
                    if (controllerBluetooth.isConnected()) controllerBluetooth.disconnect();
                    else setControllerStatus(R.string.not_connected, false);
                }


                break;

            case R.id.hud_switch:

                if (hudSwitch.isChecked())
                {
                    if (!hudBluetooth.isConnected())
                    {
                        // Make sure the switch is set to false until we're connected
                        boolean found = false;

                        // Look for the SkiTron device in the list of paired devices
                        for (BluetoothDevice device : hudBluetooth.getBondedDevices())
                        {
                            // If we find one, then connect
                            if (device.getName().contains(DEVICE_NAME))
                            {
                                found = true;
                                hudBluetooth.startConnection(device);
                                setHudStatus(R.string.attempting_connection, false);
                            }
                        }

                        if (!found) setHudStatus(R.string.not_paired, false);
                    }
                }
                else
                {
                    // If we're already connected, disconnect
                    if (hudBluetooth.isConnected()) hudBluetooth.disconnect();
                    else setHudStatus(R.string.not_connected, false);
                }

                break;

            case R.id.send_placed:
                sendBitmap();

                break;

            case R.id.cycle_left:
                fragmentAccessor.getViewController().cycleActiveHUDView(-1);
                break;

            case R.id.cycle_right:
                fragmentAccessor.getViewController().cycleActiveHUDView(1);
                break;
            default:
                break;
        }
    }
}
