package com.capstone.sk.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Aaron on 3/9/2015.
 */
public class SharedPrefs
{
    private static final String PREF_NAME = "com.capstone.sk.prefs";
    public static final String PREF_ACCOUNT_NAME = "account_name";
    public static final String PREF_ACCOUNT_DEFAULT = "";

    public static SharedPreferences getSharedPreferences(Context context)
    {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }
}
