package com.capstone.sk.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.capstone.sk.R;

/**
 * Created by Aaron on 5/21/2014.
 * Provides utilities for a variety of things that do not need to be in other files.
 * These include grabbing settings, getting assets, and updating layouts.
 */
public class Utils
{
    public static final String ELEVATION_UNITS = "key_elevation_units";
    public static final String SPEED_UNITS = "key_speed_units";
    private static final String TAG = Utils.class.getSimpleName();

    private static SharedPreferences prefs = null;

    private static SharedPreferences getPrefs(Context context)
    {
        if (prefs == null)
        {
            prefs = PreferenceManager.getDefaultSharedPreferences(context);
            return prefs;
        }
        else return prefs;
    }

    public static void setActionBarTitle(Activity activity, String title)
    {
        ActionBarActivity actionBarActivity = null;

        try
        {
            actionBarActivity = (ActionBarActivity) activity;

            if (actionBarActivity.getSupportActionBar() != null)
                actionBarActivity.getSupportActionBar().setTitle(title);
        } catch (ClassCastException e)
        {
            Log.e(TAG, "Activity " + actionBarActivity.getPackageName() + " is not an ActionBarActivity.");
        }
    }

    public static String getUnitStringFromKey(Context context, String key)
    {
        String[] unitStrings = null;
        SharedPreferences preferences = getPrefs(context);

        if (key.equals(ELEVATION_UNITS))
        {
            unitStrings = context.getResources().getStringArray(R.array.elevation_unit_strings);
        }
        else if (key.equals(SPEED_UNITS))
        {
            unitStrings = context.getResources().getStringArray(R.array.speed_unit_strings);
        }

        int index = Integer.parseInt(preferences.getString(key, "0"));

        assert unitStrings != null;
        return unitStrings[index];
    }

    public static Float getConversionFactorFromKey(Context context, String key, int id)
    {
        SharedPreferences preferences = getPrefs(context);
        String[] unitStrings = context.getResources().getStringArray(id);
        int index = Integer.parseInt(preferences.getString(key, "0"));

        return Float.parseFloat(unitStrings[index]);
    }

    public static int getIntPref(Context context, String key)
    {
        SharedPreferences preferences = getPrefs(context);
        return Integer.parseInt(preferences.getString(key, "0"));
    }

    public static boolean getBooleanPref(Context context, String key)
    {
        SharedPreferences preferences = getPrefs(context);
        return preferences.getBoolean(key, true);
    }

    public static void makeToast(Context context, String text)
    {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void updateLayoutFromPreference(RelativeLayout layout, Context context)
    {
        SharedPreferences preferences = getPrefs(context);
        int resolution = Integer.parseInt(preferences.getString("key_display_resolution", "1"));

        if (resolution == 0)
        {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(320, 240);
            params.gravity = Gravity.CENTER;
            params.topMargin = 80;

            layout.setLayoutParams(params);
        }
        else if (resolution == 1)
        {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(640, 480);
            params.gravity = Gravity.CENTER;
            params.topMargin = 120;

            layout.setLayoutParams(params);
        }
        else if (resolution == 2)
        {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(800, 480);
            params.gravity = Gravity.CENTER;
            params.topMargin = 80;

            layout.setLayoutParams(params);
        }
        else
        {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(96, 64);
            params.gravity = Gravity.CENTER;
            params.topMargin = 150;

            layout.setLayoutParams(params);
        }
    }

    public static void getResolution(Context context, int[] values)
    {
        SharedPreferences preferences = getPrefs(context);
        int resolution = Integer.parseInt(preferences.getString("key_display_resolution", "1"));

        switch (resolution)
        {
            case 0:
                values[0] = 320;
                values[1] = 240;
                break;
            case 1:
                values[0] = 640;
                values[1] = 480;
                break;
            case 2:
                values[0] = 800;
                values[1] = 480;
                break;
            case 3:
                values[0] = 96;
                values[1] = 64;
                break;
            default:
                values[0] = 0;
                values[1] = 0;
                break;
        }
    }

}
