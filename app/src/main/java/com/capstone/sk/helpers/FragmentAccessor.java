package com.capstone.sk.helpers;

import com.capstone.sk.comm.ControllerBluetooth;
import com.capstone.sk.comm.HUDBluetooth;
import com.capstone.sk.control.hud.ViewController;
import com.capstone.sk.messaging.MessageController;

/**
 * Created by Aaron on 3/7/2015.
 */
public interface FragmentAccessor
{
    HUDBluetooth getHUDBluetooth();

    ViewController getViewController();

    ControllerBluetooth getControllerBluetooth();

    MessageController getMessageController();
}