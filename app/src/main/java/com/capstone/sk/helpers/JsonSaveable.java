package com.capstone.sk.helpers;

import android.util.JsonReader;
import android.util.JsonWriter;

import java.io.IOException;

/**
 * Created by Aaron on 3/7/2015.
 */
public interface JsonSaveable
{
    public abstract void saveToJson(JsonWriter writer) throws IOException;

    public abstract void loadFromJson(JsonReader reader) throws IOException;
}
