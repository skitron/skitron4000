package com.capstone.sk.helpers;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.capstone.sk.R;

/**
 * Created by Aaron on 1/16/2015.
 */
public class ViewFactory
{
    public static View makeListFooterView(Context context)
    {
        final View view = new View(context);
        int dp = context.getResources().getDimensionPixelSize(R.dimen.hud_main_footer_height);
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp);
        view.setLayoutParams(params);

        return view;
    }

    public static LinearLayout makeIconBankLayout(Context context)
    {
        LinearLayout linearLayout = new LinearLayout(context);
        int height = context.getResources().getDimensionPixelSize(R.dimen.icon_bank_height);
        int margin = 12;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        params.setMargins(margin, margin, margin, margin);
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        return linearLayout;
    }

    public static ImageView makeClickableImageView(Context context)
    {
        final ImageView imageView = new ImageView(context);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageView.setLayoutParams(params);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setPadding(5, 5, 5, 5);

        imageView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    EventDragShadowBuilder shadowBuilder = new EventDragShadowBuilder(imageView, (int) event.getX(), (int) event.getY());
                    view.startDrag(null, shadowBuilder, view, 0);
                    view.setVisibility(View.INVISIBLE);

                    return true;
                }
                else if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    view.setVisibility(View.VISIBLE);
                    return true;
                }

                return false;
            }
        });


        return imageView;
    }

    public static TextView makeTextView(Context context, final CharSequence text)
    {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextColor(Color.WHITE);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(params);
        textView.setTextSize(28);
        textView.setPaintFlags(0);
        textView.setPadding(12, 0, 0, 0);

        textView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    EventDragShadowBuilder shadowBuilder = new EventDragShadowBuilder(view, (int) event.getX(), (int) event.getY());
                    view.startDrag(null, shadowBuilder, view, 0);
                    view.setVisibility(View.INVISIBLE);

                    return true;
                }
                else if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    view.setVisibility(View.VISIBLE);
                    return true;
                }

                return false;
            }
        });

        return textView;
    }

    static class EventDragShadowBuilder extends View.DragShadowBuilder
    {
        private int touchPointX, touchPointY;

        public EventDragShadowBuilder(View view, int touchPointX, int touchPointY)
        {
            super(view);
            this.touchPointX = touchPointX;
            this.touchPointY = touchPointY;
        }

        @Override
        public void onProvideShadowMetrics(@NonNull Point shadowSize, @NonNull Point shadowTouchPoint)
        {
            shadowSize.set(super.getView().getWidth(), super.getView().getHeight());
            shadowTouchPoint.set(touchPointX, touchPointY);
        }
    }

}
