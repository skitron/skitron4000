package com.capstone.sk.customui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.capstone.sk.R;

/**
 * Created by Aaron on 3/12/2015.
 */
public class CircularProgressBar extends View
{
    private Paint paint;
    private RectF bounds;
    private int color;
    private int max;
    private int progress;
    private float radius;

    public CircularProgressBar(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    public CircularProgressBar(Context context)
    {
        super(context);
        init(context, null, 0);
    }

    public CircularProgressBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public void init(Context context, AttributeSet attrs, int defStyle)
    {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircularProgressBar, 0, 0);

        int thickness;
        try
        {
            color = a.getColor(R.styleable.CircularProgressBar_p_color, Color.WHITE);
            radius = a.getDimensionPixelSize(R.styleable.CircularProgressBar_p_radius, 0);
            max = a.getInt(R.styleable.CircularProgressBar_p_max, 0);
            progress = a.getInt(R.styleable.CircularProgressBar_p_progress, 0);
            thickness = a.getInt(R.styleable.CircularProgressBar_p_thickness, 0);
        } finally
        {
            a.recycle();
        }

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(thickness);
        bounds = new RectF();
    }

    public void setMax(int max)
    {
        this.max = max;
        invalidate();
        requestLayout();
    }

    public void setProgress(int progress)
    {
        this.progress = progress;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        bounds.left = 20;
        bounds.right = getWidth() - 20;
        bounds.top = 20;
        bounds.bottom = getHeight() - 20;

        canvas.rotate(-90, getWidth() / 2, getHeight() / 2);
        canvas.drawArc(bounds, 0, 360 * ((float) progress / max), false, paint);
    }

    public void setRadius(float radius)
    {
        this.radius = radius;
        invalidate();
        requestLayout();
    }

    public int getMax()
    {
        return max;
    }

    public int getProgress()
    {
        return progress;
    }
}
