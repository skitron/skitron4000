package com.capstone.sk.customui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.capstone.sk.R;

/**
 * Created by Aaron on 3/12/2015.
 */
public class CircleView extends View
{
    private Paint paint;
    private float radius, baseRadius;

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    public CircleView(Context context)
    {
        super(context);
        init(context, null, 0);
    }

    public CircleView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public void init(Context context, AttributeSet attrs, int defStyle)
    {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleView, 0, 0);

        int color;
        try
        {
            color = a.getColor(R.styleable.CircleView_circle_color, Color.WHITE);
            radius = a.getDimensionPixelSize(R.styleable.CircleView_circle_radius, 0);
            baseRadius = a.getDimensionPixelSize(R.styleable.CircleView_circle_base_radius, 0);
        } finally
        {
            a.recycle();
        }

        paint.setColor(color);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        canvas.drawCircle(getWidth() / 2, getHeight() / 2, baseRadius + radius, paint);
    }

    public void setRadius(float radius)
    {
        this.radius = radius;
        invalidate();
        requestLayout();
    }
}
