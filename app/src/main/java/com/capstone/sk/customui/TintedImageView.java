package com.capstone.sk.customui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.capstone.sk.R;

/**
 * Used for ImageViews that are tint controlled with a selector.
 * This is for the Navigation Drawer icons so they tint correctly when pressed.
 */
public class TintedImageView extends ImageView
{
    private ColorStateList tint;

    public TintedImageView(Context context)
    {
        super(context);
    }

    public TintedImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public TintedImageView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle)
    {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TintedImageView, defStyle, 0);
        tint = a.getColorStateList(R.styleable.TintedImageView_tint);
        a.recycle();
    }

    @Override
    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (tint != null && tint.isStateful()) updateTintColor();
    }

    public void setColorFilter(ColorStateList tint)
    {
        this.tint = tint;
        super.setColorFilter(tint.getColorForState(getDrawableState(), 0));
    }

    private void updateTintColor()
    {
        int color = tint.getColorForState(getDrawableState(), 0);
        setColorFilter(color);
    }

}