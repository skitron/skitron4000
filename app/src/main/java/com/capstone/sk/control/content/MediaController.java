package com.capstone.sk.control.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;

/**
 * Created by Trent on 3/4/2015.
 */
public class MediaController
{
    private static final String ARTIST = "artist";
    private static final String TRACK = "track";
    private static final String ALBUM = "album";
    private static final String TAG = MediaController.class.getSimpleName();

    private AudioManager audioManager;
    private TelephonyManager telephonyManager;
    private Context context;
    private IntentFilter filter;
    private OnMediaChangedListener listener;
    private int currentSignalStrength;

    public MediaController(final Context context)
    {
        this.context = context;
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        currentSignalStrength = Integer.MAX_VALUE;

        filter = new IntentFilter();
        filter.addAction("com.android.music.metachanged");
        filter.addAction("com.android.music.playstatechanged");
        filter.addAction("com.android.music.playbackcomplete");
        filter.addAction("com.android.music.queuechanged");
        filter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);

        PhoneStateListener phoneStateListener = new PhoneStateListener()
        {
            @Override
            public void onSignalStrengthsChanged(SignalStrength signalStrength)
            {
                int strength;

                if (signalStrength.isGsm()) strength = 2 * signalStrength.getGsmSignalStrength() - 113;
                else strength = signalStrength.getCdmaDbm();

                if (listener != null && strength != currentSignalStrength)
                    listener.onSignalStrengthChanged(strength);

                currentSignalStrength = strength;
            }

        };

        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    public void setListener(OnMediaChangedListener listener)
    {
        this.listener = listener;
    }

    public void playPause()
    {
        Intent i = new Intent("com.android.music.musicservicecommand");
        if (audioManager.isMusicActive())
        {
            i.putExtra("command", "pause");
        }
        else
        {
            i.putExtra("command", "play");
        }
        context.sendBroadcast(i);
    }

    public void volumeChange(int change)
    {
        int stream, flags = 1;
        if (telephonyManager.getCallState() == TelephonyManager.CALL_STATE_OFFHOOK)
            stream = AudioManager.USE_DEFAULT_STREAM_TYPE;
        else stream = AudioManager.STREAM_MUSIC;

        audioManager.adjustSuggestedStreamVolume(change, stream, flags);
    }

    public void skip(String direction)
    {
        Intent i = new Intent("com.android.music.musicservicecommand");
        i.putExtra("command", direction);

        context.sendBroadcast(i);
    }

    public void answerCall()
    {
        Intent i = new Intent(Intent.ACTION_MEDIA_BUTTON);
        i.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));

        context.sendOrderedBroadcast(i, null);
    }

    public void endCall()
    {
        Intent i = new Intent(Intent.ACTION_MEDIA_BUTTON);
        i.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));

        context.sendOrderedBroadcast(i, null);
    }

    public void blockCall()
    {
        Intent i = new Intent(Intent.ACTION_MEDIA_BUTTON);
        i.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_CALL));
        context.sendOrderedBroadcast(i, null);
    }

    public void register()
    {
        context.registerReceiver(receiver, filter);
    }

    public void unregister()
    {
        context.unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (Intent.ACTION_NEW_OUTGOING_CALL.equals(intent.getAction()))
            {
                String callerNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                String callerName = getContactName(context, callerNumber);

                if (listener != null) listener.onCall(callerNumber, callerName);
            }
            // Song has changed
            else if (intent.hasExtra(ARTIST))
            {
                String artist = intent.hasExtra(ARTIST) ? intent.getStringExtra(ARTIST) : "No artist";
                String album = intent.hasExtra(ALBUM) ? intent.getStringExtra(ALBUM) : "No album";
                String track = intent.hasExtra(TRACK) ? intent.getStringExtra(TRACK) : "No track";

                if (listener != null) listener.onSongChanged(artist, album, track);
            }
        }
    };

    public String getContactName(Context context, String number)
    {
        String name = null;

        // define the columns I want the query to return
        String[] projection = new String[]{
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};

        // encode the phone number and build the filter URI
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        // query time
        Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);

        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }
            else
            {
                name = "Unknown";
            }
            cursor.close();
        }
        return name;
    }


    public interface OnMediaChangedListener
    {
        void onSongChanged(String artist, String album, String track);

        void onCall(String callerNumber, String callerName);

        void onSignalStrengthChanged(int strength);
    }
}