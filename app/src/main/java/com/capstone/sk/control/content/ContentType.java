package com.capstone.sk.control.content;

/**
 * Created by Aaron on 1/16/2015.
 */
public enum ContentType
{
    ELEVATION("elevation"), SPEED("speed"),
    CALLER_NAME("caller_name"), CALLER_NUMBER("caller_number"),
    TIME("time"),
    PHONE_BATTERY("phone_battery"), HUD_BATTERY("hud_battery"), NEW_MESSAGE("new_message"),
    CELL_SIGNAL("cell_signal"),
    TRACK_NAME("song_title"), ARTIST_NAME("artist_name"), ALBUM_NAME("album_name"),
    MESSAGE_FROM("message_from"), MESSAGE_TO("message_to"),
    MESSAGE_STATUS("message_status"), MESSAGE_TIME("message_time"),
    NONE("none");

    private String tag;

    ContentType(String tag)
    {
        this.tag = tag;
    }

    public static ContentType fromString(String text)
    {
        if (text != null)
        {
            for (ContentType contentType : ContentType.values())
            {
                if (text.equalsIgnoreCase(contentType.tag))
                {
                    return contentType;
                }
            }
        }

        // Default is to return none
        return NONE;
    }

    public String getTag()
    {
        return tag;
    }
}