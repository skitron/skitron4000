package com.capstone.sk.control.hud;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.capstone.sk.R;
import com.capstone.sk.control.button.ButtonFunctions;
import com.capstone.sk.control.content.ContentManager;
import com.capstone.sk.control.content.ContentType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Aaron on 11/22/2014.
 */
public class ViewController
{
    private static final String FILENAME = "saved_huds.json";

    private HUDView activeView;
    private ArrayList<HUDView> hudViews;
    private Context context;
    private ContentManager contentManager;
    private OnHudViewChangedListener listener;
    private int measuredWidth, measuredHeight;
    private int activeIndex;

    /**
     * Constructor for a view controller.
     * Inflates the hud view either from scratch or from a saved JSON model.
     *
     * @param context the context used to grab preferences
     */
    public ViewController(Context context)
    {
        this.context = context;
        hudViews = new ArrayList<>();
        contentManager = new ContentManager(context);
        activeIndex = 0;

        contentManager.setNewContentListener(new ContentManager.OnNewContentListener()
        {
            @Override
            public void onPendingContent(int type)
            {
                switch (type)
                {
                    case ContentManager.LOCATION:
                        updateActive();
                        break;
                    case ContentManager.CALL:

                        break;
                    case ContentManager.MUSIC:
                        updateActive();
                        break;
                    case ContentManager.MESSAGE:
                        updateActive();
                        break;
                }
            }
        });

        setupMeasurements();
        loadHUDS();

        if (hudViews.size() > 0) activeView = hudViews.get(activeIndex);
    }

    public void setOnHudViewChangedListener(OnHudViewChangedListener listener)
    {
        this.listener = listener;
    }

    /**
     * Calculates the correct measurements for the HUDS so they are displayed correctly.
     */
    private void setupMeasurements()
    {
        // Get the size of the screen
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Get the size of the action bar
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        final int actionBarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        // Get the size of the status bar
        int statusBarHeight = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);

        // Make the view lay itself out off screen, factoring in the size that appears in the editor
        // This size is half of the screen height that's available, so the action bar needs to be factored in
        // as well as the status bar
        measuredWidth = View.MeasureSpec.makeMeasureSpec(size.x, View.MeasureSpec.EXACTLY);
        measuredHeight = View.MeasureSpec.makeMeasureSpec((size.y - actionBarHeight - statusBarHeight) / 2, View.MeasureSpec.EXACTLY);
    }

    /**
     * Makes all the HUDViews reload values from file,
     * then re-measure and lay themselves out.
     */
    private void refreshViews()
    {
        contentManager.getDefaultContent();

        for (HUDView hudView : hudViews)
        {
            hudView.load();
            hudView.updateContent(contentManager.getDefaultContent());

            layoutView(hudView);
        }
    }

    private void layoutView(HUDView hudView)
    {
        RelativeLayout layout = (RelativeLayout) hudView.getView();
        layout.measure(measuredWidth, measuredHeight);
        layout.layout(0, 0, layout.getMeasuredWidth(), layout.getMeasuredHeight());
    }

    /**
     * Removes the current HUD view in the ViewController with a matching name, and resaves the new
     * HUDView. Otherwise, it creates a brand new view with the given name.
     *
     * @param hudView The HUDView to save
     */
    public void resaveView(HUDView hudView)
    {
        int pos = 0;
        boolean found = false;

        // It's a view that we've already created
        for (Iterator<HUDView> it = hudViews.iterator(); it.hasNext(); )
        {
            HUDView view = it.next();
            if (view.getHUDName().equals(hudView.getHUDName()))
            {
                it.remove();
                found = true;
                break;
            }

            pos++;
        }

        if (found) // The list already has the hudview
        {
            hudView.save();
            hudViews.add(pos, hudView);
        }
        else // The list doesn't have the hudview, so we add it
        {
            HUDView formed = create(hudView.getHUDName(), hudView.getTemplateName());
            hudViews.add(formed);
        }
    }

    public ArrayList<HUDView> getHUDViews()
    {
        refreshViews();
        return hudViews;
    }

    /**
     * Creates a view that represents the HUDView offscreen.
     * Android does none of the rendering of the view onscreen, so the values
     * for width and height of the view are based on the HUDEditor.
     *
     * @param hudName name of the view to create
     * @return a HUDView that corresponds to the hudID
     */
    private HUDView create(String hudName, String templateName)
    {
        // Render the active hud view off screen
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_hud_editor, null);
        RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.hud_scaled);

        // Does all the adding views and making sure the elements have the default content
        HUDView hudView = new HUDView(context, hudName, templateName);
        hudView.recreate(layout);
        hudView.load();

        // Make the layout measure and lay itself out
        // This normally happens when you inflate a view and let Android do all the measurement for you
        // but since this needs to happen offscreen, the measurement and layout are done manually
        layout.measure(measuredWidth, measuredHeight);
        layout.layout(0, 0, layout.getMeasuredWidth(), layout.getMeasuredHeight());

        return hudView;
    }

    /**
     * Saves all of the HUDViews to their respective files.
     */
    public void saveHUDS()
    {
        FileOutputStream fileOutputStream;
        JsonWriter writer = null;
        try
        {
            // Open the app-private file output with HUDSAVE and the value of the type
            fileOutputStream = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            writer = new JsonWriter(new OutputStreamWriter(fileOutputStream));

            // Start the JSON object
            writer.beginArray();

            for (HUDView hudView : hudViews)
            {
                writer.beginObject();

                writer.name("Name").value(hudView.getHUDName());
                writer.name("TemplateName").value(hudView.getTemplateName());

                writer.endObject();
            }

            writer.endArray();

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            // Close the writer in a finally block so that we
            // always close it even if exceptions happen in the try
            if (writer != null) try
            {
                writer.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Loads all of the HUDS from a file.
     * If the file doesn't exist then go to the template.
     *
     * This exists to make loading the right HUDS easier, and to tie names and template names
     * easier to files that may or may not exist.
     */
    private void loadHUDS()
    {
        hudViews.clear();

        InputStream inputStream = null;
        JsonReader reader = null;

        try
        {
            File saved = new File(context.getFilesDir(), FILENAME);

            // Try to see if we already have a file that's saved as Saved_HUDS
            if (saved.exists()) inputStream = new FileInputStream(saved);

                // If not then we open the default template
            else inputStream = context.getAssets().open("templates/default_huds.json");

        } catch (IOException e)
        {
            e.printStackTrace();
        }

        try
        {
            assert inputStream != null;
            reader = new JsonReader(new InputStreamReader(inputStream));

            reader.beginArray();

            while (reader.hasNext())
            {
                String readerName, hudName = "None", templateName = "None";

                reader.beginObject();

                while (reader.hasNext())
                {
                    readerName = reader.nextName();

                    if (readerName.equals("Name")) hudName = reader.nextString();
                    else if (readerName.equals("TemplateName")) templateName = reader.nextString();
                }

                reader.endObject();

                hudViews.add(create(hudName, templateName));
            }

            reader.endArray();


        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            if (reader != null) try
            {
                reader.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public HUDView getActiveHUDView()
    {
        if (activeView != null)
        {
            activeView.updateContent(contentManager.getDefaultContent());
            layoutView(activeView);
        }

        return activeView;
    }

    public void updateActive()
    {
        if (hasContent(ContentType.ELEVATION, ContentType.SPEED)) contentManager.getLocationController().startGPS();
        else contentManager.getLocationController().stopGPS();

        if (activeView != null)
        {
            activeView.updateContent(contentManager.getCurrentContent());
            layoutView(activeView);
            if (listener != null) listener.onChange(activeView);
        }
    }

    /**
     * Removes the given HUDView if it exists. Also deletes the save file associated with that HUDView.
     *
     * @param hudView The HUDView to remove
     */
    public void removeView(HUDView hudView)
    {
        for (Iterator<HUDView> iterator = hudViews.iterator(); iterator.hasNext(); )
        {
            if (iterator.next() == hudView)
            {
                context.deleteFile("HUDSAVE" + hudView.getHUDName().toUpperCase());
                iterator.remove();
            }
        }
    }

    /**
     * Returns the HUDView with the associated name
     *
     * @param name The name to search for
     * @return The HUDView if the name exists, and null otherwise
     */
    public HUDView getHUDViewForName(String name)
    {
        for (HUDView hudView : hudViews)
        {
            if (name.equals(hudView.getHUDName()))
            {
                return hudView;
            }
        }

        return null;
    }

    /**
     * Checks to see if the controller has a HUDView with the given name.
     *
     * @param hudName The name to check for
     * @return true if the name exists, false if it doesn't
     */
    public boolean hasName(String hudName)
    {
        for (HUDView hudView : hudViews)
        {
            if (hudView.getHUDName().equals(hudName))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Calls the button press associated with the button name. This is based off the active view's button press.
     *
     * @param buttonName The button name, one of greenShort, greenLong, yellowShort, yellowLong,
     *                   redShort, redLong, blueShort, blueLong.
     */
    public void buttonPress(String buttonName)
    {
        if (activeView == null) return;

        String functionName = activeView.getButtonPress(buttonName);

        if (!functionName.isEmpty())
        {
            ButtonFunctions.run(functionName, contentManager, this, context);
        }
    }

    /**
     * Determines if the active view's content contains all of the given content types.
     *
     * @param args Comma separated ContentTypes
     * @return True if all of the content types are found in the active view's content
     */
    public boolean hasContent(ContentType... args)
    {
        if (activeView != null)
        {
            ArrayList<ContentType> hudContent = activeView.getContentTypes();
            int num = 0;

            for (ContentType contentType : args)
            {
                if (hudContent.contains(contentType)) num++;
            }

            return num == args.length;
        }

        return false;

    }

    /**
     * Cycles in the given direction by changing the index. Since modulo can return a negative value,
     * if the value is negative then we should wrap to the end of the list.
     *
     * @param direction The direction (and coincidentally amount) to scroll by
     */
    public void cycleActiveHUDView(int direction)
    {
        if (hudViews.size() == 0) return;

        activeIndex = (activeIndex + direction) % hudViews.size();
        if (activeIndex < 0) activeIndex = hudViews.size() - 1;

        activeView = hudViews.get(activeIndex);

        if (hasContent(ContentType.CALLER_NAME, ContentType.CALLER_NUMBER) && hudViews.size() > 1)
            cycleActiveHUDView(direction);

        updateActive();
    }

    public void onDestroy()
    {
        contentManager.onDestroy();
        contentManager = null;
    }

    public ContentManager getContentManager()
    {
        return contentManager;
    }

    public void setActiveIndex(int activeIndex)
    {
        this.activeIndex = activeIndex;
    }

    public interface OnHudViewChangedListener
    {
        void onChange(HUDView activeView);
    }
}

