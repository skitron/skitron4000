package com.capstone.sk.control.content;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.capstone.sk.R;
import com.capstone.sk.helpers.Utils;
import com.capstone.sk.messaging.MessageController;
import com.capstone.sk.messaging.VoiceMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Aaron on 1/16/2015.
 *
 * Manages content for each of the elements.
 */
public class ContentManager
{
    private static final String TAG = ContentManager.class.getSimpleName();

    public static final int MUSIC = 1;
    public static final int LOCATION = 2;
    public static final int CALL = 3;
    public static final int MESSAGE = 4;

    private Bundle currentContent, defaultContent;
    private Context context;
    private Handler periodicUpdateHandler;
    private Intent batteryStatus;
    private LocationController locationController;
    private MediaController mediaController;
    private MessageController messageController;
    private OnNewContentListener newContentListener;

    private String[] unitLabels;
    private float[] unitConversions;

    public ContentManager(Context context)
    {
        this.context = context;

        currentContent = new Bundle();
        defaultContent = new Bundle();
        periodicUpdateHandler = new Handler();

        unitLabels = new String[2];
        unitConversions = new float[2];

        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        batteryStatus = context.registerReceiver(null, filter);

        locationController = new LocationController(context);
        mediaController = new MediaController(context);
        messageController = new MessageController(context);

        // Update the battery every 10 minutes
        periodicUpdateHandler.post(batteryRunnable);

        locationController.setOnLocationChangedListener(new LocationController.OnLocationChangedListener()
        {
            @Override
            public void onLocationChanged(Location location)
            {
                if (location != null)
                {
                    updateContent(ContentType.ELEVATION, String.format("%4.0f %s", location.getAltitude() * unitConversions[1], unitLabels[1]));
                    updateContent(ContentType.SPEED, String.format("%5.1f %s", location.getSpeed() * unitConversions[0], unitLabels[0]));

                    if (newContentListener != null)
                        newContentListener.onPendingContent(LOCATION);
                }
            }
        });

        mediaController.setListener(new MediaController.OnMediaChangedListener()
        {
            @Override
            public void onSongChanged(String artist, String album, String track)
            {
                updateContent(ContentType.ALBUM_NAME, album);
                updateContent(ContentType.ARTIST_NAME, artist);
                updateContent(ContentType.TRACK_NAME, track);

                if (newContentListener != null)
                    newContentListener.onPendingContent(MUSIC);
            }

            @Override
            public void onCall(String callerNumber, String callerName)
            {
                updateContent(ContentType.CALLER_NAME, callerName);
                updateContent(ContentType.CALLER_NUMBER, callerNumber);

                if (newContentListener != null)
                    newContentListener.onPendingContent(CALL);
            }

            @Override
            public void onSignalStrengthChanged(int strength)
            {
                if (strength > 0) updateContent(ContentType.CELL_SIGNAL, R.drawable.ic_network_none);

                if (strength <= -100) updateContent(ContentType.CELL_SIGNAL, R.drawable.ic_network_0);
                else if (strength <= -96) updateContent(ContentType.CELL_SIGNAL, R.drawable.ic_network_1);
                else if (strength <= -90) updateContent(ContentType.CELL_SIGNAL, R.drawable.ic_network_2);
                else if (strength <= -85) updateContent(ContentType.CELL_SIGNAL, R.drawable.ic_network_3);
                else updateContent(ContentType.CELL_SIGNAL, R.drawable.ic_network_4);
            }
        });

        messageController.setNotificationListener(new MessageController.NotificationListener()
        {
            @Override
            public void onCreateNotification()
            {
                updateContent(ContentType.NEW_MESSAGE, R.drawable.ic_new_message);

                if (newContentListener != null)
                    newContentListener.onPendingContent(MESSAGE);
            }

            @Override
            public void onDismissNotification()
            {
                updateContent(ContentType.NEW_MESSAGE, R.drawable.none);
                if (newContentListener != null)
                    newContentListener.onPendingContent(MESSAGE);
            }
        });
    }

    /**
     * Fills in the current content with default values.
     */
    private void fillCurrentContent()
    {
        for (ContentType contentType : ContentType.values())
        {
            String tag = contentType.getTag();

            switch (contentType)
            {
                case ELEVATION:
                    currentContent.putString(tag, String.format("%4.0f %s", 0 * unitConversions[1], unitLabels[1]));
                    break;
                case SPEED:
                    currentContent.putString(tag, String.format("%5.1f %s", 0 * unitConversions[0], unitLabels[0]));
                    break;
                case TIME:
                    currentContent.putString(tag, "12:34 PM");
                    break;
                case CALLER_NAME:
                    currentContent.putString(tag, "------------");
                    break;
                case CALLER_NUMBER:
                    currentContent.putString(tag, "------------");
                    break;
                case TRACK_NAME:
                    currentContent.putString(tag, "No track");
                    break;
                case ARTIST_NAME:
                    currentContent.putString(tag, "No artist");
                    break;
                case ALBUM_NAME:
                    currentContent.putString(tag, "No album");
                    break;
                case PHONE_BATTERY:
                    currentContent.putInt(tag, R.drawable.ic_battery_80);
                    break;
                case CELL_SIGNAL:
                    currentContent.putInt(tag, R.drawable.ic_network_1);
                    break;
                case MESSAGE_STATUS:
                    currentContent.putString(tag, "Waiting for input...");
                    break;
                case MESSAGE_FROM:
                    currentContent.putString(tag, "Message From:");
                    break;
                case MESSAGE_TO:
                    currentContent.putString(tag, "Message To:");
                    break;
                case MESSAGE_TIME:
                    currentContent.putString(tag, "");
                    break;
                default:
                    break;
            }
        }
    }

    public void updateContent(ContentType contentType, String value)
    {
        currentContent.putString(contentType.getTag(), value);
    }

    public void updateContent(ContentType contentType, int value)
    {
        currentContent.putInt(contentType.getTag(), value);
    }

    /**
     * Updates the content associated with a message when a message is loaded or cycled through.
     *
     * @param message The message with properties that correspond to content types
     */
    public void updateMessageContent(VoiceMessage message)
    {
        updateContent(ContentType.MESSAGE_FROM, "Message From: " + message.getFrom());
        updateContent(ContentType.MESSAGE_TIME, (!message.isMessageHeard() ? "New - " : "") +
                "Sent " + message.getDelayFrom(System.currentTimeMillis()));
    }

    public Bundle getCurrentContent()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm a", Locale.US);
        currentContent.putString(ContentType.TIME.getTag(), sdf.format(new Date()));

        return currentContent;
    }

    /**
     * Update and return the default content that is displayed in the HUDEditorFragment.
     *
     * @return A Bundle of the current content, after being updated
     */
    public Bundle getDefaultContent()
    {
        for (ContentType contentType : ContentType.values())
        {
            String tag = contentType.getTag();

            switch (contentType)
            {
                case ELEVATION:
                    defaultContent.putString(tag, String.format("%4.0f %s", 1609 * unitConversions[1], unitLabels[1]));
                    break;
                case SPEED:
                    defaultContent.putString(tag, String.format("%5.1f %s", 15 * unitConversions[0], unitLabels[0]));
                    break;
                case TIME:
                    defaultContent.putString(tag, "12:34 PM");
                    break;
                case CALLER_NAME:
                    defaultContent.putString(tag, "Trent Begin");
                    break;
                case CALLER_NUMBER:
                    defaultContent.putString(tag, "504-867-5309");
                    break;
                case TRACK_NAME:
                    defaultContent.putString(tag, "Never Gonna Give You Up");
                    break;
                case ARTIST_NAME:
                    defaultContent.putString(tag, "Rick Astley");
                    break;
                case ALBUM_NAME:
                    defaultContent.putString(tag, "Whenever You Need Somebody");
                    break;
                case PHONE_BATTERY:
                    defaultContent.putInt(tag, R.drawable.ic_battery_20);
                    break;
                case CELL_SIGNAL:
                    defaultContent.putInt(tag, R.drawable.ic_network_1);
                    break;
                case MESSAGE_FROM:
                    defaultContent.putString(tag, "Message From: Trent");
                    break;
                case MESSAGE_TO:
                    defaultContent.putString(tag, "Message To: Aaron");
                    break;
                case MESSAGE_TIME:
                    defaultContent.putString(tag, "2 hours, 30 min ago");
                    break;
                case MESSAGE_STATUS:
                    defaultContent.putString(tag, "Message received.");
                    break;
                case NEW_MESSAGE:
                    defaultContent.putInt(tag, R.drawable.ic_new_message);
                    break;
                default:
                    break;
            }
        }

        return defaultContent;
    }


    public MediaController getMediaController()
    {
        return mediaController;
    }

    public MessageController getMessageController()
    {
        return messageController;
    }

    public void setNewContentListener(OnNewContentListener newContentListener)
    {
        this.newContentListener = newContentListener;
    }

    private Runnable batteryRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            float batteryPct = (level / (float) scale) * 100;

            if (batteryPct <= 20) updateContent(ContentType.PHONE_BATTERY, R.drawable.ic_battery_20);
            else if (batteryPct <= 30) updateContent(ContentType.PHONE_BATTERY, R.drawable.ic_battery_30);
            else if (batteryPct <= 50) updateContent(ContentType.PHONE_BATTERY, R.drawable.ic_battery_50);
            else if (batteryPct <= 65) updateContent(ContentType.PHONE_BATTERY, R.drawable.ic_battery_60);
            else if (batteryPct <= 85) updateContent(ContentType.PHONE_BATTERY, R.drawable.ic_battery_80);
            else if (batteryPct <= 100) updateContent(ContentType.PHONE_BATTERY, R.drawable.ic_battery_90);

            Log.d(TAG, "Battery percent is " + batteryPct);

            periodicUpdateHandler.postDelayed(batteryRunnable, 10 * 60 * 1000);
        }
    };

    public LocationController getLocationController()
    {
        return locationController;
    }

    public void onDestroy()
    {
        mediaController.unregister();
        locationController.stopGPS();
        messageController.onDestroy();

        messageController = null;
        mediaController = null;
        locationController = null;
    }

    /**
     * Code that goes here is any information that needs to be pulled from the settings menu.
     */
    public void onResume()
    {
        unitLabels[0] = Utils.getUnitStringFromKey(context, Utils.SPEED_UNITS);
        unitLabels[1] = Utils.getUnitStringFromKey(context, Utils.ELEVATION_UNITS);

        unitConversions[0] = Utils.getConversionFactorFromKey(context, "key_speed_units", R.array.speed_unit_factors);
        unitConversions[1] = Utils.getConversionFactorFromKey(context, "key_elevation_units", R.array.elevation_unit_factors);

        if (!Utils.getBooleanPref(context, "key_location_on")) locationController.stopGPS();

        fillCurrentContent();

        messageController.register();
        mediaController.register();
    }

    public interface OnNewContentListener
    {
        void onPendingContent(int type);
    }
}
