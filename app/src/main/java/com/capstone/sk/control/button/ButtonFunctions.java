package com.capstone.sk.control.button;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.capstone.sk.control.content.ContentManager;
import com.capstone.sk.control.content.ContentType;
import com.capstone.sk.control.hud.ViewController;
import com.capstone.sk.helpers.SharedPrefs;
import com.capstone.sk.messaging.AudioController;
import com.capstone.sk.messaging.AudioListener;
import com.capstone.sk.messaging.MessageController;
import com.capstone.sk.messaging.VoiceMessage;

import java.util.HashMap;

/**
 * Created by Trent on 3/4/2015.
 */
public class ButtonFunctions
{
    private static HashMap<String, Function> functionMap = new HashMap<>();

    private static final String VOLUME_UP = "Volume Up";
    private static final String VOLUME_DOWN = "Volume Down";
    private static final String PLAY_PAUSE = "Play/Pause";
    private static final String SKIP_FORWARD = "Skip Forward";
    private static final String SKIP_BACKWARD = "Skip Backward";
    private static final String HUD_FORWARD = "HUD Cycle Forward";
    private static final String HUD_BACKWARD = "HUD Cycle Backward";
    private static final String MESSAGE_RECORD_TOGGLE = "Message Record Toggle";
    private static final String MESSAGE_SEND = "Message Send";
    private static final String MESSAGE_NAME_FORWARD = "Message Name Forward";
    private static final String MESSAGE_NAME_BACKWARD = "Message Name Backward";
    private static final String MESSAGE_CYCLE_FORWARD = "Message Cycle Forward";
    private static final String MESSAGE_CYCLE_BACKWARD = "Message Cycle Backward";
    private static final String MESSAGE_PLAYBACK = "Message Playback";

    private static final String NONE = "None";

    static
    {
        /**
         * Increases the volume by 1 step.
         */
        functionMap.put(VOLUME_UP, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                cm.getMediaController().volumeChange(1);
            }
        });

        /**
         * Decreases the volume by 1 step.
         */
        functionMap.put(VOLUME_DOWN, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                cm.getMediaController().volumeChange(-1);
            }
        });

        /**
         * Toggles playing/pausing the currently playing music.
         */
        functionMap.put(PLAY_PAUSE, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                cm.getMediaController().playPause();
            }
        });

        /**
         * Skips the currently playing song and goes to the next.
         */
        functionMap.put(SKIP_FORWARD, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                cm.getMediaController().skip("next");
            }
        });

        /**
         * Restarts the currently playing song if more than 30 seconds has elapsed,
         * otherwise goes to the previously playing song.
         */
        functionMap.put(SKIP_BACKWARD, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                cm.getMediaController().skip("previous");
            }
        });

        /**
         * Cycles the active hud view forward by 1 step.
         */
        functionMap.put(HUD_FORWARD, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                vc.cycleActiveHUDView(1);
            }
        });

        /**
         * Cycles the active hud view backwards by 1 step.
         */
        functionMap.put(HUD_BACKWARD, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                vc.cycleActiveHUDView(-1);
            }
        });

        /**
         * Toggles recording a message on and off.
         */
        functionMap.put(MESSAGE_RECORD_TOGGLE, new Function()
        {
            @Override
            public void run(final ContentManager cm, final ViewController vc, Context context)
            {
                AudioController audioController = cm.getMessageController().getAudioController();

                audioController.setAudioListener(new AudioListener()
                {
                    public void onStartRecord()
                    {
                        Log.d("ButtFunc", "Start record.");
                        cm.updateContent(ContentType.MESSAGE_STATUS, "Message recording...");
                        vc.updateActive();
                    }

                    @Override
                    public void onEndRecord()
                    {
                        Log.d("ButtFunc", "End record.");
                        cm.updateContent(ContentType.MESSAGE_STATUS, "Message finished recording");
                        vc.updateActive();
                    }
                });


                if (!audioController.isRecording()) audioController.startRecording();
                else audioController.stopRecording();
            }
        });

        /**
         * Sends the currently recorded message.
         */
        functionMap.put(MESSAGE_SEND, new Function()
        {
            @Override
            public void run(final ContentManager cm, final ViewController vc, Context context)
            {
                MessageController messageController = cm.getMessageController();
                AudioController ac = messageController.getAudioController();

                SharedPreferences preferences = SharedPrefs.getSharedPreferences(context);
                String accountName = preferences.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);

                ac.stopRecording();
                ac.stopReplaying();

                String to = messageController.getName(0);
                if (to != null)
                {
                    messageController.sendMessage(new MessageController.MessageSendCallback()
                    {
                        @Override
                        public void onMessageSend(MessageController.SendError errorCode)
                        {
                            switch (errorCode)
                            {
                                case SEND_ERROR_NO_MESSAGE:
                                    cm.updateContent(ContentType.MESSAGE_STATUS, "No message recorded.");
                                    break;
                                case SEND_SUCCESSFUL:
                                    cm.updateContent(ContentType.MESSAGE_STATUS, "Message sent successfully.");
                                    break;
                                case SEND_UNSUCCESSFUL:
                                    cm.updateContent(ContentType.MESSAGE_STATUS, "Message failed to send.");
                                    break;
                            }

                            vc.updateActive();
                        }
                    }, to, accountName);

                    cm.updateContent(ContentType.MESSAGE_STATUS, "Attempting to send message...");
                    vc.updateActive();
                }
            }
        });

        /**
         * Scrolls through a list of names to send messages to.
         */
        functionMap.put(MESSAGE_NAME_BACKWARD, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                MessageController mc = cm.getMessageController();

                String name = mc.getName(-1);
                if (name != null)
                {
                    cm.updateContent(ContentType.MESSAGE_TO, "Message To: " + name);
                    vc.updateActive();
                }
            }
        });

        functionMap.put(MESSAGE_NAME_FORWARD, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                MessageController mc = cm.getMessageController();

                String name = mc.getName(1);
                if (name != null)
                {
                    cm.updateContent(ContentType.MESSAGE_TO, "Message To: " + name);
                    vc.updateActive();
                }
            }
        });

        /**
         * Plays the currently selected message.
         */
        functionMap.put(MESSAGE_PLAYBACK, new Function()
        {
            @Override
            public void run(final ContentManager cm, final ViewController vc, Context context)
            {
                MessageController mc = cm.getMessageController();
                AudioController ac = mc.getAudioController();
                final VoiceMessage message = mc.getMessage(0);

                ac.setAudioListener(new AudioListener()
                {
                    @Override
                    public void onStartReplay(int duration)
                    {
                        cm.updateContent(ContentType.MESSAGE_STATUS, "Playing message...");
                        cm.updateContent(ContentType.MESSAGE_TIME, "Sent " + message.getDelayFrom(System.currentTimeMillis()));
                        vc.updateActive();
                    }

                    @Override
                    public void onEndReplay()
                    {
                        cm.updateContent(ContentType.MESSAGE_STATUS, "Done playing message.");
                        vc.updateActive();
                    }
                });

                if (message != null)
                {
                    ac.playMessage(message);
                }
            }
        });

        /**
         * Cycles backward to the previous message.
         */
        functionMap.put(MESSAGE_CYCLE_BACKWARD, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                VoiceMessage message = cm.getMessageController().getMessage(1);

                if (message != null)
                {
                    cm.updateMessageContent(message);
                    vc.updateActive();
                }
            }
        });

        /**
         * Cycles forward to the next message.
         */
        functionMap.put(MESSAGE_CYCLE_FORWARD, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
                VoiceMessage message = cm.getMessageController().getMessage(-1);

                if (message != null)
                {
                    cm.updateMessageContent(message);
                    vc.updateActive();
                }
            }
        });

        functionMap.put(NONE, new Function()
        {
            @Override
            public void run(ContentManager cm, ViewController vc, Context context)
            {
            }
        });

    }

    void answerCall()
    {

    }

    void endCall()
    {
    }

    void rejectCall()
    {
    }

    void toggleGPS()
    {

    }

    public static void run(String functionName, ContentManager contentManager, ViewController viewController, Context context)
    {
        if (functionMap.containsKey(functionName))
            functionMap.get(functionName).run(contentManager, viewController, context);
    }

    public interface Function
    {
        void run(final ContentManager cm, final ViewController vc, Context context);
    }
}
