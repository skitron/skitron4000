package com.capstone.sk.control.button;

import android.util.JsonReader;
import android.util.JsonWriter;

import com.capstone.sk.helpers.JsonSaveable;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Aaron on 3/7/2015.
 */
public class ButtonInformation implements JsonSaveable, Serializable
{
    private HashMap<String, String> functionMap;

    public ButtonInformation()
    {
        functionMap = new HashMap<>();
    }

    @Override
    public void saveToJson(JsonWriter writer) throws IOException
    {
        writer.name(getClass().getSimpleName());

        writer.beginObject();

        for (String key : functionMap.keySet())
        {
            writer.name(key).value(functionMap.get(key));
        }

        writer.endObject();
    }

    @Override
    public void loadFromJson(JsonReader reader) throws IOException
    {
        reader.beginObject();

        while (reader.hasNext())
        {
            functionMap.put(reader.nextName(), reader.nextString());
        }

        reader.endObject();
    }

    public String getFunction(String key)
    {
        return functionMap.get(key);
    }

    public void setFunction(String key, String function)
    {
        functionMap.put(key, function);
    }
}
