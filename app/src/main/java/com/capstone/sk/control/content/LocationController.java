package com.capstone.sk.control.content;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.capstone.sk.helpers.Utils;

/**
 * Created by Aaron on 11/22/2014.
 */
public class LocationController implements LocationListener
{
    private static final int DEFAULT_INTERVAL = 1000;
    private static final String TAG = LocationController.class.getSimpleName();

    private LocationManager locationManager;
    private Location bestLocation;
    private Context context;
    private OnLocationChangedListener onLocationChangedListener;
    private boolean isUpdating;

    public LocationController(Context context)
    {
        this.context = context;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        bestLocation = locationManager.getLastKnownLocation(getBestProviderName());
        isUpdating = false;
    }

    /**
     * Get provider name.
     *
     * @return Name of best suiting provider.
     */
    private String getBestProviderName()
    {
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM); // Chose your desired power consumption level.
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
        criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
        criteria.setAltitudeRequired(true); // Choose if you use altitude.
        criteria.setBearingRequired(false); // Choose if you use bearing.
        criteria.setCostAllowed(false); // Choose if this provider can waste money :-)

        // Provide your criteria and flag enabledOnly that tells
        // LocationManager only to return active providers.
        String providerName = locationManager.getBestProvider(criteria, true);
        Log.d(TAG, "Best provider is currently: " + providerName);
        return providerName;
    }

    /**
     * Starts the gps request at a 1 second interval.
     */
    public void startGPS()
    {
        if (!isUpdating && Utils.getBooleanPref(context, "key_location_on"))
        {
            locationManager.requestLocationUpdates(getBestProviderName(), DEFAULT_INTERVAL, 0, this);
            isUpdating = true;
        }
    }

    public void stopGPS()
    {
        isUpdating = false;
        locationManager.removeUpdates(this);
    }

    public Location getBestLocation()
    {
        return bestLocation;
    }

    @Override
    public void onLocationChanged(Location location)
    {
        bestLocation = location;
        if (onLocationChangedListener != null) onLocationChangedListener.onLocationChanged(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    @Override
    public void onProviderEnabled(String provider)
    {

    }

    @Override
    public void onProviderDisabled(String provider)
    {

    }

    public void setOnLocationChangedListener(OnLocationChangedListener onLocationChangedListener)
    {
        this.onLocationChangedListener = onLocationChangedListener;
    }

    public interface OnLocationChangedListener
    {
        void onLocationChanged(Location location);
    }

}