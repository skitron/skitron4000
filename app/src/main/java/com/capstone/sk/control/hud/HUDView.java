package com.capstone.sk.control.hud;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.JsonWriter;
import android.view.View;
import android.widget.RelativeLayout;

import com.capstone.sk.control.button.ButtonInformation;
import com.capstone.sk.control.content.ContentType;
import com.capstone.sk.editor.elements.HUDBackgroundElement;
import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.elements.HUDIconBankElement;
import com.capstone.sk.editor.elements.HUDImageElement;
import com.capstone.sk.editor.elements.HUDTextElement;
import com.capstone.sk.helpers.ViewFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Created by Aaron on 6/9/2014.
 *
 * The class where all the action happens.
 */
public class HUDView
{
    private static final String FILENAME = "_hud_save.json";
    private static final String TEMPLATE = "_hud_template.json";

    private String hudName, templateName;
    private Context context;
    private ArrayList<HUDElement> hudElements;
    private HUDBackgroundElement backgroundElement;
    private ButtonInformation buttonInformation;
    private RelativeLayout rootView;

    public HUDView(Context context, String name, String templateName)
    {
        this.context = context;
        this.hudName = name;
        this.templateName = templateName;

        hudElements = new ArrayList<>();
        buttonInformation = new ButtonInformation();
    }

    /**
     * Add a new HUDElement to the HUDView. Called externally when elements are added
     * after recreation.
     *
     * @param newElement The element to add
     */
    public void addElement(HUDElement newElement)
    {
        hudElements.add(newElement);

        if (rootView != null && !(newElement instanceof HUDBackgroundElement))
            rootView.addView(newElement.getView());
    }

    /**
     * Remove the specified element from the HUDView. Also removes it from the root view
     * that backs the HUDView.
     *
     * @param removedElement The element to remove
     */
    public void removeElement(HUDElement removedElement)
    {
        if (hudElements.contains(removedElement))
        {
            rootView.removeView(removedElement.getView());
            hudElements.remove(removedElement);
        }
    }

    public void updateContent(Bundle content)
    {
        for (HUDElement element : hudElements)
            element.updateContent(content);
    }

    /**
     * Saves all the elements in the HUDView in JSON format to an app-private files directory.
     */
    public void save()
    {
        FileOutputStream fileOutputStream;
        JsonWriter writer = null;
        try
        {
            // Open the app-private file output with HUDSAVE and the value of the type
            fileOutputStream = context.openFileOutput(hudName.toLowerCase() + FILENAME, Context.MODE_PRIVATE);
            writer = new JsonWriter(new OutputStreamWriter(fileOutputStream));

            // Start the JSON object
            writer.beginObject();

            writer.name("HUDName").value(hudName);
            buttonInformation.saveToJson(writer);

            for (HUDElement element : hudElements)
            {
                // Write each element to JSON format in its own object
                element.saveToJson(writer);
            }

            writer.endObject();

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            // Close the writer in a finally block so that we
            // always close it even if exceptions happen in the try
            if (writer != null) try
            {
                writer.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }

    }

    /**
     * Saves a HUDView in JSON format to a non-app private directory so we can look a them.
     */
    public void saveExternal()
    {
        FileOutputStream fileOutputStream;
        JsonWriter writer = null;
        try
        {
            // Use the external files dir instead of the app-private directory
//            File output = new File(context.getExternalFilesDir(null), hudName.toLowerCase() + FILENAME);
            File output = new File(Environment.getExternalStorageDirectory(), "SkiTron/messages/" + hudName.toLowerCase() + ".json");
            fileOutputStream = new FileOutputStream(output);
            writer = new JsonWriter(new OutputStreamWriter(fileOutputStream));

            writer.beginObject();

            writer.name("HUDName").value(hudName);
            buttonInformation.saveToJson(writer);

            for (HUDElement element : hudElements)
                element.saveToJson(writer);

            writer.endObject();

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            if (writer != null) try
            {
                writer.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Loads a HUDView using the private file.
     * If it cannot find the file then it resorts to using a template.
     */
    public void load()
    {
        InputStream inputStream = null;
        JsonReader reader = null;

        try
        {
            File saved = new File(context.getFilesDir(), hudName.toLowerCase() + FILENAME);

            // Try to see if we already have a file that's saved as HUDSAVE + value
            if (saved.exists()) inputStream = new FileInputStream(saved);

                // If not then we open the default template
            else inputStream = context.getAssets().open("templates/" + templateName.toLowerCase() + TEMPLATE);

        } catch (IOException e)
        {
            e.printStackTrace();
        }

        try
        {
            assert inputStream != null;
            reader = new JsonReader(new InputStreamReader(inputStream));
            String name = "";
            int i = 0;

            // Start the big object
            reader.beginObject();

            while (reader.hasNext())
            {
                if (reader.peek() == JsonToken.NAME) name = reader.nextName();

                switch (name)
                {
                    case "HUDName":
                        name = reader.nextString();
                        if (hudName.isEmpty()) hudName = name;
                        break;
                    case "ButtonInformation":
                        buttonInformation.loadFromJson(reader);
                        break;
                    default:
                        hudElements.get(i).loadFromJson(reader);
                        i++;
                        break;
                }
            }

            reader.endObject();

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            if (reader != null) try
            {
                reader.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Given a background, all of the elements are created and added to the rootView according
     * to the JSON formatted file in memory. If no such file exists then a template is used.
     */
    public void recreate(RelativeLayout rootView)
    {
        this.rootView = rootView;

        InputStream inputStream = null;

        try
        {
            File saved = new File(context.getFilesDir(), hudName.toLowerCase() + FILENAME);

            // See if the file has been saved already
            if (saved.exists()) inputStream = new FileInputStream(saved);

                // If not use the template
            else inputStream = context.getAssets().open("templates/" + templateName.toLowerCase() + TEMPLATE);

        } catch (IOException e)
        {
            e.printStackTrace();
        }

        JsonReader reader = null;
        try
        {
            assert inputStream != null;
            reader = new JsonReader(new InputStreamReader(inputStream));
            String name = "";

            reader.beginObject();

            while (reader.hasNext())
            {
                if (reader.peek() == JsonToken.NAME)
                    name = reader.nextName();

                switch (name)
                {
                    case "HUDName":
                        // If the HUD doesn't already have a name (like ones added not from the file)
                        // then we load in the name
                        if (hudName.isEmpty())
                        {
                            hudName = reader.nextString();
                        }
                        // Otherwise we have to swallow the name so the reader doesn't choke on it the next go around
                        else
                        {
                            reader.nextString();
                        }
                        break;
                    case "HUDBackgroundElement":
                        HUDBackgroundElement backgroundElement = new HUDBackgroundElement(rootView);
                        backgroundElement.loadFromJson(reader);
                        addElement(backgroundElement);

                        // The background element acts at the view from which the bitmap is taken
                        // This is saved later for easier access
                        this.backgroundElement = backgroundElement;
                        break;
                    case "HUDImageElement":
                        HUDImageElement imageElement = new HUDImageElement(ViewFactory.makeClickableImageView(context));
                        imageElement.loadFromJson(reader);
                        addElement(imageElement);
                        break;
                    case "HUDTextElement":
                        HUDTextElement textElement = new HUDTextElement(ViewFactory.makeTextView(context, "TEXT"), context);
                        textElement.loadFromJson(reader);
                        addElement(textElement);
                        break;
                    case "HUDIconBankElement":
                        HUDIconBankElement iconBankElement = new HUDIconBankElement(ViewFactory.makeIconBankLayout(context), context);
                        iconBankElement.recreateFromJson(reader);
                        addElement(iconBankElement);
                        break;
                    case "ButtonInformation":
                        buttonInformation.loadFromJson(reader);
                    default:
                        break;
                }
            }

            reader.endObject();

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            if (reader != null) try
            {
                reader.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }

    }

    /**
     * Get all of the content types associated with the HUDView.
     *
     * @return A list of content types
     */
    public ArrayList<ContentType> getContentTypes()
    {
        ArrayList<ContentType> contentTypes = new ArrayList<>();

        for (HUDElement element : hudElements)
        {
            if (!contentTypes.contains(element.getContentType()))
                contentTypes.add(element.getContentType());
        }

        return contentTypes;
    }

    public String getButtonPress(String button)
    {
        return buttonInformation.getFunction(button);
    }

    public View getView()
    {
        return backgroundElement.getView();
    }

    public HUDBackgroundElement getBackgroundElement()
    {
        return backgroundElement;
    }

    public String getTemplateName()
    {
        return templateName;
    }

    public ButtonInformation getButtonInformation()
    {
        return buttonInformation;
    }

    public void setButtonInformation(ButtonInformation buttonInformation)
    {
        this.buttonInformation = buttonInformation;
    }

    public String getHUDName()
    {
        return hudName;
    }
}
