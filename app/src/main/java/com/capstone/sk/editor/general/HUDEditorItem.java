package com.capstone.sk.editor.general;

import com.capstone.sk.editor.elements.HUDBackgroundElement;
import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.elements.HUDIconBankElement;
import com.capstone.sk.editor.elements.HUDImageElement;
import com.capstone.sk.editor.elements.HUDTextElement;

/**
 * Created by Aaron on 5/21/2014.
 *
 * Abstract class that is the parent for all of the HUDEditorItems.
 * It implements and provides default actions for the EditorItem interface.
 *
 * This way, subclasses only need to override the actions and updates they care about.
 *
 * Each element has a type, selected element and text content.
 */
public abstract class HUDEditorItem implements EditorItem
{
    public static final int VIEW_TYPE_COLOR = 0;
    public static final int VIEW_TYPE_SLIDER = 1;
    public static final int VIEW_TYPE_DIALOG = 2;

    private boolean isEnabled = true;
    private HUDElement selectedElement;
    private String textContent;

    public abstract int getViewType();

    public abstract void onEditorUpdate(HUDElement element);

    public boolean isEnabled()
    {
        return isEnabled;
    }

    public void setEnabled(boolean enabled)
    {
        isEnabled = enabled;
    }

    public void applyActionFrom(EditorItem element)
    {
        selectedElement.acceptAction(element);
    }

    public HUDElement getSelectedElement()
    {
        return selectedElement;
    }

    public void setSelectedElement(HUDElement element)
    {
        this.selectedElement = element;
    }

    public String getTextContent()
    {
        return textContent;
    }

    public void setTextContent(String textContent)
    {
        this.textContent = textContent;
    }

    @Override
    public void updateTextElement(HUDTextElement textElement)
    {
        setEnabled(false);
    }

    @Override
    public void updateImageElement(HUDImageElement imageElement)
    {
        setEnabled(false);
    }

    @Override
    public void updateBackgroundElement(HUDBackgroundElement backgroundElement)
    {
        setEnabled(false);
    }

    @Override
    public void updateIconBankElement(HUDIconBankElement iconBankElement)
    {
        setEnabled(false);
    }


    @Override
    public void actionTextElement(HUDTextElement textElement)
    {
    }

    @Override
    public void actionImageElement(HUDImageElement imageElement)
    {
    }

    @Override
    public void actionBackgroundElement(HUDBackgroundElement backgroundElement)
    {
    }

    @Override
    public void actionIconBankElement(HUDIconBankElement iconBankElement)
    {
    }
}
