package com.capstone.sk.editor.elements;

import android.graphics.Color;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.view.View;

import com.capstone.sk.control.content.ContentType;
import com.capstone.sk.editor.general.EditorItem;

import java.io.IOException;

/**
 * Created by Aaron on 8/11/2014.
 */
public class HUDBackgroundElement extends HUDElement
{
    private int backgroundColor;

    public HUDBackgroundElement(View view)
    {
        super(view);
        this.backgroundColor = Color.BLACK;
    }

    public int getBackgroundColor()
    {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor)
    {
        this.backgroundColor = backgroundColor;
        getView().setBackgroundColor(backgroundColor);
    }

    @Override
    public void saveToJson(JsonWriter writer) throws IOException
    {
        writer.name(getClass().getSimpleName());

        writer.beginObject();

        writer.name("content").value(getContentType().getTag());
        writer.name("bgcolor").value(backgroundColor);

        writer.endObject();
    }

    @Override
    public void loadFromJson(JsonReader reader) throws IOException
    {
        reader.beginObject();

        while (reader.hasNext())
        {
            String name = reader.nextName();

            if (name.equals("bgcolor")) setBackgroundColor(reader.nextInt());
            else if (name.equals("content")) setContentType(ContentType.fromString(reader.nextString()));
            else reader.skipValue();
        }

        reader.endObject();
    }

    @Override
    public void updateContent(Bundle content)
    {
        // Do nothing, no content to update
    }

    @Override
    public void acceptUpdate(EditorItem editorItem)
    {
        editorItem.updateBackgroundElement(this);
    }

    @Override
    public void acceptAction(EditorItem editorItem)
    {
        editorItem.actionBackgroundElement(this);
    }
}
