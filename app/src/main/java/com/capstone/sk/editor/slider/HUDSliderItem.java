package com.capstone.sk.editor.slider;

import android.content.Context;
import android.widget.TextView;

import com.capstone.sk.editor.general.HUDEditorItem;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

/**
 * Created by Aaron on 5/21/2014.
 */
public abstract class HUDSliderItem extends HUDEditorItem
{
    private DiscreteSeekBar seekBar;
    private int newProgress;
    private Context context;

    public HUDSliderItem(Context context)
    {
        this.context = context;
    }

    public void setupText(TextView textView)
    {
        textView.setText(getTextContent());
    }

    public void setupSlider(DiscreteSeekBar seekBar)
    {
        this.seekBar = seekBar;
        onEditorUpdate(getSelectedElement());

        seekBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener()
        {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int progress, boolean fromUser)
            {
                newProgress = progress;

                if (fromUser)
                {
                    getSelectedElement().acceptAction(HUDSliderItem.this);
                }
            }
        });
    }


    @Override
    public int getViewType()
    {
        return HUDEditorItem.VIEW_TYPE_SLIDER;
    }

    public void setMin(int min)
    {
        seekBar.setMin(min);
    }

    public int getProgress()
    {
        return newProgress;
    }

    public void setProgress(int progress)
    {
        seekBar.setProgress(progress);
    }

    public int getMax()
    {
        return seekBar.getMax();
    }

    public void setMax(int max)
    {
        seekBar.setMax(max);
    }
}
