package com.capstone.sk.editor.general;

import android.content.Context;

import com.capstone.sk.R;
import com.capstone.sk.adapters.HUDEditorAdapter;
import com.capstone.sk.editor.color.HUDColorBackgroundItem;
import com.capstone.sk.editor.color.HUDColorForegroundItem;
import com.capstone.sk.editor.dialog.HUDDialogFontItem;
import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.slider.HUDSliderSizeItem;

import java.util.ArrayList;

/**
 * Created by Aaron on 10/17/2014.
 */
public class HUDEditor
{
    private ArrayList<HUDEditorItem> hudItems, enabledItems;
    private HUDElement currentSelected;

    public HUDEditor(Context context, HUDElement firstSelected)
    {
        int[] colors = context.getResources().getIntArray(R.array.color_choices);
        hudItems = new ArrayList<>();
        enabledItems = new ArrayList<>();

        hudItems.add(new HUDColorBackgroundItem(context, colors, colors[0]));
        hudItems.add(new HUDColorForegroundItem(context, colors, colors[0]));
        hudItems.add(new HUDSliderSizeItem(context));
        hudItems.add(new HUDDialogFontItem(context));

        // Set the default selected item
        for (HUDEditorItem item : hudItems)
            item.setSelectedElement(firstSelected);
    }

    /**
     * Takes the selected element and updates the list of editor items.
     * The adapter is given enabled items to update itself.
     *
     * @param element
     * @param adapter
     */
    public void setSelectedElement(HUDElement element, HUDEditorAdapter adapter)
    {
        if (element != currentSelected)
        {
            enabledItems.clear();

            for (HUDEditorItem item : hudItems)
            {
                // Set the selected element for the item and then
                // make the item update itself (enabled/disabled), internal values
                item.setSelectedElement(element);
                item.onEditorUpdate(element);

                // Add the enabled items to the list
                if (item.isEnabled()) enabledItems.add(item);
            }

            // Set the adapter's items and make it refresh the views
            adapter.setHUDItems(enabledItems);
            adapter.notifyDataSetChanged();

            currentSelected = element;
        }
    }

    public ArrayList<HUDEditorItem> getHUDItems()
    {
        return hudItems;
    }

    public HUDElement getSelectedItem()
    {
        return currentSelected;
    }
}
