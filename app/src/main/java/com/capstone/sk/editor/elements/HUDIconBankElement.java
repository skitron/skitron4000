package com.capstone.sk.editor.elements;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.capstone.sk.R;
import com.capstone.sk.control.content.ContentType;
import com.capstone.sk.editor.general.EditorItem;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Aaron on 1/16/2015.
 */
public class HUDIconBankElement extends HUDElement
{
    private ArrayList<HUDImageElement> imageElements;
    private LinearLayout layout;
    private Context context;
    private int mainColor, orientation;

    public HUDIconBankElement(LinearLayout layout, Context context)
    {
        super(layout);
        this.layout = layout;
        this.context = context;

        // Defaults
        imageElements = new ArrayList<>();
        mainColor = Color.WHITE;
        orientation = LinearLayout.HORIZONTAL;
    }

    @Override
    public void saveToJson(JsonWriter writer) throws IOException
    {
        // The outside name
        writer.name(getClass().getSimpleName());

        // The outside object
        writer.beginObject();

        writer.name("maincolor").value(mainColor);
        writer.name("orientation").value(orientation);

        for (HUDImageElement imageElement : imageElements)
        {
            // Write the HUDImageElement
            writer.name(imageElement.getClass().getSimpleName());

            writer.beginObject();

            writer.name("content").value(imageElement.getContentType().getTag());
            writer.name("filter").value(imageElement.getColorFilter());

            writer.endObject();
        }

        writer.endObject();

    }

    @Override
    public void loadFromJson(JsonReader reader) throws IOException
    {
        int loadIndex = 0;

        reader.beginObject();

        // Read in the properties for the HUDImageElements
        while (reader.hasNext())
        {
            // Swallow the name of the HUDImageElement
            String name = reader.nextName();
            if (name.equals("maincolor")) setMainColor(reader.nextInt());
            else if (name.equals("orientation")) setOrientation(reader.nextInt());
            else
            {
                HUDImageElement imageElement = imageElements.get(loadIndex);
                loadImageElementFromJson(imageElement, reader);
                loadIndex++;
            }
        }

        reader.endObject();
    }

    @Override
    public void updateContent(Bundle content)
    {
        for (HUDImageElement imageElement : imageElements)
            imageElement.updateContent(content);
    }

    public void recreateFromJson(JsonReader reader) throws IOException
    {
        reader.beginObject();

        // Read in the properties for the HUDImageElements
        while (reader.hasNext())
        {
            // Swallow the name of the HUDImageElement
            String name = reader.nextName();
            if (name.equals("maincolor")) setMainColor(reader.nextInt());
            else if (name.equals("orientation")) setOrientation(reader.nextInt());
            else
            {
                HUDImageElement imageElement = new HUDImageElement(makeIconImageView());
                loadImageElementFromJson(imageElement, reader);

                layout.addView(imageElement.getView());
                imageElements.add(imageElement);
            }
        }

        reader.endObject();
    }

    private void loadImageElementFromJson(HUDImageElement imageElement, JsonReader reader) throws IOException
    {
        reader.beginObject();

        while (reader.hasNext())
        {
            String name = reader.nextName();

            if (name.equals("content")) imageElement.setContentType(ContentType.fromString(reader.nextString()));
            else if (name.equals("filter")) imageElement.setColorFilter(reader.nextInt());
        }

        reader.endObject();
    }

    public void setOrientation(int orientation)
    {
        this.orientation = orientation;
        layout.setOrientation(orientation);
    }

    public int getMainColor()
    {
        return mainColor;
    }

    public void setMainColor(int mainColor)
    {
        this.mainColor = mainColor;

        for (HUDImageElement imageElement : imageElements)
            imageElement.setColorFilter(mainColor);
    }

    public void addIcon(ContentType contentType)
    {
        HUDImageElement imageElement = new HUDImageElement(makeIconImageView());
        imageElement.setContentType(contentType);
        layout.addView(imageElement.getView());
        imageElements.add(imageElement);
    }

    private ImageView makeIconImageView()
    {
        final ImageView imageView = new ImageView(context);
        int padding = context.getResources().getDimensionPixelSize(R.dimen.icon_bank_padding);
        int size = context.getResources().getDimensionPixelSize(R.dimen.icon_bank_icon_size);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
        params.gravity = Gravity.CENTER;
        imageView.setLayoutParams(params);
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setColorFilter(mainColor);

        return imageView;
    }

    @Override
    public void acceptUpdate(EditorItem editorItem)
    {
        editorItem.updateIconBankElement(this);
    }

    @Override
    public void acceptAction(EditorItem editorItem)
    {
        editorItem.actionIconBankElement(this);
    }
}
