package com.capstone.sk.editor.color;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.colorpicker.ColorPickerDialog;
import com.android.colorpicker.ColorPickerSwatch;
import com.android.colorpicker.ColorStateDrawable;
import com.capstone.sk.editor.general.HUDEditorItem;

/**
 * Created by Aaron on 5/21/2014.
 */
public abstract class HUDColorItem extends HUDEditorItem
{
    private ImageView colorButtonView;
    private int[] colors;
    private int selectedColor;
    private Activity parent;

    public HUDColorItem(Context context, int[] colors, int selectedColor)
    {
        this.colors = colors;
        this.selectedColor = selectedColor;
        this.parent = (Activity) context;
    }

    public void setupText(TextView textView)
    {
        textView.setText(getTextContent());
    }

    /**
     * Set up the color button with a color and OnClick listener.
     * This is called from the HUDEditorAdapter.
     *
     * @param colorButtonView the view to setup
     */
    public void setupButton(ImageView colorButtonView)
    {
        this.colorButtonView = colorButtonView;
        redrawColor(selectedColor);

        colorButtonView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ColorPickerDialog colorPickerDialog = ColorPickerDialog.newInstance(com.android.colorpicker.R.string.color_picker_default_title, colors, selectedColor, 4, ColorPickerDialog.SIZE_SMALL);

                //Implement listener to get selected color value
                colorPickerDialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener()
                {
                    @Override
                    public void onColorSelected(int color)
                    {
                        redrawColor(color);

                        // Visitor pattern for the "action", or the thing that happens
                        // when the color changes
                        getSelectedElement().acceptAction(HUDColorItem.this);
                    }

                });

                colorPickerDialog.show(parent.getFragmentManager(), "cal");
            }
        });
    }

    /**
     * Make the button change its color and redraw.
     * Has a special case for transparency.
     *
     * @param newColor color to draw
     */
    public void redrawColor(int newColor)
    {
        this.selectedColor = newColor;

        if (newColor != Color.TRANSPARENT)
        {
            Drawable[] colorDrawable = new Drawable[]{parent.getResources().getDrawable(com.android.colorpicker.R.drawable.color_picker_swatch)};
            colorButtonView.setImageDrawable(new ColorStateDrawable(colorDrawable, newColor));
        }
        else
        {
            Drawable[] colorDrawable = new Drawable[]{parent.getResources().getDrawable(com.android.colorpicker.R.drawable.transparent_color_swatch)};
            colorButtonView.setImageDrawable(new LayerDrawable(colorDrawable));
        }
    }

    public int getSelectedColor()
    {
        return selectedColor;
    }

    @Override
    public int getViewType()
    {
        return HUDEditorItem.VIEW_TYPE_COLOR;
    }
}
