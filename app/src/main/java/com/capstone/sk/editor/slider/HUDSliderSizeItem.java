package com.capstone.sk.editor.slider;

import android.content.Context;

import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.elements.HUDImageElement;
import com.capstone.sk.editor.elements.HUDTextElement;

/**
 * Created by Aaron on 8/13/2014.
 */
public class HUDSliderSizeItem extends HUDSliderItem
{
    public HUDSliderSizeItem(Context context)
    {
        super(context);
    }

    @Override
    public void onEditorUpdate(HUDElement element)
    {
        element.acceptUpdate(this);
    }

    @Override
    public void updateTextElement(HUDTextElement textElement)
    {
        setTextContent("Text Size");
        setEnabled(true);
        setMax(64);
        setMin(16);
        setProgress((int) textElement.getCurrentTextSize());
    }

    @Override
    public void updateImageElement(HUDImageElement imageElement)
    {
        setEnabled(true);
        setTextContent("Image Size");
        setMax(100);
        setMin(50);
        setProgress((int) (imageElement.getScale() * 100));
    }

    @Override
    public void actionTextElement(HUDTextElement textElement)
    {
        textElement.setCurrentTextSize(getProgress());
    }

    @Override
    public void actionImageElement(HUDImageElement imageElement)
    {
        float percent = ((float) (getProgress())) / getMax();
        imageElement.setScale(percent);
    }
}
