package com.capstone.sk.editor.color;

import android.content.Context;

import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.elements.HUDIconBankElement;
import com.capstone.sk.editor.elements.HUDImageElement;
import com.capstone.sk.editor.elements.HUDTextElement;

/**
 * Created by Aaron on 8/13/2014.
 */
public class HUDColorForegroundItem extends HUDColorItem
{

    public HUDColorForegroundItem(Context parent, int[] colors, int defaultColor)
    {
        super(parent, colors, defaultColor);
    }

    @Override
    public void onEditorUpdate(HUDElement element)
    {
        element.acceptUpdate(this);
    }

    @Override
    public void updateTextElement(HUDTextElement textElement)
    {
        setTextContent("Text Color");
        setEnabled(true);
        redrawColor(textElement.getTextColor());
    }

    @Override
    public void updateImageElement(HUDImageElement imageElement)
    {
        setEnabled(true);
        redrawColor(imageElement.getColorFilter());
        setTextContent("Image Color");
    }

    @Override
    public void updateIconBankElement(HUDIconBankElement iconBankElement)
    {
        setEnabled(true);
        setTextContent("Icon Color");
        redrawColor(iconBankElement.getMainColor());
    }

    @Override
    public void actionTextElement(HUDTextElement textElement)
    {
        textElement.setTextColor(getSelectedColor());
    }

    @Override
    public void actionImageElement(HUDImageElement imageElement)
    {
        imageElement.setColorFilter(getSelectedColor());
    }

    @Override
    public void actionIconBankElement(HUDIconBankElement iconBankElement)
    {
        iconBankElement.setMainColor(getSelectedColor());
    }

}
