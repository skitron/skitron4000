package com.capstone.sk.editor.dialog;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.capstone.sk.editor.general.HUDEditorItem;
import com.capstone.sk.helpers.Typefaces;

/**
 * Created by Aaron on 10/15/2014.
 */
public abstract class HUDDialogItem extends HUDEditorItem
{
    private Context context;
    private Button button;
    private String buttonContent;

    public HUDDialogItem(Context context)
    {
        this.context = context;
        buttonContent = "";
    }

    public abstract void onButtonPress();

    public void setupButton(Button button)
    {
        this.button = button;
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onButtonPress();
            }
        });
        button.setText(buttonContent.toUpperCase());
        button.setTypeface(Typefaces.get(getContext(), "fonts/" + buttonContent + ".ttf"));
    }

    public void setupText(TextView textView)
    {
        textView.setText(getTextContent());
    }

    public void setButtonContent(String buttonContent)
    {
        this.buttonContent = buttonContent;
    }

    public Context getContext()
    {
        return context;
    }

    public Button getButton()
    {
        return button;
    }

    @Override
    public int getViewType()
    {
        return HUDEditorItem.VIEW_TYPE_DIALOG;
    }
}
