package com.capstone.sk.editor.general;

import com.capstone.sk.editor.elements.HUDBackgroundElement;
import com.capstone.sk.editor.elements.HUDIconBankElement;
import com.capstone.sk.editor.elements.HUDImageElement;
import com.capstone.sk.editor.elements.HUDTextElement;

/**
 * Created by Aaron on 1/5/2015.
 *
 * This interface allows the HUDEditorItems to define what they do
 * when a new element is selected and when their values are updated.
 *
 * The Visitor pattern is employed to reduce the usage of instanceof and
 * make the code more readable.
 */
public interface EditorItem
{
    void updateTextElement(HUDTextElement textElement);

    void updateImageElement(HUDImageElement imageElement);

    void updateBackgroundElement(HUDBackgroundElement backgroundElement);

    void updateIconBankElement(HUDIconBankElement iconBankElement);

    void actionTextElement(HUDTextElement textElement);

    void actionImageElement(HUDImageElement imageElement);

    void actionBackgroundElement(HUDBackgroundElement backgroundElement);

    void actionIconBankElement(HUDIconBankElement iconBankElement);

}
