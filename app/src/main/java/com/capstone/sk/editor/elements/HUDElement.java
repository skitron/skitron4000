package com.capstone.sk.editor.elements;

import android.os.Bundle;
import android.view.View;

import com.capstone.sk.control.content.ContentType;
import com.capstone.sk.editor.general.Element;
import com.capstone.sk.helpers.JsonSaveable;

/**
 * Created by Aaron on 6/14/2014.
 *
 * The abstract class that all HUDElements are children of.
 * These children must implement a way to save and restore themselves from JSON.
 *
 * As part of the spec for elements, all setter methods that deal with a view
 * must both set the field for the class and set the view's value as well.
 *
 * Unfortunately all the subclasses must implement the Element interface,
 * as the interface specifically needs to pass in HUDText, Image, or Background elements
 * and not the parent class.
 */
public abstract class HUDElement implements Element, JsonSaveable
{
    private View view;
    private ContentType contentType;

    public HUDElement(View view)
    {
        this.view = view;
        this.contentType = ContentType.NONE;

        view.setTag(this);
    }

    public abstract void updateContent(Bundle content);

    public View getView()
    {
        return view;
    }

    public void setView(View view)
    {
        this.view = view;
    }

    public ContentType getContentType()
    {
        return contentType;
    }

    public void setContentType(ContentType contentType)
    {
        this.contentType = contentType;
    }
}
