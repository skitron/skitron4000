package com.capstone.sk.editor.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capstone.sk.R;
import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.elements.HUDTextElement;
import com.capstone.sk.helpers.Typefaces;

/**
 * Created by Aaron on 1/9/2015.
 */
public class HUDDialogFontItem extends HUDDialogItem
{
    private String fontName;
    private DialogListAdapter adapter;

    public HUDDialogFontItem(Context context)
    {
        super(context);
        adapter = new DialogListAdapter(context, R.array.fonts);
    }

    @Override
    public void onButtonPress()
    {
        new MaterialDialog.Builder(getContext())
                .title("Select a Font")
                .adapter(adapter)
                .callback(new MaterialDialog.ButtonCallback()
                {
                    @Override
                    public void onPositive(MaterialDialog dialog)
                    {
                        fontName = adapter.getSelectedItem();
                        getSelectedElement().acceptAction(HUDDialogFontItem.this);
                    }
                })
                .positiveText("CHOOSE")
                .show();
    }

    @Override
    public void onEditorUpdate(HUDElement element)
    {
        setTextContent("Font");
        element.acceptUpdate(this);
    }

    @Override
    public void actionTextElement(HUDTextElement textElement)
    {
        textElement.setFont(fontName);
        getButton().setText(fontName.toUpperCase());
        getButton().setTypeface(Typefaces.get(getContext(), "fonts/" + fontName + ".ttf"));
    }

    @Override
    public void updateTextElement(HUDTextElement textElement)
    {
        adapter.setSelectedPosition(adapter.getPositionForName(textElement.getFont()));

        setEnabled(true);
        setButtonContent(adapter.getSelectedItem());
    }

    protected class DialogListAdapter extends BaseAdapter
    {
        private LayoutInflater inflater;
        private String[] content;
        private int selectedPosition;

        public DialogListAdapter(Context context, int arrayResID)
        {
            content = context.getResources().getStringArray(arrayResID);
            inflater = LayoutInflater.from(context);
        }

        public void setSelectedPosition(int position)
        {
            this.selectedPosition = position;
        }

        public String getSelectedItem()
        {
            return content[selectedPosition];
        }

        public int getPositionForName(String name)
        {
            for (int i = 0; i < content.length; i++)
            {
                if (content[i].equals(name)) return i;
            }

            return 0;
        }

        @Override
        public int getCount()
        {
            return content.length;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            DialogViewHolder holder;

            if (convertView == null)
            {
                holder = new DialogViewHolder();

                convertView = inflater.inflate(R.layout.dialog_list_item, parent, false);
                holder.textView = (TextView) convertView.findViewById(R.id.dialog_text);
                holder.radioButton = (RadioButton) convertView.findViewById(R.id.dialog_radio_button);

                holder.radioButton.setChecked(position == selectedPosition);
                holder.radioButton.setTag(position);
                holder.radioButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        selectedPosition = (Integer) v.getTag();
                        notifyDataSetInvalidated();
                    }
                });

                convertView.setTag(holder);
            }
            else
            {
                holder = (DialogViewHolder) convertView.getTag();
            }

            holder.textView.setText(content[position]);
            holder.radioButton.setChecked(position == selectedPosition);
            holder.textView.setTypeface(Typefaces.get(getContext(), "fonts/" + content[position] + ".ttf"));

            return convertView;
        }


        private class DialogViewHolder
        {
            TextView textView;
            RadioButton radioButton;
        }
    }

}
