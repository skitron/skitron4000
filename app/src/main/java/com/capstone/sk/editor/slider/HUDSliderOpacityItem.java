package com.capstone.sk.editor.slider;

import android.content.Context;
import android.view.View;

import com.capstone.sk.editor.elements.HUDBackgroundElement;
import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.elements.HUDImageElement;
import com.capstone.sk.editor.elements.HUDTextElement;

/**
 * Created by Aaron on 8/17/2014.
 */
public class HUDSliderOpacityItem extends HUDSliderItem
{
    public HUDSliderOpacityItem(Context context)
    {
        super(context);

        setTextContent("Opacity");

        setMax(100);
        setMin(0);
    }

    @Override
    public void onEditorUpdate(HUDElement element)
    {
        element.acceptUpdate(this);
    }

    @Override
    public void updateTextElement(HUDTextElement textElement)
    {
        this.setEnabled(true);
    }

    @Override
    public void updateImageElement(HUDImageElement imageElement)
    {
        this.setEnabled(true);
    }

    @Override
    public void updateBackgroundElement(HUDBackgroundElement backgroundElement)
    {
        this.setEnabled(true);
    }

    @Override
    public void actionTextElement(HUDTextElement textElement)
    {
        View view = textElement.getView();
        view.setAlpha((float) getProgress() / getMax());
    }

}
