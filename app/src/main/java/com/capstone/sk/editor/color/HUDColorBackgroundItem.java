package com.capstone.sk.editor.color;

import android.content.Context;

import com.capstone.sk.editor.elements.HUDBackgroundElement;
import com.capstone.sk.editor.elements.HUDElement;

/**
 * Created by Aaron on 8/13/2014.
 */
public class HUDColorBackgroundItem extends HUDColorItem
{
    public HUDColorBackgroundItem(Context parent, int[] colors, int defaultColor)
    {
        super(parent, colors, defaultColor);
    }

    @Override
    public void onEditorUpdate(HUDElement element)
    {
        setTextContent("Background Color");
        element.acceptUpdate(this);
    }

    @Override
    public void actionBackgroundElement(HUDBackgroundElement backgroundElement)
    {
        backgroundElement.setBackgroundColor(getSelectedColor());
    }

    @Override
    public void updateBackgroundElement(HUDBackgroundElement backgroundElement)
    {
        setEnabled(true);
        redrawColor(backgroundElement.getBackgroundColor());
    }


}
