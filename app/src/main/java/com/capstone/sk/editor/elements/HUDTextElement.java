package com.capstone.sk.editor.elements;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.widget.TextView;

import com.capstone.sk.control.content.ContentType;
import com.capstone.sk.editor.general.EditorItem;
import com.capstone.sk.helpers.Typefaces;

import java.io.IOException;

/**
 * Created by Aaron on 8/11/2014.
 */
public class HUDTextElement extends HUDElement
{
    private int textColor, backgroundColor;
    private float currentTextSize;
    private TextView textView;
    private String fontName;
    private Context context;

    public HUDTextElement(TextView view, Context context)
    {
        super(view);
        this.textView = view;
        this.textColor = Color.WHITE;
        this.backgroundColor = Color.TRANSPARENT;
        this.context = context;
        this.fontName = "Roboto-Light";
        this.currentTextSize = view.getTextSize();
    }

    @Override
    public void saveToJson(JsonWriter writer) throws IOException
    {
        writer.name(getClass().getSimpleName());

        writer.beginObject();

        writer.name("content").value(getContentType().getTag());
        writer.name("textcolor").value(textColor);
        writer.name("bgcolor").value(backgroundColor);
        writer.name("x").value((double) textView.getX());
        writer.name("y").value((double) textView.getY());
        writer.name("ctextsize").value((double) currentTextSize);
        writer.name("font").value(fontName);

        writer.endObject();
    }

    @Override
    public void loadFromJson(JsonReader reader) throws IOException
    {
        reader.beginObject();

        while (reader.hasNext())
        {
            String name = reader.nextName();

            if (name.equals("content")) setContentType(ContentType.fromString(reader.nextString()));
            else if (name.equals("textcolor")) setTextColor(reader.nextInt());
            else if (name.equals("bgcolor")) setBackgroundColor(reader.nextInt());
            else if (name.equals("x")) setX((float) reader.nextDouble());
            else if (name.equals("y")) setY((float) reader.nextDouble());
            else if (name.equals("ctextsize")) setCurrentTextSize((float) reader.nextDouble());
            else if (name.equals("font")) setFont(reader.nextString());
            else reader.skipValue();
        }

        reader.endObject();
    }

    @Override
    public void updateContent(Bundle content)
    {
        if (content != null)
        {
            textView.setText(content.getString(getContentType().getTag()));
        }
    }

    public String getFont()
    {
        return fontName;
    }

    public void setFont(String fontName)
    {
        this.fontName = fontName;
        textView.setTypeface(Typefaces.get(context, "fonts/" + fontName + ".ttf"));
    }

    public void setX(float x)
    {
        textView.setX(x);
    }

    public void setY(float y)
    {
        textView.setY(y);
    }

    public int getTextColor()
    {
        return textColor;
    }

    public void setTextColor(int textColor)
    {
        this.textColor = textColor;
        textView.setTextColor(textColor);
    }

    public void setBackgroundColor(int backgroundColor)
    {
        this.backgroundColor = backgroundColor;
        textView.setBackgroundColor(backgroundColor);
    }

    public float getCurrentTextSize()
    {
        return currentTextSize;
    }

    public void setCurrentTextSize(float currentTextSize)
    {
        this.currentTextSize = currentTextSize;
        textView.setTextSize(currentTextSize);
    }

    @Override
    public void acceptUpdate(EditorItem editorItem)
    {
        editorItem.updateTextElement(this);
    }

    @Override
    public void acceptAction(EditorItem editorItem)
    {
        editorItem.actionTextElement(this);
    }


}
