package com.capstone.sk.editor.elements;

import android.graphics.Color;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.widget.ImageView;

import com.capstone.sk.control.content.ContentType;
import com.capstone.sk.editor.general.EditorItem;

import java.io.IOException;

/**
 * Created by Aaron on 8/11/2014.
 */
public class HUDImageElement extends HUDElement
{
    private float scale;
    private ImageView imageView;
    private int colorFilter;
    private int imageID;

    public HUDImageElement(ImageView view)
    {
        super(view);
        this.imageView = view;
        this.colorFilter = Color.WHITE;
        this.scale = 1.0f;
    }

    public void setImageID(int id)
    {
        this.imageID = id;
        imageView.setImageResource(id);
    }

    public float getScale()
    {
        return scale;
    }

    public void setScale(float scale)
    {
        this.scale = scale;
        imageView.setScaleX(scale);
        imageView.setScaleY(scale);
    }

    public void setX(float x)
    {
        imageView.setX(x);
    }

    public void setY(float y)
    {
        imageView.setY(y);
    }

    public int getColorFilter()
    {
        return colorFilter;
    }

    public void setColorFilter(int color)
    {
        this.colorFilter = color;
        imageView.setColorFilter(color);
    }

    @Override
    public void saveToJson(JsonWriter writer) throws IOException
    {
        writer.name(getClass().getSimpleName());

        writer.beginObject();

        writer.name("content").value(getContentType().getTag());
        writer.name("x").value((double) imageView.getX());
        writer.name("y").value((double) imageView.getY());
        writer.name("scale").value(scale);
        writer.name("filter").value(colorFilter);

        writer.endObject();
    }

    @Override
    public void loadFromJson(JsonReader reader) throws IOException
    {
        reader.beginObject();

        while (reader.hasNext())
        {
            String name = reader.nextName();

            if (name.equals("x")) setX((float) reader.nextDouble());
            else if (name.equals("content")) setContentType(ContentType.fromString(reader.nextString()));
            else if (name.equals("y")) setY((float) reader.nextDouble());
            else if (name.equals("scale")) setScale((float) reader.nextDouble());
            else if (name.equals("filter")) setColorFilter(reader.nextInt());
            else reader.skipValue();
        }

        reader.endObject();
    }

    @Override
    public void updateContent(Bundle content)
    {
        if (content != null)
        {
            setImageID(content.getInt(getContentType().getTag()));
        }
    }

    @Override
    public void acceptUpdate(EditorItem editorItem)
    {
        editorItem.updateImageElement(this);
    }

    @Override
    public void acceptAction(EditorItem editorItem)
    {
        editorItem.actionImageElement(this);
    }

}
