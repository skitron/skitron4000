package com.capstone.sk.editor.slider;

import android.content.Context;

import com.capstone.sk.editor.elements.HUDBackgroundElement;
import com.capstone.sk.editor.elements.HUDElement;
import com.capstone.sk.editor.elements.HUDImageElement;
import com.capstone.sk.editor.elements.HUDTextElement;

/**
 * Created by Aaron on 8/13/2014.
 */
public class HUDSliderRotationItem extends HUDSliderItem
{
    public HUDSliderRotationItem(Context context)
    {
        super(context);
        setTextContent("Rotation");
    }

    @Override
    public void onEditorUpdate(HUDElement element)
    {
        element.acceptUpdate(this);
    }

    @Override
    public void updateTextElement(HUDTextElement textElement)
    {
        this.setEnabled(true);
        setMax(360);
        setMin(0);
        setProgress(0);
    }

    @Override
    public void updateImageElement(HUDImageElement imageElement)
    {
        this.setEnabled(false);
    }

    @Override
    public void updateBackgroundElement(HUDBackgroundElement backgroundElement)
    {
        this.setEnabled(true);
        setMax(360);
        setMin(0);
        setProgress(0);
    }
}
