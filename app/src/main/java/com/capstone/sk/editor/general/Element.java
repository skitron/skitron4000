package com.capstone.sk.editor.general;

/**
 * Created by Aaron on 1/5/2015.
 *
 * This is implemented by HUDElements.
 * Using the visitor pattern, they must accept an update or action.
 */
public interface Element
{
    void acceptUpdate(EditorItem editorItem);

    void acceptAction(EditorItem editorItem);
}
