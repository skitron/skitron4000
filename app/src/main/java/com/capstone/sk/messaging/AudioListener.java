package com.capstone.sk.messaging;

/**
 * Created by Aaron on 4/26/2015.
 */
public abstract class AudioListener
{
    public AudioListener()
    {
        super();
    }

    @Override
    protected final Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    @Override
    public final boolean equals(Object o)
    {
        return super.equals(o);
    }

    @Override
    protected final void finalize() throws Throwable
    {
        super.finalize();
    }

    @Override
    public final int hashCode()
    {
        return super.hashCode();
    }

    @Override
    public final String toString()
    {
        return super.toString();
    }

    public void onStartRecord()
    {

    }

    public void onEndRecord()
    {
    }

    public void onRecordStep(int messageLength, int messageAmplitude)
    {

    }

    public void onStartReplay(int duration)
    {
    }

    public void onEndReplay()
    {
    }

    public void onReplayStep(int messageLength)
    {
    }
}
