package com.capstone.sk.messaging;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import com.capstone.sk.R;
import com.capstone.sk.helpers.Utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Aaron on 4/26/2015.
 */
public class AudioController
{
    private static final String TAG = AudioController.class.getSimpleName();
    private static final String AUDIO_FILE_FORMAT = ".mp4";
    private static final String MESSAGE_DIRECTORY = "SkiTron/messages";
    private static final String TEMP_MESSAGE_FILENAME = MESSAGE_DIRECTORY + "/im-";

    private static final int PROGRESS_INCREMENT = 50;
    public static final int MAX_MESSAGE_DURATION = 15 * 1000; // 15 second message limit
    private final Context context;

    private MediaRecorder recorder;
    private MediaPlayer player;
    private Handler audioHandler;
    private AudioListener audioListener;
    private boolean replaying, recording;
    private int messageLength, replayLength, fileIndex;

    public AudioController(Context context)
    {
        // Check to see if the SkiTron/messages directory exists on the SD card
        // If it doesn't, create it
        this.context = context;

        audioHandler = new Handler();

        File messageDir = new File(Environment.getExternalStorageDirectory(), MESSAGE_DIRECTORY);
        if (!messageDir.exists())
        {
            boolean success = messageDir.mkdirs();
            if (!success) Log.d(TAG, "Failed to create message output directory.");
        }

    }

    public boolean isReplaying()
    {
        return replaying;
    }

    public boolean isRecording()
    {
        return recording;
    }

    public void setAudioListener(AudioListener listener)
    {
        this.audioListener = listener;
    }

    /**
     * Replays the message from the standard temporary file that is overwritten with each record.
     */
    public void startReplaying()
    {
        File file = new File(Environment.getExternalStorageDirectory(), TEMP_MESSAGE_FILENAME + AUDIO_FILE_FORMAT);

        if (!file.exists())
            return;

        replaying = true;
        player = new MediaPlayer();

        try
        {
            player.setDataSource(file.getAbsolutePath());
            player.prepareAsync();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        // Start playing once prepared
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mp)
            {
                mp.start();
                replayLength = 0;

                if (audioListener != null) audioListener.onStartReplay(mp.getDuration());
                audioHandler.post(replayRunnable);
            }
        });

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                stopReplaying();
            }
        });
    }

    /**
     * Stops the current replay and calls the onEndReplay method.
     */
    public void stopReplaying()
    {
        if (replaying)
        {
            replaying = false;

            if (player != null)
            {
                if (player.isPlaying()) player.stop();
                player.reset();
                player.release();
            }

            audioHandler.removeCallbacks(replayRunnable);
            if (audioListener != null) audioListener.onEndReplay();
        }
    }

    /**
     * Starts a recording using .mp4 format and the mic audio source.
     * The file is output to a temporary file which is overwritten each time a record happens.
     */
    public void startRecording()
    {
        recording = true;
        messageLength = 0;

        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        // Record to temporary file
        File temp = new File(Environment.getExternalStorageDirectory(), TEMP_MESSAGE_FILENAME + AUDIO_FILE_FORMAT);
        recorder.setOutputFile(temp.getAbsolutePath());

        try
        {
            recorder.prepare();
        } catch (IOException e)
        {
            Log.e(TAG, "prepare() failed");
        }

        // Check to see if the start recording sound should be played.
        long duration = 0;
        boolean playSound = Utils.getBooleanPref(context, "key_message_record_sound");
        if (playSound) duration = playSoundEffect(R.raw.start_recording_2);

        audioHandler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    recorder.start();
                } catch (IllegalStateException e)
                {
                    Log.e(TAG, "Don't do that asshole.");
                }

                // Start the runnable that tells us how long the message is and where we're at in the recording process.
                if (audioListener != null) audioListener.onStartRecord();
                audioHandler.post(recordRunnable);
            }
        }, duration / 2);
    }

    /**
     * Stop the current recording.
     */
    public void stopRecording()
    {
        if (recording)
        {
            recording = false;

            if (audioListener != null) audioListener.onEndRecord();
            audioHandler.removeCallbacks(recordRunnable);

            boolean playSound = Utils.getBooleanPref(context, "key_message_record_sound");
            if (playSound) playSoundEffect(R.raw.stop_recording_2);

            try
            {
                recorder.stop();
                recorder.reset();
                recorder.release();
            } catch (RuntimeException e)
            {
                Log.e(TAG, "Stop called immediately after start.");
            }
        }
    }

    /**
     * Decodes a Base64 encoded voice message.
     *
     * @param content A Base64 string to decode
     * @return the file path where the decoded bytes are stored
     */
    private String base64Decode(String content)
    {
        byte[] audio = Base64.decode(content, Base64.NO_WRAP);
        File output = new File(Environment.getExternalStorageDirectory(), TEMP_MESSAGE_FILENAME + fileIndex + AUDIO_FILE_FORMAT);

        BufferedOutputStream bos;
        try
        {
            bos = new BufferedOutputStream((new FileOutputStream(output)));
            bos.write(audio);
            bos.flush();
            bos.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        fileIndex++;

        return output.getAbsolutePath();
    }

    /**
     * Decodes a message and then plays the decoded information
     *
     * @param message the message to decode
     * @see com.capstone.sk.messaging.VoiceMessage
     */
    public void playMessage(final VoiceMessage message)
    {
        stopRecording();

        String path = base64Decode(message.getContent());
        player = new MediaPlayer();

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                replaying = false;
                mp.reset();
                mp.release();
            }
        });

        // Play after prepared, as we use prepareAsync() instead of prepare()
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mp)
            {
                replaying = true;
                message.setMessageHeard(true);
                mp.start();
            }
        });

        try
        {
            player.setDataSource(path);
            player.prepareAsync();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    /**
     * Plays the given sound effect from the assets/raw directory.
     *
     * @param resId The resource id to play
     */
    private long playSoundEffect(int resId)
    {
        MediaPlayer mp = MediaPlayer.create(context, resId);
        mp.setVolume(0.25f, 0.25f);
        mp.start();

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                mp.reset();
                mp.release();
            }
        });

        return mp.getDuration();
    }

    /**
     * Delete all of the temporary audio files created during playMessage.
     */
    public void deleteTempFiles()
    {
        File messageDir = new File(Environment.getExternalStorageDirectory(), MESSAGE_DIRECTORY);
        int filesDeleted = 0;

        for (File audioFile : messageDir.listFiles())
        {
            if (audioFile.delete()) filesDeleted++;
        }

        Log.d(TAG, "Deleted " + filesDeleted + " files.");
    }


    /**
     * Runnable that is updated when a sound file is being replayed.
     */
    private Runnable replayRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            replayLength += PROGRESS_INCREMENT * 2;

            if (audioListener != null)
                audioListener.onReplayStep(replayLength);

            audioHandler.postDelayed(replayRunnable, PROGRESS_INCREMENT * 2);
        }
    };


    /**
     * A runnable that keeps track of the audio progress.
     */
    private Runnable recordRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            messageLength += PROGRESS_INCREMENT;

            if (audioListener != null)
                audioListener.onRecordStep(messageLength, recorder.getMaxAmplitude());

            if (messageLength < MAX_MESSAGE_DURATION)
                audioHandler.postDelayed(recordRunnable, PROGRESS_INCREMENT);
            else
                stopRecording();
        }
    };

    public String getFilename()
    {
        return TEMP_MESSAGE_FILENAME + AUDIO_FILE_FORMAT;
    }

    public int getMessageLength()
    {
        return messageLength;
    }

    public void toggleRecording()
    {
        if (recording) stopRecording();
        else startRecording();
    }

    public void toggleReplaying()
    {
        if (replaying) stopReplaying();
        else startReplaying();
    }
}
