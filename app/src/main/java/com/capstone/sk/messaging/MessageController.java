package com.capstone.sk.messaging;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.capstone.sk.helpers.SharedPrefs;
import com.capstone.sk.helpers.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Aaron on 3/9/2015.
 * Handles message recording and playback, as well as send and receive.
 */
public class MessageController
{
    private static final String MESSAGE_SAVE_FILENAME = "voicemessages.json";
    private static final String GROUP_SAVE_FILENAME = "groups.json";
    private static final String TAG = "MessageController";
    private static final String BASE_URL = "http://golang-willhaines.rhcloud.com";

    public enum SendError
    {
        SEND_ERROR_NO_FROM, SEND_ERROR_NO_TO, SEND_ERROR_NO_MESSAGE, SEND_UNSUCCESSFUL, SEND_SUCCESSFUL
    }

    private Context context;
    private NotificationListener notificationListener;
    private OnNewMessageListener onNewMessageListener;
    private AudioController audioController;
    private ArrayList<VoiceMessage> pendingMessages, currentMessages;
    private ArrayList<Group> currentGroups;

    private int nameIndex, messageIndex;

    public MessageController(Context context)
    {
        Log.d(TAG, "New MessageController is being created at " + new Date().toString());
        this.context = context;

        audioController = new AudioController(context);
        pendingMessages = new ArrayList<>();
        currentMessages = new ArrayList<>();
        currentGroups = new ArrayList<>();

        // Check to see if the message save file (voicemessages.json) exists.
        File messageSave = context.getFileStreamPath(MESSAGE_SAVE_FILENAME);
        try
        {
            boolean fileNotThere = messageSave.createNewFile();
            if (!fileNotThere) Log.d(TAG, "Message save file already exists.");
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Called when the Activity is destroyed to make sure we don't leak Context.
     * Without setting context = null, the activity stays around and a version of this
     * class is never destroyed.
     */
    public void onDestroy()
    {
        context = null;
    }

    /**
     * Creates a JsonArrayRequest to get all messages to a certain username from the server. The messages are returned
     * in an ArrayList which is passed to the message callback.
     */
    public void refreshMessages()
    {
        SharedPreferences prefs = SharedPrefs.getSharedPreferences(context);
        String to = prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);

        // Creates a new JSONArrayRequest
        AuthJsonArrayRequest authJsonArrayRequest =
                new AuthJsonArrayRequest(((BASE_URL) + "/get_messages"), to, new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        if (context == null) return;

                        Gson gson = new Gson();

                        for (int i = 0; i < response.length(); i++)
                        {
                            JSONObject message;
                            try
                            {
                                message = (JSONObject) response.get(i);
                                VoiceMessage voiceMessage = gson.fromJson(message.toString(), VoiceMessage.class);
                                pendingMessages.add(voiceMessage);
                                currentMessages.add(voiceMessage);
                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }

                        // Manual refresh
                        if (onNewMessageListener != null)
                        {
                            onNewMessageListener.onNewMessages(new ArrayList<>(currentMessages));
                        }
                    }
                }
                        ,
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                Log.e(TAG, "Get messages returns: " + error.toString());
                            }
                        }
                );

        if (pendingMessages.size() > 0)
        {
            if (onNewMessageListener != null)
            {
                onNewMessageListener.onNewMessages(new ArrayList<>(currentMessages));
                pendingMessages.clear();
            }
        }
        else
        {
            // Add the message to the requestQueue
            MessageRequest.getInstance(context.getApplicationContext()).addToRequestQueue(authJsonArrayRequest);
        }
    }


    /**
     * Used for testing messages. Has a valid base64 string so that it doesn't shit itself when played.
     *
     * @param to      The to field
     * @param message The fake voice message
     * @return Whether or not the message sent
     */
    public boolean sendTestMessage(String to, VoiceMessage message)
    {
        Gson gson = new Gson();

        // Encodes message to JSON String
        String messageString = gson.toJson(message);

        // Creates a JSON object from the string
        JSONObject messageJSON = null;
        try
        {
            messageJSON = new JSONObject(messageString);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // Creates a new JSON Object Request Object to send the message
        AuthJsonObjectRequest authJsonObjectRequest =
                new AuthJsonObjectRequest(Request.Method.POST, ((BASE_URL) + "/send_message"), to, messageJSON,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response)
                            {

                                Log.d(TAG, "Transmission successful");
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                Log.d(TAG, "Transmission error");
                            }
                        }
                );


        // Add the message to the queue
        MessageRequest.getInstance(context.getApplicationContext()).addToRequestQueue(authJsonObjectRequest);

        return true;
    }

    /**
     * Sends a message to the server with the given to and from parameters.
     * This does nothing if no message has been recorded or the to or from strings are empty.
     *
     * @param to   Message recipient.
     * @param from Message sender.
     * @return whether or not a message was sent
     */
    public boolean sendMessage(final MessageSendCallback callback, String to, String from)
    {
        File messageContent = new File(Environment.getExternalStorageDirectory(), audioController.getFilename());

        if (from.length() < 1)
        {
            if (callback != null) callback.onMessageSend(SendError.SEND_ERROR_NO_FROM);
            return false;
        }
        else if (to.length() < 1)
        {
            if (callback != null) callback.onMessageSend(SendError.SEND_ERROR_NO_TO);
            return false;
        }
        else if (!messageContent.exists())
        {
            if (callback != null) callback.onMessageSend(SendError.SEND_ERROR_NO_MESSAGE);
            return false;
        }

        audioController.stopRecording();
        audioController.stopReplaying();

        // Creates a new VoiceMessage Object
        VoiceMessage message = new VoiceMessage.Builder()
                .to(to)
                .from(from)
                .content(base64Encode(messageContent))
                .duration(String.format("%1.2f s", (float) audioController.getMessageLength() / 1000))
                .build();

        Gson gson = new Gson();

        // Encodes message to JSON String
        String messageString = gson.toJson(message);

        // Creates a JSON object from the string
        JSONObject messageJSON = null;
        try
        {
            messageJSON = new JSONObject(messageString);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // Delete the temporary file
        messageContent.delete();

        // Creates a new JSON Object Request Object to send the message
        AuthJsonObjectRequest authJsonObjectRequest =
                new AuthJsonObjectRequest(Request.Method.POST, ((BASE_URL) + "/send_message"), to, messageJSON,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response)
                            {
                                if (context == null) return;

                                if (callback != null) callback.onMessageSend(SendError.SEND_SUCCESSFUL);
                                Utils.makeToast(context, "Message sent successfully.");
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                if (context == null) return;

                                if (callback != null) callback.onMessageSend(SendError.SEND_UNSUCCESSFUL);
                                Utils.makeToast(context, "Message failed to send.");
                            }
                        }
                );

        // Add the message to the queue
        MessageRequest.getInstance(context.getApplicationContext()).addToRequestQueue(authJsonObjectRequest);

        return true;
    }

    /**
     * FUCKING GROUPS BEOTCH.
     *
     * @return
     */
    public void getGroups(final GroupResponseCallback callback)
    {
        SharedPreferences prefs = SharedPrefs.getSharedPreferences(context);
        String user = prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);

        AuthJsonArrayRequest authJsonArrayRequest =
                new AuthJsonArrayRequest(((BASE_URL) + "/get_groups"), user, new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        if (context == null) return;
                        Log.d(TAG, "Response: " + response.toString());

                        Gson gson = new Gson();

                        for (int i = 0; i < response.length(); i++)
                        {
                            JSONObject object;
                            try
                            {
                                object = (JSONObject) response.get(i);
                                Group group = gson.fromJson(object.toString(), Group.class);
                                addGroup(group);
                            } catch (JSONException | JsonSyntaxException e)
                            {
                                e.printStackTrace();
                            }
                        }

                        if (callback != null)
                        {
                            callback.onGroupResponse(new ArrayList<>(currentGroups));
                        }

                    }
                },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                if (context == null) return;
                                Log.e(TAG, "Get Groups returns: " + error.toString());

                                if (callback != null) callback.onGroupResponse(null);
                            }
                        }
                );

        Log.d(TAG, "Getting Groups");
        MessageRequest.getInstance(context.getApplicationContext()).addToRequestQueue(authJsonArrayRequest);
    }

    /**
     * Called when a user wants to join a group with the given name and password.
     *
     * @param name     Name of the group to join
     * @param pass     Password of the group to join
     * @param callback A callback that returns the results of the group join
     */
    public void joinGroup(String name, String pass, final GroupJoinCallback callback)
    {
        SharedPreferences prefs = SharedPrefs.getSharedPreferences(context);
        String user = prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);

        Gson gson = new Gson();

        // Encodes group name and password to JSON String
        final Group group = new Group(name, pass);
        String groupRequest = gson.toJson(group);

        // Creates a JSON object from the string
        JSONObject groupJSON = null;
        try
        {
            groupJSON = new JSONObject(groupRequest);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // Creates a new JSON Object Request Object to send the message
        AuthJsonObjectRequest authJsonObjectRequest =
                new AuthJsonObjectRequest(Request.Method.POST, ((BASE_URL) + "/join_group"), user, groupJSON,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response)
                            {
                                if (context == null) return;

                                String code = "Error!";
                                try
                                {
                                    code = response.getString("Err");
                                } catch (JSONException e)
                                {
                                    e.printStackTrace();
                                }

                                if (code.equals(""))
                                {
                                    addGroup(group);
                                }

                                if (callback != null) callback.onGroupResponse(code);
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                if (context == null) return;
                                Log.e(TAG, "Group add error: " + error.toString());

                                if (callback != null) callback.onGroupResponse("error");
                            }
                        }
                );


        // Add the message to the queue
        MessageRequest.getInstance(context.getApplicationContext()).addToRequestQueue(authJsonObjectRequest);
    }

    public void leaveGroup(String name)
    {
        SharedPreferences prefs = SharedPrefs.getSharedPreferences(context);
        String user = prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);

        Gson gson = new Gson();

        // Encodes group name and password to JSON String
        String groupRequest = gson.toJson(name);

        //TODO: encode password into group joining

        // Creates a JSON object from the string
        JSONObject groupJSON = null;
        try
        {
            groupJSON = new JSONObject(groupRequest);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // Creates a new JSON Object Request Object to send the message
        AuthJsonObjectRequest authJsonObjectRequest =
                new AuthJsonObjectRequest(Request.Method.POST, ((BASE_URL) + "/leave_group"), user, groupJSON,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response)
                            {
                                if (context == null) return;

                                Log.d(TAG, "Group add successful");
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                if (context == null) return;
                                Log.d(TAG, "Group add error");
                            }
                        }
                );

        // Add the message to the queue
        MessageRequest.getInstance(context.getApplicationContext()).addToRequestQueue(authJsonObjectRequest);

    }

    /**
     * Loads all of the messages from the json file where they're stored.
     */
    public void load()
    {
        ArrayList<VoiceMessage> messages = load(new TypeToken<ArrayList<VoiceMessage>>() {}, MESSAGE_SAVE_FILENAME, false);
        ArrayList<Group> groups = load(new TypeToken<ArrayList<Group>>() {}, GROUP_SAVE_FILENAME, false);

        if (messages != null)
        {
            Log.d(TAG, messages.size() + " messages on load.");
            currentMessages = new ArrayList<>(messages);
        }

        if (groups != null)
        {
            Log.d(TAG, groups.size() + " groups on load.");
            currentGroups = new ArrayList<>(groups);
        }
    }

    /**
     * Inner method that loads messages based on the given filename.
     * Used to load messages from either a pending or current message file.
     *
     * @param filename   Filename to load from
     * @param deleteFile Delete the file after load?
     * @return An ArrayList of VoiceMessages corresponding to the file loaded from
     */
    private <V> V load(TypeToken<V> typeToken, String filename, boolean deleteFile)
    {
        FileInputStream fis;
        String json;
        try
        {
            fis = context.openFileInput(filename);

            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e)
        {
            return null;
        }

        if (deleteFile)
        {
            File file = context.getFileStreamPath(filename);
            if (file != null) file.delete();
        }

        Gson gson = new Gson();
        return gson.fromJson(json, typeToken.getType());
    }

    /**
     * Perform saving of all the voices messages we currently have and overwrite
     * the current save file.
     */
    public void save()
    {
        save(MESSAGE_SAVE_FILENAME, currentMessages);
        save(GROUP_SAVE_FILENAME, currentGroups);
    }

    /**
     * Inner method to save any arraylist to any filename.
     *
     * @param filename
     * @param list
     */
    private void save(String filename, ArrayList<?> list)
    {
        // Save all of the incoming messages
        Gson gson = new Gson();
        String json;

        Log.d(TAG, "There are " + list.size() + " current messages on save.");

        json = gson.toJson(list);

        try
        {
            FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
            fos.write(json.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Encodes a given file as Base64 and returns the string.
     *
     * @param fileToEncode the file to encode
     * @return a Base64 string
     */
    private String base64Encode(File fileToEncode)
    {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        InputStream is = null;
        try
        {
            is = new FileInputStream(fileToEncode);
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        byte[] temp = new byte[1024];
        int read;

        try
        {
            assert is != null;
            while ((read = is.read(temp)) >= 0)
            {
                buffer.write(temp, 0, read);
            }

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                assert is != null;
                is.close();
                buffer.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        byte[] data = buffer.toByteArray();

        return Base64.encodeToString(data, Base64.NO_WRAP);
    }

    /**
     * Cycles through names in an array, depending on a boolean parameter. This is currently used as a
     * temporary feature before groups are implemented.
     *
     * @param direction If true, increments the array index by one, otherwise
     * @return The name at the current index
     */
    public String getName(int direction)
    {
        // Clamp to index to between 0 and size - 1
        nameIndex = nameIndex + direction;
        if (nameIndex < 0) nameIndex = 0;
        else if (nameIndex > currentGroups.size() - 1) nameIndex = currentGroups.size() - 1;

        if (currentMessages.size() >= 1) return currentGroups.get(nameIndex).getName();

        return null;
    }

    /**
     * Returns a message from the current messages based on direction. This is used
     * by the HUD so it can navigate through the current messages. If the direction is
     * 0, the message at the current index is given, otherwise the index is incremented
     * either way and clamped, then the VoiceMessage at that index is returned.
     *
     * @param direction The direction the index should sweep in, -1, 0, or 1
     * @return The voice message corresponding to the current index
     * @see VoiceMessage
     */
    public VoiceMessage getMessage(int direction)
    {
        // Clamp to index to between 0 and size - 1
        messageIndex = messageIndex + direction;
        if (messageIndex < 0) messageIndex = 0;
        else if (messageIndex > currentMessages.size() - 1) messageIndex = currentMessages.size() - 1;

        if (currentMessages.size() >= 1) return currentMessages.get(messageIndex);

        return null;
    }

    /**
     * Allows external classes to update the current messages.
     *
     * @param deletedMessages the messages to remove
     */
    public void removeMessages(ArrayList<VoiceMessage> deletedMessages)
    {
        currentMessages.removeAll(deletedMessages);
    }

    /**
     * Allows external classes to update the current messages.
     *
     * @param deletedMessages the messages to remove
     */
    public void removeGroups(ArrayList<Group> deletedMessages)
    {
        currentGroups.removeAll(deletedMessages);
    }

    /**
     * Returns the current messages.
     *
     * @return A List of current messages
     */
    public List<VoiceMessage> getMessages()
    {
        return currentMessages;
    }

    /**
     * Returns the current groups.
     *
     * @return A list of current groups.
     */
    public List<Group> getGroups()
    {
        return currentGroups;
    }

    public void addGroup(Group group)
    {
        if (!currentGroups.contains(group)) currentGroups.add(group);
    }

    public void register()
    {
        IntentFilter intentFilter = new IntentFilter(MessageService.ACTION_MESSAGE_NOTIFICATION);
        context.registerReceiver(notificationReceiver, intentFilter);
    }

    public void unregister()
    {
        context.unregisterReceiver(notificationReceiver);
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver notificationReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
//            Log.d(TAG, "Incoming intent from MessageService.");
            handleNotificationIntent(intent);
        }
    };

    public void handleNotificationIntent(Intent intent)
    {
        if (intent != null)
        {
            ArrayList<VoiceMessage> newPending = load(new TypeToken<ArrayList<VoiceMessage>>() {}, MessageService.PENDING_MESSAGE_FILENAME, true);
            if (newPending == null) return;

            for (int i = 0; i < newPending.size(); i++)
            {
                VoiceMessage message = newPending.get(i);

                if (!currentMessages.contains(message))
                {
                    currentMessages.add(message);
                    pendingMessages.add(message);
                }
            }

            save();

            if (onNewMessageListener != null)
            {
                onNewMessageListener.onNewMessages(pendingMessages);
                pendingMessages.clear();
            }

        }
    }

    public AudioController getAudioController()
    {
        return audioController;
    }

    public void setNotificationListener(NotificationListener notificationListener)
    {
        this.notificationListener = notificationListener;
    }

    public void setOnNewMessageListener(OnNewMessageListener onNewMessageListener)
    {
        this.onNewMessageListener = onNewMessageListener;
    }

    /**
     * Callback that defines what happens when messages come in.
     */
    public interface OnNewMessageListener
    {
        void onNewMessages(ArrayList<VoiceMessage> messages);
    }

    public interface GroupResponseCallback
    {
        void onGroupResponse(ArrayList<Group> response);
    }

    public interface GroupJoinCallback
    {
        void onGroupResponse(String code);
    }

    /**
     * Sends back the error code from the send function.
     */
    public interface MessageSendCallback
    {
        void onMessageSend(SendError errorCode);
    }

    /**
     * Notifies listeners that a notification has occurred.
     */
    public interface NotificationListener
    {
        void onCreateNotification();

        void onDismissNotification();
    }
}
