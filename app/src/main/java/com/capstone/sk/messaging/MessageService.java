package com.capstone.sk.messaging;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.capstone.sk.R;
import com.capstone.sk.activities.MainActivity;
import com.capstone.sk.helpers.SharedPrefs;
import com.capstone.sk.helpers.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Aaron on 4/24/2015.
 */
public class MessageService extends Service
{
    private static final String TAG = MessageService.class.getSimpleName();
    private static final String BASE_URL = "http://golang-willhaines.rhcloud.com";
    public static final String PENDING_MESSAGE_FILENAME = "pendingmessages.json";
    public static final String ACTION_MESSAGE_NOTIFICATION = "com.capstone.sk.action.MESSAGE";
    public static final String CLEAR_NOTIFICATION = "com.capstone.sk.action.CLEAR_NOTIFICATION";
    public static final int MESSAGE_NOTIFICATION_ID = 42;

    private static int numMessages = 0;
    private static HashSet<String> names = new HashSet<>(20);
    private static CopyOnWriteArrayList<VoiceMessage> pendingMessages = new CopyOnWriteArrayList<>();
    private ServiceHandler mServiceHandler;

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler
    {
        public ServiceHandler(Looper looper)
        {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg)
        {
            waitMessages();
        }
    }

    @Override
    public void onCreate()
    {
        Log.d(TAG, "onCreate() " + new Date().toString());

        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        Looper mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent != null)
        {
            boolean clearPending = intent.getBooleanExtra(CLEAR_NOTIFICATION, false);

            if (clearPending)
            {
                Log.d(TAG, "Clearing notification.");
                numMessages = 0;
                names.clear();

                NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(MessageService.MESSAGE_NOTIFICATION_ID);
            }
        }

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent)
    {
        super.onTaskRemoved(rootIntent);
        Log.d(TAG, "Task removed.");
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy()
    {
        Log.d(TAG, "onDestroy()");
    }

    private Runnable errorRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            startService(new Intent(getBaseContext(), MessageService.class));
            stopSelf();
        }
    };

    private Runnable waitRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            waitMessages();
        }
    };

    /**
     * Attempts to retrieve pending messages on a timed schedule, without the user explicitly refreshing messages.
     */
    public void waitMessages()
    {
        SharedPreferences prefs = SharedPrefs.getSharedPreferences(this);
        String user = prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);
        Log.d(TAG, "Username is " + user);

        // Creates a new JSONArrayRequest
        AuthJsonArrayRequest authJsonArrayRequest =
                new AuthJsonArrayRequest(((BASE_URL) + "/wait_messages"), user, new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        boolean newMessages = false;
                        Gson gson = new Gson();
                        Log.d(TAG, "Received " + response.length() + " messages.");

                        for (int i = 0; i < response.length(); i++)
                        {
                            JSONObject message;
                            try
                            {
                                message = (JSONObject) response.get(i);
                                VoiceMessage voiceMessage = gson.fromJson(message.toString(), VoiceMessage.class);
                                if (!pendingMessages.contains(voiceMessage))
                                {
                                    newMessages = true;
                                    pendingMessages.add(voiceMessage);
                                }
                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }

                        if (response.length() > 0)
                        {
                            savePending();

                            if (MainActivity.isActivityRunning())
                            {
                                sendNotifyBroadcast();
                            }
                            else
                            {
                                numMessages += response.length();
                                if (Utils.getBooleanPref(MessageService.this, "key_notifications") && newMessages)
                                    createNotification();
                            }

                            clearMessages();
                        }
                        else
                        {
                            Log.d(TAG, "Stopping self because of blank response.");
                            stopSelf();
                        }

                    }
                },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                Log.e(TAG, "Wait messages returns: " + error.toString());

                                mServiceHandler.removeCallbacks(errorRunnable);
                                mServiceHandler.postDelayed(errorRunnable, TimeUnit.SECONDS.toMillis(30));
                            }
                        });


        // Timeout after 10 minutes with 0 retries and 0 backoff multiplier
        authJsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.MINUTES.toMillis(8), 0, 0f));

        // Sends the message on the requestQueue
        Log.d(TAG, "Starting wait messages.");
        MessageRequest.getInstance(this).addToRequestQueue(authJsonArrayRequest);
    }

    /**
     * Requests the server to clear a list of messages so they don't get sent again.
     */
    private void clearMessages()
    {
        SharedPreferences prefs = SharedPrefs.getSharedPreferences(this);
        String user = prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT);
        Gson gson = new Gson();

        for (final VoiceMessage pendingMessage : pendingMessages)
        {
            JSONObject messageJSON = null;
            try
            {
                messageJSON = new JSONObject(gson.toJson(pendingMessage));
            } catch (JSONException e)
            {
                e.printStackTrace();
            }

            // Creates a new JSON Object Request Object to send the message
            AuthJsonObjectRequest authJsonObjectRequest =
                    new AuthJsonObjectRequest(Request.Method.POST, ((BASE_URL) + "/clear_messages"), user, messageJSON,
                            new Response.Listener<JSONObject>()
                            {
                                @Override
                                public void onResponse(JSONObject response)
                                {
                                    // If we actually removed something
                                    boolean exists = pendingMessages.remove(pendingMessage);

                                    if (pendingMessages.size() == 0 && exists)
                                    {
                                        // No more messages to clear, time to start waiting again.
                                        Log.d(TAG, "Restarting, no more pending left to clear.");
                                        mServiceHandler.postDelayed(waitRunnable, 25);
                                    }
                                    else
                                    {
                                        // Still have messages left
                                        Log.d(TAG, pendingMessages.size() + " messages left to clear.");
                                    }
                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error)
                                {
                                    Log.e(TAG, "Error from server: " + error.toString());
                                }
                            }
                    );

            // Add the message to the queue
            MessageRequest.getInstance(this).addToRequestQueue(authJsonObjectRequest);
        }
    }

    /**
     * Creates the notification when there are pending messages. The notification is set to use the singleTop
     * property of the app, which will open an existing instance, and create another one if one doesn't exist.
     */
    private void createNotification()
    {
        String title, content = "From ";

        // Put all of the names in a set
        for (VoiceMessage message : pendingMessages)
            names.add(message.getFrom());

        if (numMessages > 1)
        {
            title = numMessages + " new messages";

            // Ensure there aren't any hanging commas after the last name,
            // and concatenate all the names into a string.
            for (Iterator<String> it = names.iterator(); it.hasNext(); )
            {
                String name = it.next();
                if (it.hasNext()) content += name + ", ";
                else content += name;
            }
        }
        else
        {
            title = pendingMessages.get(0).getFrom();
            content = pendingMessages.get(0).getDuration();
        }

        // Get the URI for the notification sound for playing later.
        Uri notificationSound = Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.notification_3);
        boolean playSound = Utils.getBooleanPref(this, "key_message_notification_sound");

        // Creates an explicit intent for an Activity in your app
        Intent notificationIntent = new Intent(this, MainActivity.class);

        // Set the fragment to navigate to "Messaging" and don't create a new instance if one exists
        notificationIntent.putExtra("Fragment", "Messaging");
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Create the pending intent that the notification will take in, using the notification intent
//        PendingIntent intent = PendingIntent.getActivity(this, numMessages, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(notificationIntent);
        // Gets a PendingIntent containing the entire back stack
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Setup the notification using the builder pattern
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentIntent(pendingIntent)
                .setSound(playSound ? notificationSound : null);

        // Because the ID remains unchanged, the existing notification is updated
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(MESSAGE_NOTIFICATION_ID, notificationBuilder.build());
    }

    /**
     * Sends a broadcast that contains the messages.
     * This MainActivity will receive this if it's open, and if not then nothing will happen.
     */
    private void sendNotifyBroadcast()
    {
        // Construct an Intent tying it to the ACTION (arbitrary event namespace)
        Intent intent = new Intent();
        intent.setAction(ACTION_MESSAGE_NOTIFICATION);
        intent.putExtra("Filename", PENDING_MESSAGE_FILENAME);

        // Fire the broadcast with intent packaged
        sendBroadcast(intent);
    }

    /**
     * Save all the pending messages in a file.
     */
    private void savePending()
    {
        // Check to see if the message save file (pendingmessages.json) exists.
        File messageSave = getFileStreamPath(PENDING_MESSAGE_FILENAME);
        try
        {
            boolean fileNotThere = messageSave.createNewFile();
            if (!fileNotThere) Log.d(TAG, "Pending save file already exists.");
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        Gson gson = new Gson();
        String json = null;

        // Load in the current messages saved in the file
        FileInputStream fis;
        try
        {
            fis = openFileInput(PENDING_MESSAGE_FILENAME);

            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        Type listType = new TypeToken<ArrayList<VoiceMessage>>()
        {
        }.getType();

        ArrayList<VoiceMessage> messages = gson.fromJson(json, listType);

        // Check to see if the file had any messages.
        if (messages != null)
        {
            messages.addAll(pendingMessages);
        }
        else
        {
            // If it doesn't, then add the pending.
            messages = new ArrayList<>();
            messages.addAll(pendingMessages);
        }

        // Recreate the json and save
        json = gson.toJson(messages);

        Log.d(TAG, "Saving: " + json);

        try
        {
            FileOutputStream fos = openFileOutput(PENDING_MESSAGE_FILENAME, Context.MODE_PRIVATE);
            fos.write(json.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
