package com.capstone.sk.messaging;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Trent on 2/15/2015.
 */
public class VoiceMessage implements Serializable
{
    @SerializedName("To")
    private String to;

    @SerializedName("From")
    private String from;

    @SerializedName("Timestamp")
    private long timestamp;

    @SerializedName("Content")
    private String content;

    @SerializedName("MessageHeard")
    private boolean isMessageHeard;

    @SerializedName("Duration")
    private String duration;

    public VoiceMessage(String to, String from, String duration, String content, Date date)
    {
        this.to = to;
        this.from = from;
        this.content = content;
        this.duration = duration;
        this.isMessageHeard = false;
        this.timestamp = System.currentTimeMillis();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof VoiceMessage)) return false;

        VoiceMessage message = (VoiceMessage) o;

        if (!to.equals(message.to)) return false;
        if (!from.equals(message.from)) return false;
        if (timestamp != message.timestamp) return false;
        return duration.equals(message.duration);
    }

    @Override
    public int hashCode()
    {
        int result = to.hashCode();
        result = 31 * result + from.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + duration.hashCode();
        return result;
    }

    public String getDuration()
    {
        return duration;
    }

    public boolean isMessageHeard()
    {
        return isMessageHeard;
    }

    public void setMessageHeard(boolean isMessageHeard)
    {
        this.isMessageHeard = isMessageHeard;
    }

    public long getMsDelayFrom(long time)
    {
        return Math.abs(time - timestamp);
    }

    public String getDelayFrom(long time)
    {
        long ms = Math.abs(time - timestamp);
        long sec = TimeUnit.MILLISECONDS.toSeconds(ms);
        long min = TimeUnit.MILLISECONDS.toMinutes(ms);
        long hours = TimeUnit.MILLISECONDS.toHours(ms);
        long days = TimeUnit.MILLISECONDS.toDays(ms);

        if (days > 6)
        {
            SimpleDateFormat hrs = new SimpleDateFormat("MMM dd", Locale.US);
            return hrs.format(timestamp);
        }
        else if (hours >= 3)
        {
            SimpleDateFormat hrs = new SimpleDateFormat("EEE h:mm a", Locale.US);
            return hrs.format(timestamp);
        }
        else if (hours > 1)
            return String.format("%d hours, %d min ago", hours, min - 60 * hours);
        else if (hours == 1)
            return String.format("%d hour ago", hours);
        else if (min >= 1)
            return String.format("%d min ago", min);
        else
            return String.format("%d sec ago", sec);
    }

    public String getFrom()
    {
        return from;
    }

    public String getContent()
    {
        return content;
    }

    public static class Builder
    {
        private String to;
        private String from;
        private String duration;
        private String content;
        private Date timestamp;

        public Builder to(String to)
        {
            this.to = to;
            return this;
        }

        public Builder from(String from)
        {
            this.from = from;
            return this;
        }

        public Builder duration(String duration)
        {
            this.duration = duration;
            return this;
        }

        public Builder content(String content)
        {
            this.content = content;
            return this;
        }

        public Builder time(Date timestamp)
        {
            this.timestamp = timestamp;
            return this;
        }

        public VoiceMessage build()
        {
            return new VoiceMessage(to, from, duration, content, timestamp);
        }
    }

}
