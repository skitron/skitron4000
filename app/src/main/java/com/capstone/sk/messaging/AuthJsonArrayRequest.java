package com.capstone.sk.messaging;

import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Trent on 2/15/2015.
 */
public class AuthJsonArrayRequest extends JsonArrayRequest
{
    private final String to;

    /**
     * Creates a new request.
     *
     * @param url           URL to fetch the JSON from
     * @param to
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public AuthJsonArrayRequest(String url, String to, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener)
    {
        super(url, listener, errorListener);
        this.to = to;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError
    {
        Map<String, String> headers = new HashMap<String, String>();
        String auth = "Basic " + Base64.encodeToString((to + ":foo").getBytes(), Base64.NO_WRAP);
        headers.put("Authorization", auth);
        return headers;
    }
}
