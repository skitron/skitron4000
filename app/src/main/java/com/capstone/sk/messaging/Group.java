package com.capstone.sk.messaging;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aaron on 4/26/2015.
 */
public class Group
{
    @SerializedName("Name")
    private String name;

    @SerializedName("Password")
    private String password;

    @SerializedName("Active")
    private boolean active;

    @SerializedName("Members")
    private String[] members;

    public Group(String name, String password)
    {
        this.name = name;
        this.active = true;
        this.password = password;
        this.members = new String[1];
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        return name.equals(group.name);
    }
}