package com.capstone.sk.messaging;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Aaron on 3/22/2015.
 * Provides singleton access to a request queue.
 */
public class MessageRequest
{
    private static MessageRequest mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private MessageRequest(Context context)
    {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized MessageRequest getInstance(Context context)
    {
        if (mInstance == null)
        {
            mInstance = new MessageRequest(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue()
    {
        if (mRequestQueue == null)
        {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req)
    {
        getRequestQueue().add(req);
    }
}
