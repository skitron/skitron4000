package com.capstone.sk.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capstone.sk.R;
import com.capstone.sk.adapters.LoginListViewAdapter;
import com.capstone.sk.adapters.NavigationDrawerAdapter;
import com.capstone.sk.comm.ControllerBluetooth;
import com.capstone.sk.comm.HUDBluetooth;
import com.capstone.sk.comm.RunLengthEncoder;
import com.capstone.sk.control.hud.HUDView;
import com.capstone.sk.control.hud.ViewController;
import com.capstone.sk.fragments.DevicesFragment;
import com.capstone.sk.fragments.GroupsFragment;
import com.capstone.sk.fragments.HUDMainFragment;
import com.capstone.sk.fragments.MessageReceiveFragment;
import com.capstone.sk.helpers.FragmentAccessor;
import com.capstone.sk.helpers.SharedPrefs;
import com.capstone.sk.messaging.MessageController;
import com.capstone.sk.messaging.MessageService;
import com.splunk.mint.Mint;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity implements
        ListView.OnItemClickListener,
        FragmentManager.OnBackStackChangedListener,
        FragmentAccessor
{
    private static final String TAG = MainActivity.class.getSimpleName();
    private static boolean activityRunning = false;

    // UI Stuff
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private LinearLayout drawer;
    private ListView drawerList;
    private NavigationDrawerAdapter navDrawerAdapter;

    // Things that deal with HUD Communication
    private HUDBluetooth hudBluetooth;
    private ViewController viewController;

    // Controller communication
    private ControllerBluetooth controllerBluetooth;

    private int selectedIndex, prevIndex;
    private SharedPreferences prefs;
    private DevicesFragment devicesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Mint.initAndStartSession(MainActivity.this, "f34dbbdb");
        setContentView(R.layout.activity_main);

        prefs = SharedPrefs.getSharedPreferences(this);
        viewController = new ViewController(this);

        viewController.setOnHudViewChangedListener(new ViewController.OnHudViewChangedListener()
        {
            @Override
            public void onChange(HUDView activeView)
            {
                hudBluetooth.writeBytes(RunLengthEncoder.getInstance(MainActivity.this).encodeHudView(activeView));
                if (devicesFragment != null) devicesFragment.updatePreview(activeView);
            }
        });

        initBluetooth();
        initUI();

        restartMessageService(false);
    }

    /**
     * Initializes the HUD and Controller bluetooth classes with correct handlers.
     */
    private void initBluetooth()
    {
        hudBluetooth = new HUDBluetooth(this, new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {
                switch (msg.what)
                {
                    case HUDBluetooth.HUD_START_CONNECTION:
                        devicesFragment.setHudStatus(R.string.attempting_connection, false);
                        break;
                    case HUDBluetooth.HUD_CONNECTED:
                        devicesFragment.setHudStatus(R.string.connected, true);
                        viewController.setActiveIndex(0);
                        viewController.updateActive();
                        break;
                    case HUDBluetooth.HUD_DATA_AVAILABLE:
                        // TODO: Interpret bluetooth data from HUD

                        break;
                    case HUDBluetooth.HUD_DISCONNECTED:
                        devicesFragment.setHudStatus(R.string.disconnected, false);
                        break;
                    case HUDBluetooth.BLUETOOTH_NOT_ENABLED:
                        devicesFragment.setHudStatus(R.string.not_connected, false);
                        break;
                    case HUDBluetooth.HUD_UNABLE_TO_CONNECT:
                        devicesFragment.setHudStatus(R.string.unable_to_connect, false);
                    default:
                        super.handleMessage(msg);
                }
            }
        });

        controllerBluetooth = new ControllerBluetooth(this, new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {
                switch (msg.what)
                {
                    case ControllerBluetooth.CONTROLLER_DATA_AVAILABLE:
                        viewController.buttonPress((String) msg.obj);
                        break;
                    case ControllerBluetooth.CONTROLLER_CONNECTED:
                        devicesFragment.setControllerStatus(R.string.connected, true);
                        break;
                    case ControllerBluetooth.CONTROLLER_DISCONNECTED:
                        devicesFragment.setControllerStatus(R.string.disconnected, false);
                        break;
                    case ControllerBluetooth.CONTROLLER_START_CONNECTION:
                        devicesFragment.setControllerStatus(R.string.attempting_connection, false);
                        break;
                    case ControllerBluetooth.CONTROLLER_UNABLE_TO_CONNECT:
                        devicesFragment.setControllerStatus(R.string.unable_to_connect, false);
                        break;
                    case ControllerBluetooth.BLUETOOTH_NOT_ENABLED:
                        devicesFragment.setControllerStatus(R.string.not_connected, false);
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        });

    }

    /**
     * Initializes the drawer and toolbar.
     * Creates all of the fragments that go into the drawer as well.
     */
    private void initUI()
    {
        /**
         * Instantiate all of the UI elements that work with the drawer.
         * The action bar (appcompat Toolbar) is setup here, as well as the drawer layout
         * and the drawer itself.
         */
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.main_listview);
        drawer = (LinearLayout) findViewById(R.id.drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final ExpandableListView loginListView = (ExpandableListView) findViewById(R.id.login_listview);

        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Support for the OnBackStackChanged method
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        // Setup the Navigation Drawer
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_toggle_drawer_open, R.string.drawer_toggle_drawer_closed);

        //Instantiate all of the fragments
        HUDMainFragment hudMainFragment = new HUDMainFragment();
        MessageReceiveFragment messageReceiveFragment = new MessageReceiveFragment();
        GroupsFragment groupsFragment = new GroupsFragment();
        devicesFragment = new DevicesFragment();
        SettingsActivity settingsActivity = new SettingsActivity();

        //Add them to navigation drawer
        navDrawerAdapter = new NavigationDrawerAdapter(this);
        navDrawerAdapter.addNavigationDrawerItem("Editor", hudMainFragment, R.drawable.ic_editor_nav);
        navDrawerAdapter.addNavigationDrawerItem("Devices", devicesFragment, R.drawable.ic_bluetooth);
        navDrawerAdapter.addNavigationDrawerItem("Messaging", messageReceiveFragment, R.drawable.ic_cloud);
        navDrawerAdapter.addNavigationDrawerItem("Friends", groupsFragment, R.drawable.ic_group);
        navDrawerAdapter.addNavigationDrawerItem("Settings", settingsActivity, R.drawable.ic_settings);

        // Adding the actual navigation drawer items to the drawer itself and setting up the listener
        // for when the drawer is closed, as well as items are chosen
        drawerList.setAdapter(navDrawerAdapter);
        drawerList.setOnItemClickListener(this);
        drawerLayout.setDrawerListener(new DrawerListener());

        // Recover the saved position
        selectedIndex = prefs.getInt("sel_index", 0);
        prevIndex = prefs.getInt("prev_index", 0);

        final LoginListViewAdapter loginAdapter = new LoginListViewAdapter(this);
        ArrayList<String> childNames = new ArrayList<>();
        childNames.add("Account Login");
        loginAdapter.addGroup("", childNames);
        loginListView.setAdapter(loginAdapter);

        loginListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener()
        {
            @Override
            public boolean onChildClick(final ExpandableListView parent, View v, int groupPosition, int childPosition, long id)
            {
                final MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.this)
                        .title("Login to an account.")
                        .customView(R.layout.add_account, true)
                        .positiveText("Login")
                        .negativeText("Cancel")
                        .autoDismiss(false)
                        .callback(new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onPositive(MaterialDialog dialog)
                            {
                                View view = dialog.getCustomView();
                                EditText accountNameField = (EditText) view.findViewById(R.id.add_account_name_field);
                                TextView errorText = (TextView) view.findViewById(R.id.add_account_error_text);

                                String accountName = accountNameField.getText().toString();

                                if (accountName.length() < 1)
                                {
                                    errorText.setVisibility(View.VISIBLE);
                                    errorText.setAlpha(1f);
                                    errorText.animate().alpha(0f).setDuration(3000);
                                }
                                else
                                {
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putString(SharedPrefs.PREF_ACCOUNT_NAME, accountName);
                                    editor.apply();

                                    dialog.dismiss();
                                    loginListView.collapseGroup(0);
                                    loginAdapter.notifyDataSetChanged();

                                    restartMessageService(true);
                                }
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                dialog.dismiss();
                            }
                        })
                        .build();

                View addView = dialog.getCustomView();
                EditText accountNameField = (EditText) addView.findViewById(R.id.add_account_name_field);
                accountNameField.setText(prefs.getString(SharedPrefs.PREF_ACCOUNT_NAME, SharedPrefs.PREF_ACCOUNT_DEFAULT));

                accountNameField.setOnFocusChangeListener(new View.OnFocusChangeListener()
                {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus)
                    {
                        if (hasFocus)
                        {
                            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        }
                    }
                });

                accountNameField.setFocusable(true);
                accountNameField.setFocusableInTouchMode(true);
                accountNameField.requestFocus();
                dialog.show();

                return true;
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        setIntent(intent);

        Log.d(TAG, "Single top intent is set.");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        Log.d(TAG, "onDestroy()");

        viewController.onDestroy();
        controllerBluetooth.onDestroy();
        hudBluetooth.unregister();

        viewController = null;
        controllerBluetooth = null;
        hudBluetooth = null;
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Log.d("MainActivity", "onPause()");

        activityRunning = false;
        getMessageController().unregister();

        // Save the position of the content the user was looking at
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("sel_index", selectedIndex);
        editor.putInt("prev_index", prevIndex);
        editor.apply();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.d(TAG, "onResume()");

        activityRunning = true;

        drawerList.setItemChecked(selectedIndex, true);
        viewController.getContentManager().onResume();
        controllerBluetooth.register();
        hudBluetooth.register();

        Intent startingIntent = getIntent();
        getMessageController().load();
        getMessageController().handleNotificationIntent(startingIntent);

        // Restart the service
        restartMessageService(true);

        // If the starting intent has "Fragment", open the fragment with the right name
        if (startingIntent.hasExtra("Fragment"))
        {
            String fragment = startingIntent.getStringExtra("Fragment");
            for (int i = 0; i < navDrawerAdapter.getCount(); i++)
            {
                if (fragment.equals(navDrawerAdapter.getTitle(i)))
                {
                    selectItem(i);
                }
            }
        }
        // Otherwise if we're not in a sub-fragment (like the HUDEditorFragment) open that
        else
        {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0)
                selectItem(selectedIndex);
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    public static boolean isActivityRunning()
    {
        return activityRunning;
    }

    private void restartMessageService(boolean clearNotifications)
    {
        // Restart the service
        Intent serviceIntent = new Intent(this, MessageService.class);
        if (clearNotifications) serviceIntent.putExtra(MessageService.CLEAR_NOTIFICATION, true);
        stopService(serviceIntent);
        startService(serviceIntent);
    }

    /**
     * START OF FRAGMENT INTERFACE METHODS
     */

    @Override
    public HUDBluetooth getHUDBluetooth()
    {
        return hudBluetooth;
    }

    @Override
    public ViewController getViewController()
    {
        return viewController;
    }

    @Override
    public ControllerBluetooth getControllerBluetooth()
    {
        return controllerBluetooth;
    }

    @Override
    public MessageController getMessageController()
    {
        return viewController.getContentManager().getMessageController();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        drawerLayout.closeDrawer(drawer);
        prevIndex = selectedIndex;
        selectedIndex = position;
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position)
    {
        switch (navDrawerAdapter.getItemViewType(position))
        {
            case NavigationDrawerAdapter.ACTIVITY_VIEW_TYPE:

                // Keep track of the fragment we want to go back to
                selectedIndex = prevIndex;
                drawerList.setItemChecked(position, false);

                Activity activity = navDrawerAdapter.getActivity(position);
                Intent intent = new Intent(this, activity.getClass());
                startActivity(intent);

                break;
            case NavigationDrawerAdapter.FRAGMENT_VIEW_TYPE:

                drawerList.setItemChecked(position, true);

                // Create a new fragment and specify the planet to show based on position
                Fragment fragment = navDrawerAdapter.getFragment(position);
                FragmentManager fragmentManager = getSupportFragmentManager();
                getSupportActionBar().setTitle(navDrawerAdapter.getTitle(position));

                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commitAllowingStateLoss();

                break;
            default:
                break;
        }
    }

    /**
     * Keeps track of the back stack, and is used for transitioning
     * from the HUD Main to HUD Editor fragments.
     */
    @Override
    public void onBackStackChanged()
    {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
        {
            drawerToggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        else
        {
            drawerToggle.setDrawerIndicatorEnabled(true);
            getSupportActionBar().setTitle(navDrawerAdapter.getTitle(selectedIndex));
        }
    }

    private class DrawerListener extends DrawerLayout.SimpleDrawerListener
    {
        @Override
        public void onDrawerClosed(View drawerView)
        {
            selectItem(selectedIndex);
        }
    }

}