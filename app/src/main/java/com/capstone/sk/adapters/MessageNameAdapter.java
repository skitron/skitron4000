package com.capstone.sk.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.capstone.sk.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by Aaron on 3/20/2015.
 */
public class MessageNameAdapter extends RecyclerView.Adapter<MessageNameAdapter.MessageNameHolder> implements Filterable
{
    private HashSet<String> names;
    private ArrayList<String> visibleNames;
    private OnItemClickListener itemClickListener;

    public MessageNameAdapter()
    {
        names = new HashSet<>();
        visibleNames = new ArrayList<>();

        names.add("Trent");
        names.add("Wet");
        names.add("Will");
        names.add("Conan");
        names.add("Trevor");
        names.add("Tierma");
        names.add("Alex");
        names.add("Aaron");
        names.add("#SkiTron");
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MessageNameHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_name_item, parent, false);
        return new MessageNameHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageNameHolder holder, int position)
    {
        holder.name.setText(visibleNames.get(position));
    }

    @Override
    public int getItemCount()
    {
        return visibleNames.size();
    }

    @Override
    public Filter getFilter()
    {

        return new Filter()
        {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                visibleNames = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults results = new FilterResults();
                ArrayList<String> filteredNames = new ArrayList<>();

                // perform your search here using the searchConstraint String.

                if (constraint.length() > 0)
                {
                    constraint = constraint.toString().toLowerCase();
                    for (String name : names)
                    {
                        if (name.toLowerCase().contains(constraint.toString()))
                        {
                            filteredNames.add(name);
                        }
                    }
                }

                Collections.sort(filteredNames);

                results.count = filteredNames.size();
                results.values = filteredNames;

                return results;
            }
        };
    }

    public class MessageNameHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        protected TextView name;

        public MessageNameHolder(View itemView)
        {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.person_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            int position = getPosition();
            String content = visibleNames.get(position);
            if (itemClickListener != null) itemClickListener.onItemClick(content);
        }
    }

    public interface OnItemClickListener
    {
        void onItemClick(String content);
    }
}
