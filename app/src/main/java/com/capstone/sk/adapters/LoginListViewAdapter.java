package com.capstone.sk.adapters;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.capstone.sk.R;
import com.capstone.sk.helpers.SharedPrefs;
import com.capstone.sk.helpers.Typefaces;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Aaron on 11/2/2014.
 *
 * Used in the Navigation drawer for account information.
 * Currently has nothing that actually ties this to an account of some kind.
 */
public class LoginListViewAdapter extends BaseExpandableListAdapter
{
    private Activity activity;
    private SharedPreferences preferences;
    private LayoutInflater inflater;
    private ArrayList<String> groupNames;
    private HashMap<String, ArrayList<String>> childNameMap;

    public LoginListViewAdapter(Activity activity)
    {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
        preferences = SharedPrefs.getSharedPreferences(activity);
        groupNames = new ArrayList<>();
        childNameMap = new HashMap<>();
    }

    public void addGroup(String groupName, ArrayList<String> childNames)
    {
        groupNames.add(groupName);
        childNameMap.put(groupName, childNames);
    }

    @Override
    public int getGroupCount()
    {
        return groupNames.size();
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        return childNameMap.get(groupNames.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        return groupNames.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
        return childNameMap.get(groupNames.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return childPosition;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        GroupHolder holder;

        if (convertView == null)
        {
            holder = new GroupHolder();
            convertView = inflater.inflate(R.layout.login_group_item, parent, false);
            holder.groupName = (TextView) convertView.findViewById(R.id.username);
            holder.indicator = (ImageView) convertView.findViewById(R.id.indicator);
            convertView.setTag(holder);
        }
        else
        {
            holder = (GroupHolder) convertView.getTag();
        }

        holder.groupName.setText(preferences.getString("account_name", ""));

        if (isExpanded) holder.indicator.setImageResource(R.drawable.ic_collapse_arrow);
        else holder.indicator.setImageResource(R.drawable.ic_expand_arrow);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
    {
        ChildHolder holder;

        if (convertView == null)
        {
            holder = new ChildHolder();
            convertView = inflater.inflate(R.layout.login_child_item, parent, false);
            holder.childName = (TextView) convertView.findViewById(R.id.child_name);
            holder.childIcon = (ImageView) convertView.findViewById(R.id.child_icon);

            holder.childIcon.setAlpha(0.54f);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ChildHolder) convertView.getTag();
        }

        holder.childName.setTypeface(Typefaces.get(activity, "fonts/Roboto-Medium.ttf"));
        holder.childName.setText(childNameMap.get(groupNames.get(groupPosition)).get(childPosition));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }

    private class ChildHolder
    {
        private TextView childName;
        private ImageView childIcon;
    }

    private class GroupHolder
    {
        private TextView groupName;
        private ImageView indicator;
    }
}
