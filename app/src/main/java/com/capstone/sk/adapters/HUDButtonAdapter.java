package com.capstone.sk.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.capstone.sk.R;
import com.capstone.sk.control.button.ButtonInformation;

import java.util.ArrayList;

/**
 * Created by Aaron on 3/4/2015.
 */
public class HUDButtonAdapter extends RecyclerView.Adapter<HUDButtonAdapter.HUDButtonHolder>
{
    private final String GREEN = "green";
    private final String YELLOW = "yellow";
    private final String BLUE = "blue";
    private final String RED = "red";
    private final String LONG = "Long";
    private final String SHORT = "Short";

    private LayoutInflater inflater;
    private ArrayList<ButtonInfo> buttonInfos;
    private int[] colors;

    public HUDButtonAdapter(Context context, ButtonInformation buttonInformation)
    {
        inflater = LayoutInflater.from(context);
        colors = context.getResources().getIntArray(R.array.controller_colors);

        buttonInfos = new ArrayList<>();
        buttonInfos.add(new ButtonInfo(GREEN, SHORT, buttonInformation.getFunction(GREEN + SHORT)));
        buttonInfos.add(new ButtonInfo(GREEN, LONG, buttonInformation.getFunction(GREEN + LONG)));
        buttonInfos.add(new ButtonInfo(YELLOW, SHORT, buttonInformation.getFunction(YELLOW + SHORT)));
        buttonInfos.add(new ButtonInfo(YELLOW, LONG, buttonInformation.getFunction(YELLOW + LONG)));
        buttonInfos.add(new ButtonInfo(BLUE, SHORT, buttonInformation.getFunction(BLUE + SHORT)));
        buttonInfos.add(new ButtonInfo(BLUE, LONG, buttonInformation.getFunction(BLUE + LONG)));
        buttonInfos.add(new ButtonInfo(RED, SHORT, buttonInformation.getFunction(RED + SHORT)));
        buttonInfos.add(new ButtonInfo(RED, LONG, buttonInformation.getFunction(RED + LONG)));

        notifyDataSetChanged();
    }

    public ArrayList<ButtonInfo> getButtonInfos()
    {
        return buttonInfos;
    }

    private int getPositionForName(Spinner spinner, String name)
    {
        for (int i = 0; i < spinner.getCount(); i++)
        {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(name)) return i;
        }

        return -1;
    }

    @Override
    public HUDButtonHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new HUDButtonHolder(inflater.inflate(R.layout.button_selection_item, parent, false));
    }

    @Override
    public void onBindViewHolder(HUDButtonHolder holder, int position)
    {
        final ButtonInfo info = buttonInfos.get(position);

        if (info.press.equals(LONG)) holder.pressIcon.setImageResource(R.drawable.ic_long_press);
        else if (info.press.equals(SHORT)) holder.pressIcon.setImageResource(R.drawable.ic_short_press);

        if (info.color.equals(GREEN)) holder.pressIcon.setColorFilter(colors[0]);
        else if (info.color.equals(YELLOW)) holder.pressIcon.setColorFilter(colors[1]);
        else if (info.color.equals(RED)) holder.pressIcon.setColorFilter(colors[2]);
        else if (info.color.equals(BLUE)) holder.pressIcon.setColorFilter(colors[3]);

        int pos = getPositionForName(holder.functionSpinner, info.functionName);
        if (pos >= 0) holder.functionSpinner.setSelection(pos);

        holder.functionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                info.functionName = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return buttonInfos.size();
    }

    public static class HUDButtonHolder extends RecyclerView.ViewHolder
    {
        protected ImageView pressIcon;
        protected Spinner functionSpinner;

        public HUDButtonHolder(View itemView)
        {
            super(itemView);

            pressIcon = (ImageView) itemView.findViewById(R.id.button_icon);
            functionSpinner = (Spinner) itemView.findViewById(R.id.hud_button_select_spinner);
        }
    }

    public class ButtonInfo
    {
        protected String color, press, functionName;

        public ButtonInfo(String color, String press, String functionName)
        {
            this.color = color;
            this.press = press;
            this.functionName = functionName;
        }

        public String getName()
        {
            return color + press;
        }

        public String getFunctionName()
        {
            return functionName;
        }
    }
}
