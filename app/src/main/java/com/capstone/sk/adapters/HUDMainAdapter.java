package com.capstone.sk.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.capstone.sk.R;
import com.capstone.sk.control.hud.HUDView;
import com.capstone.sk.fragments.HUDButtonFragment;
import com.capstone.sk.fragments.HUDEditorFragment;
import com.capstone.sk.helpers.ViewFactory;
import com.joooonho.SelectableRoundedImageView;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by Aaron on 1/10/2015.
 *
 * Adapter for showing all the hud views in list format.
 * The OnClick functions for each of the list elements is placed here.
 *
 * Bitmaps for each of the HUDViews are generated whenever the list is updated,
 * as to not create a giant memory allocation just for bitmaps whenever the list is scrolled.
 */
public class HUDMainAdapter extends RecyclerView.Adapter<HUDMainAdapter.HUDMainHolder>
{
    private static final int NORMAL = 0;
    private static final int FOOTER = 1;

    private LayoutInflater inflater;
    private ArrayList<HUDView> hudViews;
    private FragmentManager fm;
    private ArrayList<Bitmap> hudBitmaps;
    private Context context;
    private Stack<HUDUndoPair> hudUndoStack;

    public HUDMainAdapter(Context context, FragmentManager fragmentManager)
    {
        this.fm = fragmentManager;
        this.context = context;

        inflater = LayoutInflater.from(context);
        hudBitmaps = new ArrayList<>();
        hudViews = new ArrayList<>();
        hudUndoStack = new Stack<>();
    }

    private Bitmap createBitmap(HUDView hudView)
    {
        View view = hudView.getView();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public void add(ArrayList<HUDView> incomingViews)
    {
        hudViews.clear();
        hudBitmaps.clear();

        for (HUDView hudView : incomingViews)
        {
            hudViews.add(hudView);
            hudBitmaps.add(createBitmap(hudView));
        }

        notifyDataSetChanged();
    }

    public HUDView getHUDView(int position)
    {
        return hudViews.get(position);
    }

    public HUDView undo()
    {
        if (!hudUndoStack.empty())
        {
            HUDUndoPair hudUndoPair = hudUndoStack.pop();
            insert(hudUndoPair.position, hudUndoPair.hudView);

            return hudUndoPair.hudView;
        }

        return null;
    }

    public void clearUndo()
    {
        hudUndoStack.clear();
    }

    public void remove(int position)
    {
        // Add to the stack for later removal
        HUDUndoPair hudUndoPair = new HUDUndoPair();
        hudUndoPair.hudView = hudViews.get(position);
        hudUndoPair.position = position;
        hudUndoStack.push(hudUndoPair);

        hudViews.remove(position);
        hudBitmaps.get(position).recycle();
        hudBitmaps.remove(position);

        notifyDataSetChanged();
    }

    public void add(HUDView hudView)
    {
        hudViews.add(hudView);
        hudBitmaps.add(createBitmap(hudView));

        notifyItemInserted(hudViews.size());
    }

    public void insert(int position, HUDView hudView)
    {
        hudViews.add(position, hudView);
        hudBitmaps.add(position, createBitmap(hudView));

        notifyItemInserted(position);
    }

    @Override
    public int getItemViewType(int position)
    {
        return position == hudViews.size() ? FOOTER : NORMAL;
    }

    @Override
    public HUDMainHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        // create a new view
        HUDMainHolder holder;

        if (viewType == NORMAL)
        {
            View v = inflater.inflate(R.layout.hud_main_item, parent, false);
            holder = new HUDMainHolder(v, NORMAL);
        }
        else
        {
            View v = ViewFactory.makeListFooterView(context);
            holder = new HUDMainHolder(v, FOOTER);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(HUDMainHolder holder, int position)
    {
        // We only care about populating a view if it's not the footer
        if (holder.viewType == NORMAL)
        {
            holder.hudImage.setImageBitmap(hudBitmaps.get(position));
            holder.hudName.setText(hudViews.get(position).getHUDName());
            holder.hudActionEdit.setTag(position);
            holder.hudActionButtonSelect.setTag(position);
        }
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemCount()
    {
        // Include extra for footer
        return hudViews.size() + 1;
    }

    public void recycle()
    {
        for (Bitmap bitmap : hudBitmaps)
            bitmap.recycle();

        hudViews.clear();
    }

    public class HUDMainHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        protected TextView hudName;
        protected ImageView hudImage, hudActionEdit, hudActionButtonSelect;
        protected CardView container;
        protected int viewType;

        public HUDMainHolder(View convertView, int viewType)
        {
            super(convertView);

            this.viewType = viewType;

            if (viewType == NORMAL)
            {
                hudImage = (SelectableRoundedImageView) convertView.findViewById(R.id.hud_image);
                hudName = (TextView) convertView.findViewById(R.id.hud_name);
                hudActionEdit = (ImageView) convertView.findViewById(R.id.hud_action_edit);
                hudActionButtonSelect = (ImageView) convertView.findViewById(R.id.hud_action_button_select);
                container = (CardView) convertView.findViewById(R.id.swiping_layout);

                hudActionEdit.setOnClickListener(this);
                hudActionButtonSelect.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v)
        {
            HUDView hudView = hudViews.get((Integer) v.getTag());

            switch (v.getId())
            {
                case R.id.hud_action_edit:
                    HUDEditorFragment fragment = HUDEditorFragment.newInstance(hudView.getHUDName(), hudView.getTemplateName());

                    Log.d("HUDMainAdapter", "Clicked on view at position " + v.getTag() + " with name " + hudView.getHUDName());

                    // Open an instance of the HUD Editor Fragment
                    fm.beginTransaction()
                            .replace(R.id.content_frame, fragment)
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                    break;
                case R.id.hud_action_button_select:

                    // Open an instance of the HUD Button Fragment
                    fm.beginTransaction()
                            .replace(R.id.content_frame, HUDButtonFragment.newInstance(hudView.getHUDName(), hudView.getButtonInformation()))
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                    break;
            }

        }
    }

    private class HUDUndoPair
    {
        protected HUDView hudView;
        protected int position;
    }
}
