package com.capstone.sk.adapters;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.capstone.sk.R;
import com.capstone.sk.messaging.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aaron on 2/27/2015.
 */
public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.GroupViewHolder>
{
    private static final int UNSELECTED = 0;
    private static final int SELECTED = 2;

    private ArrayList<Group> groups;
    private LayoutInflater inflater;
    private int nameColors[];
    private SparseBooleanArray selectedItems;
    private ClickListener clickListener;

    public GroupListAdapter(FragmentActivity parent, ClickListener clickListener)
    {
        this.clickListener = clickListener;

        selectedItems = new SparseBooleanArray();
        groups = new ArrayList<>();
        inflater = parent.getLayoutInflater();
        nameColors = parent.getApplicationContext().getResources().getIntArray(R.array.message_name_colors);
    }

    public boolean isPositionSelected(int pos)
    {
        return selectedItems.get(pos, false);
    }

    public void toggleSelection(int pos)
    {
        if (selectedItems.get(pos, false))
            selectedItems.delete(pos);
        else
            selectedItems.put(pos, true);
    }

    public void clearSelections()
    {
        selectedItems.clear();
    }

    public int getSelectedItemCount()
    {
        return selectedItems.size();
    }

    public List<Integer> getSelectedIndicies()
    {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++)
        {
            items.add(selectedItems.keyAt(i));
        }

        return items;
    }

    public void add(Group group)
    {
        if (!groups.contains(group))
        {
            groups.add(0, group);
            notifyItemInserted(0);
        }
    }

    public Group remove(int position)
    {
        Group group = groups.get(position);

        groups.remove(position);
        notifyItemRemoved(position);

        return group;
    }

    public ArrayList<Group> removeAll()
    {
        ArrayList<Group> temp = new ArrayList<>(groups);
        groups.clear();
        notifyDataSetChanged();
        return temp;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (isPositionSelected(position)) return SELECTED;
        else return UNSELECTED;
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view;
        switch (viewType)
        {
            case UNSELECTED:
                view = inflater.inflate(R.layout.group_item, parent, false);
                return new GroupViewHolder(view, clickListener);
            case SELECTED:
                view = inflater.inflate(R.layout.group_item_selected, parent, false);
                return new GroupViewHolder(view, clickListener);
            default:
                view = inflater.inflate(R.layout.group_item, parent, false);
                return new GroupViewHolder(view, clickListener);
        }
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position)
    {
        String groupName = groups.get(position).getName();
        String firstLetter;
        int index;

        if (groupName.startsWith("#") && groupName.length() >= 2)
        {
            firstLetter = groupName.substring(0, 2).toUpperCase();
            index = (int) firstLetter.charAt(1) - 65;
        }
        else
        {
            firstLetter = groupName.substring(0, 1).toUpperCase();
            index = (int) firstLetter.charAt(0) - 65;
        }

        if (index < 0 || index > 25) index = 0;

        // Set the usual group attributes
        holder.groupName.setText(groupName);
        holder.groupFirstLetter.setText(firstLetter);

        holder.itemView.setTag(position);
        holder.groupFirstShadow.setTag(nameColors[index]);

        if (!isPositionSelected(position))
        {
            // Correct the view visibility and reset the color filter
            holder.groupFirstShadow.setColorFilter(nameColors[index]);
            holder.groupFirstLetter.setVisibility(View.VISIBLE);
            holder.groupFirstCheck.setVisibility(View.INVISIBLE);
            holder.groupShadow.setRotationY(0);
            holder.groupName.setTypeface(holder.groupName.getTypeface(), Typeface.NORMAL);
            holder.itemView.setActivated(false);
        }
        else
        {
            // Make sure the shadow is gray and the check is visible
            holder.groupFirstShadow.setColorFilter(Color.GRAY);
            holder.groupFirstLetter.setVisibility(View.INVISIBLE);
            holder.groupFirstCheck.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount()
    {
        return groups.size();
    }

    public void addAll(List<Group> newGroups)
    {
        groups.addAll(newGroups);
        notifyItemRangeInserted(0, groups.size());
    }

    public interface ClickListener
    {
        void onItemClicked(int position, Group group);

        boolean onItemLongClicked(int position);
    }

    public class GroupViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {
        private static final int ANIMATION_DURATION = 150;

        protected TextView groupFirstLetter, groupName;
        protected ImageView groupFirstShadow, groupFirstCheck;
        protected RelativeLayout groupShadow;
        protected AnimatorListenerAdapter toSelected, fromSelected;
        private ClickListener clickListener;

        public GroupViewHolder(View itemView, ClickListener clickListener)
        {
            super(itemView);

            this.clickListener = clickListener;

            groupFirstLetter = (TextView) itemView.findViewById(R.id.group_first_letter);
            groupName = (TextView) itemView.findViewById(R.id.group_name);
            groupFirstShadow = (ImageView) itemView.findViewById(R.id.group_first_letter_shadow);
            groupFirstCheck = (ImageView) itemView.findViewById(R.id.group_first_letter_check);
            groupShadow = (RelativeLayout) itemView.findViewById(R.id.group_shadow);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            setupAnimationListeners();
        }

        @Override
        public void onClick(View v)
        {
            if (clickListener != null)
                clickListener.onItemClicked(getPosition(), groups.get(getPosition()));
        }

        @Override
        public boolean onLongClick(View v)
        {
            if (isPositionSelected(getPosition())) animateFromSelected();
            else animateToSelected();

            if (clickListener != null)
                clickListener.onItemLongClicked(getPosition());

            return true;
        }

        /**
         * Creates the animation listeners for animating to and from selected positions.
         */
        private void setupAnimationListeners()
        {
            toSelected = new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    groupShadow.animate().rotationY(180).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            notifyItemChanged(getPosition());
                        }
                    }).setDuration(ANIMATION_DURATION).start();

                    groupFirstShadow.setColorFilter(Color.GRAY);
                    groupFirstLetter.setVisibility(View.INVISIBLE);
                    groupFirstCheck.setVisibility(View.VISIBLE);

                    groupFirstCheck.setScaleX(-0.25f);
                    groupFirstCheck.setScaleY(0.25f);
                    groupFirstCheck.animate().scaleX(-1f).scaleY(1).setDuration(ANIMATION_DURATION).start();
                }
            };

            fromSelected = new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    groupShadow.animate().rotationY(0).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            notifyItemChanged(getPosition());
                        }
                    }).setDuration(ANIMATION_DURATION).start();

                    groupFirstShadow.setColorFilter((Integer) groupFirstShadow.getTag());
                    groupFirstLetter.setVisibility(View.VISIBLE);
                    groupFirstCheck.setVisibility(View.INVISIBLE);
                }
            };
        }

        /**
         * Accessing rotation from outside the message adapter.
         * This is used for rotating back to unselected position if deletion is canceled.
         */
        public void animateFromSelected()
        {
            toggleSelection(getPosition());
            groupShadow.animate().rotationY(90).setListener(fromSelected).setDuration(ANIMATION_DURATION).start();
        }

        /**
         * Access rotation to selected.
         */
        private void animateToSelected()
        {
            toggleSelection(getPosition());
            itemView.setActivated(true);
            groupShadow.animate().rotationY(90).setListener(toSelected).setDuration(ANIMATION_DURATION).start();
        }
    }
}
