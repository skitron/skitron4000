package com.capstone.sk.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.capstone.sk.R;
import com.capstone.sk.editor.color.HUDColorItem;
import com.capstone.sk.editor.dialog.HUDDialogItem;
import com.capstone.sk.editor.general.HUDEditorItem;
import com.capstone.sk.editor.slider.HUDSliderItem;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;

/**
 * Created by Aaron on 5/21/2014.
 *
 * Adapter for showing the list of HUDEditorItems.
 * Every time an item is clicked, the list is refreshed and redrawn from scratch.
 * The view holder pattern and/or checking for a null view is not done, as elements are
 * set to enabled and everything must be redrawn.
 */
public class HUDEditorAdapter extends BaseAdapter
{
    private LayoutInflater inflater;
    private ArrayList<HUDEditorItem> hudItems;

    public HUDEditorAdapter(Context context, ArrayList<HUDEditorItem> hudItems)
    {
        this.hudItems = hudItems;
        inflater = LayoutInflater.from(context);
    }

    public void setHUDItems(ArrayList<HUDEditorItem> items)
    {
        this.hudItems = items;
    }

    @Override
    public int getItemViewType(int position)
    {
        return hudItems.get(position).getViewType();
    }

    @Override
    public int getViewTypeCount()
    {
        return 3;
    }

    @Override
    public int getCount()
    {
        return hudItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return hudItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        int type = getItemViewType(position);

        switch (type)
        {
            case HUDEditorItem.VIEW_TYPE_SLIDER:
                convertView = inflater.inflate(R.layout.hud_list_slider, parent, false);
                HUDSliderItem sizeItem = (HUDSliderItem) hudItems.get(position);
                sizeItem.setupText((TextView) convertView.findViewById(R.id.slider_text));
                sizeItem.setupSlider((DiscreteSeekBar) convertView.findViewById(R.id.slider));
                break;
            case HUDEditorItem.VIEW_TYPE_COLOR:
                convertView = inflater.inflate(R.layout.hud_list_color, parent, false);
                HUDColorItem colorItem = (HUDColorItem) hudItems.get(position);
                colorItem.setupText((TextView) convertView.findViewById(R.id.color_text));
                colorItem.setupButton((ImageView) convertView.findViewById(R.id.color_button));
                break;
            case HUDEditorItem.VIEW_TYPE_DIALOG:
                convertView = inflater.inflate(R.layout.hud_list_dialog, parent, false);
                HUDDialogItem buttonItem = (HUDDialogItem) hudItems.get(position);
                buttonItem.setupButton((Button) convertView.findViewById(R.id.button));
                buttonItem.setupText((TextView) convertView.findViewById(R.id.button_text));
                break;
            default:
                break;
        }


        return convertView;
    }
}
