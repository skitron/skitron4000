package com.capstone.sk.adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capstone.sk.R;
import com.capstone.sk.customui.TintedImageView;
import com.capstone.sk.helpers.Typefaces;

import java.util.ArrayList;

/**
 * Created by Aaron on 11/1/2014.
 *
 * Adapter for showing all of the Navigation Drawer Items.
 */
public class NavigationDrawerAdapter extends BaseAdapter
{
    public static final int FRAGMENT_VIEW_TYPE = 0;
    public static final int ACTIVITY_VIEW_TYPE = 1;

    private ArrayList<NavigationDrawerItem> navigationDrawerItems;
    private LayoutInflater inflater;
    private Activity activity;

    public NavigationDrawerAdapter(Activity activity)
    {
        this.activity = activity;

        navigationDrawerItems = new ArrayList<>();
        inflater = activity.getLayoutInflater();
    }

    public void addNavigationDrawerItem(String title, Fragment fragment, int iconRes)
    {
        navigationDrawerItems.add(new NavigationDrawerItem(title, fragment, iconRes));
    }

    public void addNavigationDrawerItem(String title, Activity activity, int iconRes)
    {
        navigationDrawerItems.add(new NavigationDrawerItem(title, activity, iconRes));
    }

    public Fragment getFragment(int position)
    {
        return navigationDrawerItems.get(position).getFragment();
    }

    public Activity getActivity(int position)
    {
        return navigationDrawerItems.get(position).getActivity();
    }

    public String getTitle(int position)
    {
        return navigationDrawerItems.get(position).getTitle();
    }

    @Override
    public int getItemViewType(int position)
    {
        return navigationDrawerItems.get(position).getViewType();
    }

    @Override
    public int getCount()
    {
        return navigationDrawerItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return navigationDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        NavigationViewHolder holder;
        NavigationDrawerItem item = navigationDrawerItems.get(position);

        if (convertView == null)
        {
            holder = new NavigationViewHolder();
            convertView = inflater.inflate(R.layout.drawer_item, parent, false);
            holder.drawerItemName = (TextView) convertView.findViewById(R.id.drawer_item_name);
            holder.drawerIcon = (TintedImageView) convertView.findViewById(R.id.drawer_item_icon);

            // Google Material Design guidelines for font and color of icon
            holder.drawerItemName.setTypeface(Typefaces.get(activity, "fonts/Roboto-Medium.ttf"));
            holder.drawerIcon.setAlpha(0.54f);

            convertView.setTag(holder);
        }
        else
        {
            holder = (NavigationViewHolder) convertView.getTag();
        }

        holder.drawerItemName.setText(item.getTitle());
        holder.drawerIcon.setImageResource(item.getIconRes());

        return convertView;
    }

    private class NavigationViewHolder
    {
        public TextView drawerItemName;
        public TintedImageView drawerIcon;
    }

    private class NavigationDrawerItem
    {
        private String title;
        private int iconRes, viewType;
        private Activity activity;
        private Fragment fragment;

        public NavigationDrawerItem(String title, Fragment fragment, int iconRes)
        {
            this.title = title;
            this.iconRes = iconRes;
            this.fragment = fragment;

            viewType = FRAGMENT_VIEW_TYPE;
        }

        public NavigationDrawerItem(String title, Activity activity, int iconRes)
        {
            this.title = title;
            this.iconRes = iconRes;
            this.activity = activity;

            viewType = ACTIVITY_VIEW_TYPE;
        }

        public int getViewType()
        {
            return viewType;
        }

        public String getTitle()
        {
            return title;
        }

        public int getIconRes()
        {
            return iconRes;
        }

        public Activity getActivity()
        {
            return activity;
        }

        public Fragment getFragment()
        {
            return fragment;
        }
    }
}
