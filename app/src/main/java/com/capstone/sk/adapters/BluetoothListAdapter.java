package com.capstone.sk.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capstone.sk.R;

import java.util.ArrayList;

/**
 * Created by Aaron on 11/11/2014.
 *
 * Adapter for showing the list of bluetooth devices
 * that are currently paired. The list is filtered outside the adapter
 * to only include devices with names that contain "SkiTron".
 */
public class BluetoothListAdapter extends BaseAdapter
{
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<BluetoothDevice> devices;

    public BluetoothListAdapter(Context context)
    {
        this.context = context;

        devices = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    public void addDevice(BluetoothDevice device)
    {
        devices.add(device);
    }

    public BluetoothDevice getDevice(int position)
    {
        return devices.get(position);
    }

    @Override
    public int getCount()
    {
        return devices.size();
    }

    @Override
    public Object getItem(int position)
    {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        BluetoothHolder holder;

        if (convertView == null)
        {
            holder = new BluetoothHolder();
            convertView = inflater.inflate(R.layout.bluetooth_list_item, parent, false);
            holder.deviceAddress = (TextView) convertView.findViewById(R.id.deviceAddress);
            holder.deviceName = (TextView) convertView.findViewById(R.id.deviceName);
            convertView.setTag(holder);
        }
        else
        {
            holder = (BluetoothHolder) convertView.getTag();
        }

        holder.deviceAddress.setText(devices.get(position).getAddress());
        holder.deviceName.setText(devices.get(position).getName());

        return convertView;
    }

    public void clear()
    {
        devices.clear();
    }

    private class BluetoothHolder
    {
        public TextView deviceName;
        public TextView deviceAddress;
    }
}
