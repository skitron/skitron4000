package com.capstone.sk.adapters;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.capstone.sk.R;
import com.capstone.sk.messaging.VoiceMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Aaron on 2/27/2015.
 */
public class MessageReceiveAdapter extends RecyclerView.Adapter<MessageReceiveAdapter.MessageViewHolder>
{
    private static final String TAG = MessageReceiveAdapter.class.getSimpleName();

    private static final int HEARD = 0;
    private static final int UNHEARD = 1;
    private static final int SELECTED = 2;

    private ArrayList<VoiceMessage> voiceMessages;
    private LayoutInflater inflater;
    private int nameColors[];
    private SparseBooleanArray selectedItems;
    private ClickListener clickListener;
    private Date date;

    public MessageReceiveAdapter(Activity parent, ClickListener clickListener)
    {
        this.clickListener = clickListener;

        selectedItems = new SparseBooleanArray();
        voiceMessages = new ArrayList<>();
        inflater = parent.getLayoutInflater();
        nameColors = parent.getApplicationContext().getResources().getIntArray(R.array.message_name_colors);
        date = new Date();
    }

    public boolean isPositionSelected(int pos)
    {
        return selectedItems.get(pos, false);
    }

    public void toggleSelection(int pos)
    {
        if (selectedItems.get(pos, false))
            selectedItems.delete(pos);
        else
            selectedItems.put(pos, true);
    }

    public void clearSelections()
    {
        selectedItems.clear();
    }

    public int getSelectedItemCount()
    {
        return selectedItems.size();
    }

    public List<Integer> getSelectedIndicies()
    {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++)
        {
            items.add(selectedItems.keyAt(i));
        }

        return items;
    }

    public void add(VoiceMessage voiceMessage)
    {
        voiceMessages.add(0, voiceMessage);
        notifyItemInserted(0);
    }

    /**
     * Sorts (in ascending order of item) the list of voice messages and adds them to
     * the MessageReceiveAdapter.
     *
     * @param messagesToSort The list of messages to sort
     * @see com.capstone.sk.adapters.MessageReceiveAdapter
     */
    private void sortAndAdd(List<VoiceMessage> messagesToSort)
    {
        final long time = System.currentTimeMillis();

        if (messagesToSort != null)
        {
            Collections.sort(messagesToSort, new Comparator<VoiceMessage>()
            {
                @Override
                public int compare(VoiceMessage lhs, VoiceMessage rhs)
                {
                    return lhs.getMsDelayFrom(time) < rhs.getMsDelayFrom(time) ? -1 : 1;
                }
            });

            Log.d(TAG, "Added " + messagesToSort.size() + " messages.");

            voiceMessages.addAll(0, messagesToSort);
            notifyItemRangeInserted(0, messagesToSort.size());
        }
    }

    public void addAll(List<VoiceMessage> messages)
    {
        sortAndAdd(messages);
    }

    public VoiceMessage remove(int position)
    {
        VoiceMessage message = voiceMessages.get(position);

        voiceMessages.remove(position);
        notifyItemRemoved(position);

        return message;
    }

    public void removeAll()
    {
        voiceMessages.clear();
        notifyDataSetChanged();
    }

    public ArrayList<VoiceMessage> getMessages()
    {
        return voiceMessages;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (isPositionSelected(position)) return SELECTED;
        else return voiceMessages.get(position).isMessageHeard() ? HEARD : UNHEARD;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view;
        switch (viewType)
        {
            case HEARD:
                view = inflater.inflate(R.layout.message_item_heard, parent, false);
                return new MessageViewHolder(view, viewType, clickListener);
            case UNHEARD:
                view = inflater.inflate(R.layout.message_item_unheard, parent, false);
                return new MessageViewHolder(view, viewType, clickListener);
            case SELECTED:
                view = inflater.inflate(R.layout.message_item_selected, parent, false);
                return new MessageViewHolder(view, viewType, clickListener);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position)
    {
        VoiceMessage message = voiceMessages.get(position);

        date.setTime(System.currentTimeMillis());

        String firstLetter = message.getFrom().substring(0, 1).toUpperCase();
        int index = (int) firstLetter.charAt(0) - 65;
        if (index < 0 || index > 25) index = 0;

        // Set the usual message attributes
        holder.messageFrom.setText(message.getFrom());
        holder.messageFirstLetter.setText(firstLetter);
        holder.messageDate.setText(message.getDelayFrom(System.currentTimeMillis()));
        holder.messageDuration.setText(message.getDuration());

        holder.itemView.setTag(position);
        holder.messageFirstShadow.setTag(nameColors[index]);

        if (!isPositionSelected(position))
        {
            // Correct the view visibility and reset the color filter
            holder.messageFirstShadow.setColorFilter(nameColors[index]);
            holder.messageFirstLetter.setVisibility(View.VISIBLE);
            holder.messageFirstCheck.setVisibility(View.INVISIBLE);

            // Correct for the extra flip that happens when going from selected to unselected
            holder.messageShadow.setRotationY(0);
            holder.messageFrom.setTypeface(holder.messageFrom.getTypeface(), message.isMessageHeard() ? Typeface.NORMAL : Typeface.BOLD);
            holder.itemView.setActivated(false);
        }
        else
        {
            // Make sure the shadow is gray and the check is visible
            holder.messageFirstShadow.setColorFilter(Color.GRAY);
            holder.messageFirstLetter.setVisibility(View.INVISIBLE);
            holder.messageFirstCheck.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount()
    {
        return voiceMessages.size();
    }

    public interface ClickListener
    {
        void onItemClicked(int position, VoiceMessage message);

        boolean onItemLongClicked(int position);
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {
        private static final int ANIMATION_DURATION = 150;

        protected TextView messageFirstLetter, messageFrom, messageDate, messageDuration;
        protected ImageView messageFirstShadow, messageFirstCheck;
        protected RelativeLayout messageShadow;
        protected AnimatorListenerAdapter toSelected, fromSelected;
        private ClickListener clickListener;

        public MessageViewHolder(View itemView, int viewType, ClickListener clickListener)
        {
            super(itemView);

            this.clickListener = clickListener;

            messageFirstLetter = (TextView) itemView.findViewById(R.id.message_first_letter);
            messageFrom = (TextView) itemView.findViewById(R.id.message_from);
            messageDate = (TextView) itemView.findViewById(R.id.message_date);
            messageDuration = (TextView) itemView.findViewById(R.id.message_length);
            messageFirstShadow = (ImageView) itemView.findViewById(R.id.message_first_letter_shadow);
            messageFirstCheck = (ImageView) itemView.findViewById(R.id.message_first_letter_check);
            messageShadow = (RelativeLayout) itemView.findViewById(R.id.message_shadow);

            itemView.setOnClickListener(this);

            if (viewType == HEARD || viewType == SELECTED)
                itemView.setOnLongClickListener(this);

            setupAnimationListeners();
        }

        @Override
        public void onClick(View v)
        {
            int position = getPosition();
            VoiceMessage message = voiceMessages.get(position);
            message.setMessageHeard(true);
            notifyItemChanged(position);

            if (clickListener != null)
            {
                // The fragment plays the message instead of the adapter
                clickListener.onItemClicked(getPosition(), message);
            }
        }

        @Override
        public boolean onLongClick(View v)
        {
            if (isPositionSelected(getPosition()))
                animateFromSelected();
            else
                animateToSelected();

            if (clickListener != null)
            {
                clickListener.onItemLongClicked(getPosition());
            }

            return true;
        }

        /**
         * Creates the animation listeners for animating to and from selected positions.
         */
        private void setupAnimationListeners()
        {
            toSelected = new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    messageShadow.animate().rotationY(180).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            notifyItemChanged(getPosition());
                        }
                    }).setDuration(ANIMATION_DURATION).start();

                    messageFirstShadow.setColorFilter(Color.GRAY);
                    messageFirstLetter.setVisibility(View.INVISIBLE);
                    messageFirstCheck.setVisibility(View.VISIBLE);

                    messageFirstCheck.setScaleX(-0.25f);
                    messageFirstCheck.setScaleY(0.25f);
                    messageFirstCheck.animate().scaleX(-1f).scaleY(1).setDuration(ANIMATION_DURATION).start();
                }
            };

            fromSelected = new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    messageShadow.animate().rotationY(0).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            notifyItemChanged(getPosition());
                        }
                    }).setDuration(ANIMATION_DURATION).start();

                    messageFirstShadow.setColorFilter((Integer) messageFirstShadow.getTag());
                    messageFirstLetter.setVisibility(View.VISIBLE);
                    messageFirstCheck.setVisibility(View.INVISIBLE);
                }
            };
        }

        /**
         * Accessing rotation from outside the message adapter.
         * This is used for rotating back to unselected position if deletion is canceled.
         */
        public void animateFromSelected()
        {
            toggleSelection(getPosition());
            messageShadow.animate().rotationY(90).setListener(fromSelected).setDuration(ANIMATION_DURATION).start();
        }

        /**
         * Access rotation to selected.
         */
        private void animateToSelected()
        {
            toggleSelection(getPosition());
            itemView.setActivated(true);
            messageShadow.animate().rotationY(90).setListener(toSelected).setDuration(ANIMATION_DURATION).start();
        }
    }
}
