package com.capstone.sk.comm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.SparseArray;
import android.view.View;

import com.capstone.sk.R;
import com.capstone.sk.control.hud.HUDView;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * Created by Aaron on 11/29/2014.
 */
public class RunLengthEncoder
{
    private static final String TAG = RunLengthEncoder.class.getSimpleName();

    private static final int MAX_12_BITS = 4095;
    private static final int HISTOGRAM_THRESHOLD = 100;
    private static final int HISTOGRAM_MAX_SIZE = 16;
    private static final int WIDTH = 800;
    private static final int HEIGHT = 480;

    private LinkedHashMap<Short, Integer> histogramColors, setColorSpace;
    private SparseArray<Integer> histogramTable;
    private SparseArray<Short> previewTable;

    private static RunLengthEncoder runLengthEncoder;
    private int calls;

    public static synchronized RunLengthEncoder getInstance(Context context)
    {
        if (runLengthEncoder == null) runLengthEncoder = new RunLengthEncoder(context);
        return runLengthEncoder;
    }

    private RunLengthEncoder(Context context)
    {
        previewTable = new SparseArray<>(100);
        histogramTable = new SparseArray<>(100);
        histogramColors = new LinkedHashMap<>(20);
        setColorSpace = new LinkedHashMap<>(20);

        int[] colors = context.getResources().getIntArray(R.array.color_choices);
        int index = 0;

        for (int color : colors)
        {
            setColorSpace.put(toRGB656(color), index);
            index++;
        }
    }

    private short toRGB656(int color)
    {
        int r = ((color & 0x00FF0000) >> 16) / 8;
        int g = ((color & 0x0000FF00) >> 8) / 4;
        int b = (color & 0x000000FF) / 8;

        short scaledRed = (short) ((r & 0x1F) << 11);
        short scaledGreen = (short) ((g & 0x3F) << 5);
        short scaledBlue = (short) (b & 0x1F);

        return (short) ((scaledRed | scaledGreen | scaledBlue) & 0xFFFF);
    }

    public Bitmap getBitmapPreview(HUDView hudView)
    {
        previewTable.clear();
        histogramColors.clear();

        // Create a bitmap from the view
        View hud = hudView.getView();
        Bitmap bitmap = Bitmap.createBitmap(hud.getMeasuredWidth(), hud.getMeasuredHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        hud.draw(canvas);

        // Scale the bitmap to the right resolution and do cleanup
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, WIDTH, HEIGHT, false);
        bitmap.recycle();

        // Copy bitmap pixels to a byte array
        ShortBuffer buffer = ShortBuffer.allocate(scaled.getAllocationByteCount() / 2);
        scaled.copyPixelsToBuffer(buffer);
        scaled.recycle();
        short[] pixels = buffer.array();

        // Histogram the image to figure out what the most used pixel colors are
        // This allows ms to limit the colors to 16, and set remaining colors
        // to something close
        int[] histogram = new int[65536];
        for (short pixel : pixels)
        {
            histogram[pixel & 0xFFFF]++;
        }

        // Create the color space using colors that have a relatively high histogram value
        int index = 0;
        for (int i = 0; i < histogram.length; i++)
        {
            if (histogram[i] > HISTOGRAM_THRESHOLD && histogramColors.size() < HISTOGRAM_MAX_SIZE)
            {
                histogramColors.put((short) (i & 0xFFFF), index);
                index++;
            }
        }

        alignToColorSpace(pixels);

        Bitmap ret = Bitmap.createBitmap(800, 480, Bitmap.Config.RGB_565);
        ShortBuffer nb = ShortBuffer.allocate(ret.getAllocationByteCount() / 2);
        nb.put(pixels);
        nb.rewind();
        ret.copyPixelsFromBuffer(nb);

        return ret;
    }

    private void alignToColorSpace(short[] pixels)
    {
        for (int i = 0; i < pixels.length; i++)
        {
            if (!histogramColors.containsKey(pixels[i]))
                pixels[i] = findClosestColor(pixels[i]);
        }
    }

    private short findClosestColor(short color)
    {
        if (previewTable.get(color) != null)
            return previewTable.get(color);

        int dist, min = Integer.MAX_VALUE;
        short minKey = 0;

        byte r2, g2, b2;

        byte r = (byte) (0xFF & ((color & 0xF800) >> 11));
        byte g = (byte) (0xFF & ((color & 0x07E0) >> 5));
        byte b = (byte) (0xFF & (color & 0x001F));

        for (short key : histogramColors.keySet())
        {
            r2 = (byte) (0xFF & ((key & 0xF800) >> 11));
            g2 = (byte) (0xFF & ((key & 0x07E0) >> 5));
            b2 = (byte) (0xFF & (key & 0x001F));

            dist = Math.abs(r2 - r) + Math.abs(g2 - g) + Math.abs(b2 - b);

            if (dist < min)
            {
                min = dist;
                minKey = key;
            }
        }

        previewTable.put(color, minKey);
        return minKey;

    }

    public byte[] encodeHudView(final HUDView hudView)
    {
        histogramTable.clear();
        histogramColors.clear();
        calls = 0;

        long time = System.nanoTime();

        // Create a bitmap from the view
        View hud = hudView.getView();
        Bitmap bitmap = Bitmap.createBitmap(hud.getMeasuredWidth(), hud.getMeasuredHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        hud.draw(canvas);

//        Log.d(TAG, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time) + " ms for creating a bitmap from view.");

        time = System.nanoTime();
        // Scale the bitmap to the right resolution and do cleanup
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, WIDTH, HEIGHT, false);
        bitmap.recycle();

//        Log.d(TAG, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time) + " ms for scaling bitmap to 800 x 480.");

        time = System.nanoTime();
        // Copy bitmap pixels to a byte array
        ShortBuffer buffer = ShortBuffer.allocate(scaled.getAllocationByteCount() / 2);
        scaled.copyPixelsToBuffer(buffer);
        scaled.recycle();
        short[] pixels = buffer.array();

//        Log.d(TAG, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time) + " ms for copying pixels to buffer.");

        time = System.nanoTime();
        // Histogram the image to figure out what the most used pixel colors are
        // This allows ms to limit the colors to 16, and set remaining colors
        // to something close
        int[] histogram = new int[65536];
        for (short pixel : pixels)
        {
            histogram[pixel & 0xFFFF]++;
        }

//        Log.d(TAG, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time) + " ms for histogramming.");

        // Create the color space using colors that have a relatively high histogram value
        int index = 0;
        for (int i = 0; i < histogram.length; i++)
        {
            if (histogram[i] > HISTOGRAM_THRESHOLD && histogramColors.size() < HISTOGRAM_MAX_SIZE)
            {
                histogramColors.put((short) (i & 0xFFFF), index);
                index++;
            }
        }

        time = System.nanoTime();
        byte[] encoded = encode(pixels);
//        Log.d(TAG, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time) + " ms for encoding.");

        time = System.nanoTime();

        // Gather the colors into an array and make the Short object 0 instead of null
        // if it doesn't exist in the set
        Set<Short> keySet = histogramColors.keySet();
        Short[] colors = new Short[16];
        keySet.toArray(colors);
        for (int i = 0; i < colors.length; i++) if (colors[i] == null) colors[i] = 0;

        // Allocate header + encoded + end + header + color space + end
        ByteBuffer finalBuffer = ByteBuffer.allocate(4 + encoded.length + 1 + 4 + 32 + 1);
        finalBuffer.order(ByteOrder.LITTLE_ENDIAN);

        // Put in the run length encoded data
        finalBuffer.put((byte) 0xA);
        finalBuffer.putShort((short) (encoded.length & 0xFFFF));
        finalBuffer.put((byte) 0xD);
        finalBuffer.put(encoded);
        finalBuffer.put((byte) 0xE);

        // Put in the color space
        finalBuffer.put((byte) 0xA);
        finalBuffer.putShort((short) ((colors.length * 2) & 0xFFFF));
        finalBuffer.put((byte) 0xC);
        for (Short color : colors) finalBuffer.putShort(color);
        finalBuffer.put((byte) 0xE);

//        Log.d(TAG, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time) + " ms for packetizing.");

//        Log.d(TAG, "Color space size: " + histogramColors.size());
//        Log.d(TAG, "Table saved " + calls + " calls.");

        return finalBuffer.array();
    }

    /**
     * Find the index of the color closest to the one given.
     *
     * @param color The color to search for a match
     * @return The index of a color that most closely matches the given color.
     */
    private int findMinIndex(short color)
    {
        if (histogramTable.get(color) != null)
        {
            calls++;
            return histogramTable.get(color);
        }

        int dist, min = Integer.MAX_VALUE, minIndex = 0;

        byte r2, g2, b2;

        byte r = (byte) (0xFF & ((color & 0xF800) >> 11));
        byte g = (byte) (0xFF & ((color & 0x07E0) >> 5));
        byte b = (byte) (0xFF & (color & 0x001F));

        for (short key : histogramColors.keySet())
        {
            r2 = (byte) (0xFF & ((key & 0xF800) >> 11));
            g2 = (byte) (0xFF & ((key & 0x07E0) >> 5));
            b2 = (byte) (0xFF & (key & 0x001F));

            dist = Math.abs(r2 - r) + Math.abs(g2 - g) + Math.abs(b2 - b);

            if (dist < min)
            {
                min = dist;
                minIndex = histogramColors.get(key);
            }
        }

        histogramTable.put(color, minIndex);
        return minIndex;
    }

    /**
     * Given an array of RGB565 pixels, encode it using vertical run lengths. The data is packed into a 16 bit format
     * for a run length, where the upper-most 4 bits are color index, and the next 12 bits are run length.
     *
     * @param pixels The array of RGB565 pixels to encode
     * @return A byte array of encoded pixels
     */
    private byte[] encode(short[] pixels)
    {
        // Create a stream with 20 kB allocated
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(20000);
        int runLength = 0;
        int i = 0, j = 0;

        for (i = 0; i < WIDTH; i++)
        {
            // Wrapping
            runLength++;

            for (j = 0; j < HEIGHT - 1; j++)
            {
                if (pixels[i + j * WIDTH] == pixels[i + (j + 1) * WIDTH])
                {
                    runLength++;

                    // If we reach the maximum for a run length in 12 bits then we add it here
                    if (runLength == MAX_12_BITS)
                    {
                        addRun(byteStream, runLength, pixels[i + j * WIDTH]);
                        runLength = 0;
                    }
                }
                else
                {
                    if (runLength > 0)
                    {
                        addRun(byteStream, runLength, pixels[i + j * WIDTH]);
                        runLength = 0;
                    }

                    runLength++;
                }
            }
        }

        // Add the last one that we would have forgotten otherwise
        addRun(byteStream, runLength, pixels[i + (j - 1) * WIDTH]);

        return byteStream.toByteArray();
    }

    private void addRun(ByteArrayOutputStream stream, int runLength, short pixel)
    {
        int colorIndex = histogramColors.containsKey(pixel) ? histogramColors.get(pixel) : findMinIndex(pixel);

        // 4 bits of color index followed by 12 bits of runLength
        byte upper = (byte) ((0xFF & ((colorIndex & 0xF) << 4)) | (0xFF & ((runLength & 0xF00) >> 8)));
        byte lower = (byte) (0xFF & runLength);

        stream.write(lower);
        stream.write(upper);
    }
}