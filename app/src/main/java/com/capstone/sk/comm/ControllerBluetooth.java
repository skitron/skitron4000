package com.capstone.sk.comm;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capstone.sk.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Aaron on 2/15/2015.
 */
public class ControllerBluetooth
{
    // Controller Notifications of connection state/data
    public static final int CONTROLLER_DATA_AVAILABLE = 0;
    public static final int CONTROLLER_CONNECTED = 1;
    public static final int CONTROLLER_DISCONNECTED = 2;
    public static final int CONTROLLER_START_CONNECTION = 3;
    public static final int CONTROLLER_UNABLE_TO_CONNECT = 4;
    public static final int BLUETOOTH_NOT_ENABLED = 5;

    // UUIDs for the characteristics/services used
    public static String BUTTON_INFORMATION = "0000beef-0000-1000-8000-00805f9b34fb";
    public static String VIBRATION_MOTOR = "0000c000-0000-1000-8000-00805f9b34fb";
    public static String MOTOR_STATE = "0000c008-0000-1000-8000-00805f9b34fb";

    // Stops scanning after 10 seconds.
    private static final String TAG = "ControllerBLE";
    private static final long SCAN_PERIOD = 10000;

    private MainActivity parent;
    private Handler scanHandler;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothGattCharacteristic vibrationCharacteristic;
    private ArrayList<BluetoothDevice> bluetoothDevices;
    private BluetoothLeService bluetoothLeService;
    private BluetoothDevice deviceToConnect;
    private Handler handler;

    private boolean isBleServiceBound = false, isScanning = false;

    public ControllerBluetooth(MainActivity parent, Handler handler)
    {
        this.parent = parent;
        this.handler = handler;

        bluetoothDevices = new ArrayList<>();
        scanHandler = new Handler();

        // Initializes a Bluetooth adapter.
        final BluetoothManager bluetoothManager = (BluetoothManager) parent.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback()
    {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord)
        {
            if (!bluetoothDevices.contains(device))
                bluetoothDevices.add(device);
        }
    };

    /**
     * Writes the vibration characteristic.
     *
     * @return false if there is no characteristic, otherwise return true
     */
    public boolean writeVibration()
    {
        if (vibrationCharacteristic == null)
        {
            return false;
        }
        else
        {
            vibrationCharacteristic.setValue(new byte[]{1});
            bluetoothLeService.writeCharacteristic(vibrationCharacteristic);
            return true;
        }
    }

    /**
     * Start a scan. If bluetooth is not enabled show the dialog.
     */
    public void beginScanning()
    {
        if (!bluetoothAdapter.isEnabled())
        {
            showEnableBluetoothDialog();
        }
        else
        {
            if (!isScanning)
                scanLeDevice(true);
        }
    }

    /**
     * Public accessor so that outside methods can stop a currently running scan.
     */
    public void stopScanning()
    {
        scanLeDevice(false);
    }

    /**
     * A dialog that shows up when bluetooth is not enabled.
     */
    private void showEnableBluetoothDialog()
    {
        handler.obtainMessage(BLUETOOTH_NOT_ENABLED).sendToTarget();

        new MaterialDialog.Builder(parent)
                .title("Enable Bluetooth?")
                .content("Skitron 4000 wishes to enable Bluetooth to pair with devices.")
                .positiveText("Yes")
                .negativeText("No")
                .callback(new MaterialDialog.ButtonCallback()
                {
                    @Override
                    public void onPositive(MaterialDialog materialDialog)
                    {
                        bluetoothAdapter.enable();
                    }
                }).show();
    }

    /**
     * Toggles a LE scan.
     *
     * @param enable the scan should be enabled
     */
    private void scanLeDevice(final boolean enable)
    {
        if (enable)
        {
            // Stops scanning after a pre-defined scan period.
            scanHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    isScanning = false;
                    bluetoothAdapter.stopLeScan(leScanCallback);
                }
            }, SCAN_PERIOD);

            Log.d(TAG, "Starting LE scan.");
            isScanning = true;
            bluetoothAdapter.startLeScan(leScanCallback);
        }
        else
        {
            isScanning = false;
            bluetoothAdapter.stopLeScan(leScanCallback);
        }
    }

    /**
     * Indicates if the controller is connected.
     *
     * @return true if the controller is connected, false if not
     */
    public boolean isConnected()
    {
        return bluetoothLeService != null && bluetoothLeService.isConnected();
    }

    /**
     * Returns of a list of the devices that the LE scan found.
     *
     * @return a non-null list
     */
    public ArrayList<BluetoothDevice> getDevices()
    {
        Log.d(TAG, "Returning " + bluetoothDevices.size() + " devices.");
        return bluetoothDevices;
    }

    /**
     * Loops through all of the Gatt Services and sets them up for notification if it's a button.
     * The vibration characteristic is stored for later.
     *
     * This is called when the bluetooth service discovers the gatt services.
     *
     * @param bluetoothLeService the bluetooth service to use for gatt services.
     */
    private void setupGattServices(BluetoothLeService bluetoothLeService)
    {
        List<BluetoothGattService> gattServices = bluetoothLeService.getSupportedGattServices();
        if (gattServices == null)
        {
            Log.d(TAG, "Gatt services are null.");
            return;
        }

        String uuid;

        for (BluetoothGattService gattService : gattServices)
        {
            uuid = gattService.getUuid().toString();

            if (uuid.equals(BUTTON_INFORMATION))
            {
                for (BluetoothGattCharacteristic gattCharacteristic : gattService.getCharacteristics())
                {
                    final int charaProp = gattCharacteristic.getProperties();

                    // Set all of the characteristics for notification
                    if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0)
                    {
                        bluetoothLeService.setCharacteristicNotification(gattCharacteristic, true);
                    }
                }
            }
            else if (uuid.equals(VIBRATION_MOTOR))
            {
                Log.d(TAG, "Vibration motor characteristic found.");
                vibrationCharacteristic = gattService.getCharacteristic(UUID.fromString(MOTOR_STATE));
            }

        }
    }

    /**
     * Creates the IntentFilter for the gatt updates.
     *
     * @return the intent filter
     */
    private IntentFilter makeGattUpdateIntentFilter()
    {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    /**
     * Cleanup the service and the receiver when the parent activity is destroyed.
     */
    public void onDestroy()
    {
        if (isBleServiceBound)
        {
            parent.unbindService(serviceConnection);
            bluetoothLeService = null;
        }

        parent.unregisterReceiver(gattUpdateReceiver);
    }


    /**
     * Called in the activity register so that the receiver is registered.
     */
    public void register()
    {
        parent.registerReceiver(gattUpdateReceiver, makeGattUpdateIntentFilter());
    }


    /**
     * Attempts to start a connection with the given bluetooth device.
     * If the service is already bound, it will attempt to use the existing connection.
     *
     * @param device the BluetoothDevice to connect to
     * @see android.bluetooth.BluetoothDevice
     */
    public void startConnection(BluetoothDevice device)
    {
        if (!isBleServiceBound)
        {
            deviceToConnect = device;
            Intent gattServiceIntent = new Intent(parent, BluetoothLeService.class);
            isBleServiceBound = parent.bindService(gattServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
        else
        {
            Log.d(TAG, "BluetoothLeService is already bound.");
            bluetoothLeService.connect(deviceToConnect.getAddress());
        }
    }

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read or notification operations.
    private final BroadcastReceiver gattUpdateReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final String action = intent.getAction();

            switch (action)
            {
                case BluetoothLeService.ACTION_GATT_CONNECTED:
                    handler.obtainMessage(CONTROLLER_CONNECTED).sendToTarget();
                    Log.d(TAG, "Connected successfully.");
                    break;
                case BluetoothLeService.ACTION_GATT_DISCONNECTED:
                    Log.d(TAG, "Disconnected.");
                    handler.obtainMessage(CONTROLLER_DISCONNECTED).sendToTarget();
                    break;
                case BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED:
                    Log.d(TAG, "Gatt services have been discovered.");
                    // Show all the supported services and characteristics on the user interface.
                    setupGattServices(bluetoothLeService);
                    break;
                case BluetoothLeService.ACTION_DATA_AVAILABLE:
                    handler.obtainMessage(CONTROLLER_DATA_AVAILABLE,
                            intent.getStringExtra(BluetoothLeService.EXTRA_DATA)).sendToTarget();
                    break;
            }
        }
    };

    // Code to manage Service lifecycle.
    private final ServiceConnection serviceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service)
        {
            bluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();

            Log.d("BluetoothLEConnection", "Attempting to initialize service.");

            if (!bluetoothLeService.initialize())
            {
                handler.obtainMessage(CONTROLLER_UNABLE_TO_CONNECT).sendToTarget();
                Log.e("BluetoothLEConnection", "Unable to initialize Bluetooth");
            }
            else
            {
                handler.obtainMessage(CONTROLLER_START_CONNECTION).sendToTarget();
                bluetoothLeService.connect(deviceToConnect.getAddress());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName)
        {
            bluetoothLeService = null;
        }
    };

    /**
     * Accessor method for disconnecting if the service is currently bound.
     */
    public void disconnect()
    {
        if (isBleServiceBound)
        {
            bluetoothLeService.disconnect();
        }
    }
}
