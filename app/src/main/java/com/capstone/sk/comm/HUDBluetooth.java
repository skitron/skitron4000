package com.capstone.sk.comm;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

/**
 * Created by Aaron on 11/11/2014.
 * Facilitates bluetooth communication.
 */
public class HUDBluetooth
{
    public static final int HUD_DATA_AVAILABLE = 0;
    public static final int HUD_CONNECTED = 1;
    public static final int HUD_DISCONNECTED = 2;
    public static final int HUD_START_CONNECTION = 3;
    public static final int HUD_UNABLE_TO_CONNECT = 4;
    public static final int BLUETOOTH_NOT_ENABLED = 5;

    private static final String TAG = "Bluetooth";
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothAdapter bluetoothAdapter;
    private ConnectedThread connectedThread;
    private ConnectThread connectThread;
    private Activity parent;
    private Handler handler;

    public HUDBluetooth(Activity parent, Handler handler)
    {
        this.parent = parent;
        this.handler = handler;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public void register()
    {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        parent.registerReceiver(receiver, intentFilter);
    }

    public void unregister()
    {
        parent.unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action))
            {
                disconnect();
            }
        }
    };

    /**
     * Given a bluetooth device, the BluetoothHandler will attempt a connection.
     * If the bluetooth adapter is null then the device doesn't have bluetooth and a connection
     * cannot happen. If it's not enabled then a popup appears to prompt the user to enable bluetooth.
     * Otherwise, we're good to try a connection and a new ConnectThread is created and started.
     *
     * @param device the BluetoothDevice to attempt a connection with
     */
    public void startConnection(final BluetoothDevice device)
    {
        if (bluetoothAdapter == null)
        {
            Log.e(TAG, "Bluetooth not available on this device.");
        }
        else if (!bluetoothAdapter.isEnabled())
        {
            showEnableBluetoothDialog();
        }
        else
        {
            connectThread = new ConnectThread(device);
            connectThread.start();
        }
    }

    /**
     * Attempts to get the set of all BluetoothDevices the bluetooth adapter has paired with.
     * If the adapter is enabled, then the set of devices is returned, otherwise the user is prompted
     * to turn on Bluetooth and a blank set of devices is returned.
     *
     * @return a set of all BluetoothDevices the adapter has paired with
     */
    public Set<BluetoothDevice> getBondedDevices()
    {
        if (bluetoothAdapter.isEnabled())
        {
            return bluetoothAdapter.getBondedDevices();
        }
        else
        {
            showEnableBluetoothDialog();
        }

        return new TreeSet<>();
    }

    /**
     * If either the connect or connected threads are already null,
     * then they are both canceled and set to null, which stops both of the threads.
     */
    public void disconnect()
    {
        if (connectThread != null)
        {
            connectThread.cancel();
            connectThread = null;
            Log.d(TAG, "Successfully canceled connect thread.");
        }

        if (connectedThread != null)
        {
            connectedThread.cancel();
            connectedThread = null;
            Log.d(TAG, "Successfully canceled connected thread.");
        }

        handler.obtainMessage(HUD_DISCONNECTED).sendToTarget();
    }

    /**
     * Helper method to write bytes from outside the connected thread.
     *
     * @param bytes an array of bytes to write
     */
    public void writeBytes(byte[] bytes)
    {
        if (connectedThread != null)
        {
            connectedThread.write(bytes, 0, bytes.length);
            connectedThread.flush();
        }
        else Log.d(TAG, "Not connected, cannot send bytes.");
    }

    private void showEnableBluetoothDialog()
    {
        handler.obtainMessage(BLUETOOTH_NOT_ENABLED).sendToTarget();

        new MaterialDialog.Builder(parent)
                .title("Enable Bluetooth?")
                .content("Skitron 4000 wishes to enable Bluetooth to pair with devices.")
                .positiveText("Yes")
                .negativeText("No")
                .callback(new MaterialDialog.ButtonCallback()
                {
                    @Override
                    public void onPositive(MaterialDialog materialDialog)
                    {
                        bluetoothAdapter.enable();
                    }
                }).show();
    }

    public boolean isConnected()
    {
        return connectedThread != null;
    }

    private class ConnectThread extends Thread
    {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device)
        {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            mmDevice = device;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try
            {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e)
            {
                Log.e(TAG, "Socket creation error: " + e.getMessage());
            }

            mmSocket = tmp;
            handler.obtainMessage(HUD_START_CONNECTION).sendToTarget();
        }

        public void run()
        {
            // Cancel discovery because it will slow down the connection
            bluetoothAdapter.cancelDiscovery();

            try
            {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.connect();
            } catch (IOException connectException)
            {
                handler.obtainMessage(HUD_UNABLE_TO_CONNECT).sendToTarget();
                Log.e(TAG, "Unable to connect: " + connectException.getMessage());

                // Unable to connect; close the socket and get out
                try
                {
                    mmSocket.close();
                } catch (IOException closeException)
                {
                    Log.e(TAG, "Unable to close socket.");
                }
                return;
            }

            handler.obtainMessage(HUD_CONNECTED).sendToTarget();

            // Do work to manage the connection (in a separate thread)
            connectedThread = new ConnectedThread(mmSocket);
            connectedThread.start();
        }

        /**
         * Will cancel an in-progress connection, and close the socket
         */
        public void cancel()
        {
            try
            {
                mmSocket.close();
            } catch (IOException closeException)
            {
                Log.e(TAG, "Unable to close socket " + closeException.getMessage());
            }
        }
    }

    private class ConnectedThread extends Thread
    {
        private BluetoothSocket mmSocket;
        private InputStream mmInStream;
        private OutputStream mmOutStream;
        private long currentTime;

        public ConnectedThread(BluetoothSocket socket)
        {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try
            {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e)
            {
                Log.e(TAG, "Stream creation error: " + e.getMessage());
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes, int offset, int count)
        {
            try
            {
                currentTime = System.nanoTime();
                mmOutStream.write(bytes, offset, count);
            } catch (IOException e)
            {
                Log.e(TAG, "Error writing bytes: " + e.getMessage());
            }
        }

        public void flush()
        {
            try
            {
                mmOutStream.flush();
            } catch (IOException e)
            {
                Log.e(TAG, "Error flushing bytes: " + e.getMessage());
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel()
        {
            try
            {
                mmSocket.close();
                mmInStream.close();
                mmOutStream.close();
            } catch (IOException e)
            {
                Log.e(TAG, "Error closing thread: " + e.getMessage());
            }

            mmSocket = null;
            mmInStream = null;
            mmOutStream = null;
        }

        public void run()
        {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true)
            {
                try
                {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);

//                    Send the obtained bytes to the UI activity
                    handler.obtainMessage(HUD_DATA_AVAILABLE, bytes, -1, buffer).sendToTarget();
                } catch (IOException e)
                {
                    break;
                }
            }
        }


    }
}
